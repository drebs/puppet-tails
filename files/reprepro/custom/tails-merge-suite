#!/bin/sh

# Contains code retrieved from grml's repo cookbook
# (https://github.com/grml/grml-infrastructure/blob/master/repo-cookbook)
# that is
# Copyright © Michael Prokop <mika@grml.org> and Alexander Wirt <formorer@grml.org>

set -e

SOURCE="$1"
TARGET="$2"

USAGE="Usage: tails-merge-suite SOURCE TARGET"

### Functions

. /usr/local/share/tails-reprepro/functions.sh

### Sanity checks

[ -n "$SOURCE" ] || error "$USAGE"
[ -n "$TARGET" ] || error "$USAGE"

### Main

reprepro dumptracks "$SOURCE"  | \
   awk '/ pool\/.*\/.*\.deb/ {print $1}' | \
   xargs --max-args=1 --no-run-if-empty basename | \
   cut -d _ -f 1 | \
   sort -u | \
   xargs --no-run-if-empty reprepro copy "$TARGET" "$SOURCE"
