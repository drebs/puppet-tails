#! /usr/bin/perl

# Example usage:
#
#    $0 ftp-master.debian.org \
#       /srv/apt-snapshots/time-based \
#       "$(date -u '+%Y%m%d')01"

# Dependencies:
#
#    libcarp-assert-perl libcarp-assert-more-perl libipc-system-simple-perl
#    libpath-tiny-perl libtry-tiny-perl

use strict;
use warnings;
use 5.10.0;

use autodie qw{:all};
use Carp;
use Carp::Assert;
use Carp::Assert::More;
use English qw{-no_match_vars};
use Path::Tiny;
use IPC::System::Simple qw{systemx $EXITVAL};
use Try::Tiny;

### Functions

sub reprepro {
    my $args = shift;
    my $opts = shift;

    assert_listref($args);
    assert_exists($opts, 'basedir', q{basedir option is set});

    systemx(
        '/usr/bin/reprepro',
        ($ENV{VERBOSE} ? '--verbose' : ()),
        ($ENV{SILENT}  ? '--silent'  : ()),
        '--basedir', $opts->{basedir}, @$args
    );
}

sub reprepro_config_file_lines {
    my $basedir  = shift;
    my $filename = shift;

    path($basedir, 'conf', $filename)->lines_utf8({chomp => 1});
}

sub distributions {
   my $basedir = shift;

   my @distributions;
   foreach my $line (reprepro_config_file_lines($basedir, 'distributions')) {
       if (my ($codename) = ($line =~ /^Codename:\s+(.*)$/)) {
           push @distributions, $codename;
       }
   }
   return @distributions;
}

sub update_mirrors {
    my $basedir = shift;

    reprepro(['update'], { basedir => $basedir });
}

sub generate_snapshots {
   my $basedir = shift;
   my $serial  = shift;

   foreach my $codename (distributions($basedir)) {
       reprepro(['gensnapshot', $codename, $serial], { basedir => $basedir });
   }
}

sub update_trace_file {
    my $archive = shift;
    my $basedir = shift;
    my $serial  = shift;

    my $trace_file = path($basedir, 'project', 'trace', $archive);
    $trace_file->parent->mkpath;
    $trace_file->spew_utf8("Archive serial: $serial\n");
}

sub clean {
    my $basedir = shift;

    reprepro(['--delete', 'clearvanished'], { basedir => $basedir });
}

### Parse and check command-line

my $USAGE = "Usage: $PROGRAM_NAME ARCHIVE BASEDIR SERIAL";

assert(@ARGV == 3, $USAGE);

my ($ARCHIVE, $BASEDIR, $SERIAL) = @ARGV;

assert_nonblank($ARCHIVE, q{ARCHIVE is not blank});
assert_nonblank($BASEDIR, q{BASEDIR is not blank});
assert(-d $BASEDIR, "BASEDIR ('$BASEDIR') is a directory");
assert_nonblank($SERIAL, q{SERIAL is not blank});
assert_like($SERIAL, qr/^\d{10}$/, q{Serial is well formed});

### Main

umask 0022;
clean($BASEDIR);
update_mirrors($BASEDIR);
generate_snapshots($BASEDIR, $SERIAL);
update_trace_file($ARCHIVE, $BASEDIR, $SERIAL);
