#!/bin/bash

set -e
set -u
set -o pipefail

TMPDIR=$(mktemp -d)

trap "rm -rf '$TMPDIR'" EXIT HUP INT QUIT TERM

YELLOW='\033[0;33m'
NC='\033[0m' # No Color

validate_modified_manifests() {
   local oldrev newrev
   oldrev="$1"
   newrev="$2"

   # skip validation when pushing new branches
   [ "$oldrev" != "0000000000000000000000000000000000000000" ] || return 0

   # skip validation when deleting branches
   [ "$newrev" != "0000000000000000000000000000000000000000" ] || return 0

   modified_manifests=$(git diff --name-only --diff-filter=d "$oldrev" "$newrev" -- '**/*.pp')
   [ -n "$modified_manifests" ] || return 0

   git archive "$newrev" -- $modified_manifests | tar -x -C "$TMPDIR"
   puppet-lint --relative --with-filename "$TMPDIR" | sed -e "s|${TMPDIR}/||"
   puppet parser validate \
          $(for file in $modified_manifests; do echo "$TMPDIR/$file"; done) 2>&1 \
          | sed -e "s|${TMPDIR}/||"
   find "$TMPDIR" -name '*.pp' -delete
}

validate_modified_erb_templates() {
   local oldrev newrev
   oldrev="$1"
   newrev="$2"

   # skip validation for new branches
   [ "$oldrev" != "0000000000000000000000000000000000000000" ] || return 0

   modified_templates=$(git diff --name-only --diff-filter=d "$oldrev" "$newrev" -- '**/*.erb')
   [ -n "$modified_templates" ] || return 0

   git archive "$newrev" -- $modified_templates | tar -x -C "$TMPDIR"
   for file in $modified_templates; do
     echo -e "${YELLOW}Checking ERB template: ${file}${NC}" | sed -e "s|${TMPDIR}/||"
     erb -x -T '-' "$TMPDIR/$file" | ruby -c
   done
   find "$TMPDIR" -name '*.erb' -delete
}

while read oldrev newrev refname; do
   validate_modified_manifests "$oldrev" "$newrev"
   validate_modified_erb_templates "$oldrev" "$newrev"
done
