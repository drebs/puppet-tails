#!/bin/bash
set -e

gitolite_root=/var/lib/gitolite
repository_root="${gitolite_root}/repositories"

/usr/bin/find "$repository_root" \
   -mindepth 1 -maxdepth 1 \
   -type d -print0 | while IFS= read -r -d '' repository_dir ; do

    repository=$(basename "$repository_dir")
    # setup the immerda remote
    case "$repository" in
        puppet-*|jenkins-jobs.git|tails.git)
	    if ! grep -qs -x "${repository}" \
                  /var/lib/gitolite/projects.list ; then
	       continue
	    fi
	    (
	       cd "$repository_dir"
	       git remote | grep -qs -x immerda || \
		  git remote add immerda "tails@git.tails.boum.org:$repository"
	       git remote | grep -qs -x labs || \
		  git remote add labs "gitolite@labs.riseup.net:$repository"
	    )
            ;;
        *)
            ;;
    esac
done
