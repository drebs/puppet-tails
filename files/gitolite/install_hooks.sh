#!/bin/bash
set -e

gitolite_root=/var/lib/gitolite
repository_root="${gitolite_root}/repositories"
tails_common_hookdir="${gitolite_root}/.gitolite/hooks/tails_common"
jenkins_jobs_hookdir="${gitolite_root}/.gitolite/hooks/jenkins_jobs"

install_post_update_dot_d_hook () {
    repository="$1"
    if [ ! -d "${repository}/hooks/post-update.d" ]; then
        mkdir "${repository}/hooks/post-update.d"
        chown gitolite:gitolite "${repository}/hooks/post-update.d"
    fi
    if [ ! -h "${repository}/hooks/post-update" ]; then
        /bin/ln -s "${tails_common_hookdir}/post_update.d.hook" "${repository}/hooks/post-update"
    fi
}

install_mirror_post_update_hook () {
    repository="$1"
    remote="$2"
    if [ ! -h "${repository}/hooks/post-update.d/${remote}_mirror-post-update.hook" ]; then
        /bin/ln -s "${tails_common_hookdir}/${remote}_mirror-post-update.hook" \
		   "${repository}/hooks/post-update.d/${remote}_mirror-post-update.hook"
    fi
}

/usr/bin/find "$repository_root" \
   -mindepth 1 -maxdepth 1 \
   -type d -print0 | while IFS= read -r -d '' repository ; do
    case $(basename "$repository") in
        jenkins-jobs.git)
            install_post_update_dot_d_hook "$repository"
            install_mirror_post_update_hook "$repository" immerda
            ;;
        puppet-*)
            if ! grep -qs -x "$(basename "$repository")" \
                 /var/lib/gitolite/projects.list ; then
                continue
            fi
            install_post_update_dot_d_hook "$repository"
            install_mirror_post_update_hook "$repository" immerda
            ;;
        tails.git)
            install_post_update_dot_d_hook "$repository"
            install_mirror_post_update_hook "$repository" immerda
            install_mirror_post_update_hook "$repository" labs
            ;;
        *)
            ;;
    esac
done
