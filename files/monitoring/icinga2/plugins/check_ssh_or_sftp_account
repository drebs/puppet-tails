#!/usr/bin/python3

import sys
import argparse
import re
from paramiko import SSHClient

def report(code, message):
    if code == 0:
        print("OK: %s" % message)
    elif code == 2:
        print("CRITICAL: %s" % message)
    sys.exit(code)

def check_ssh(host, port, username):
    ssh = SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(host, port, username=username, key_filename="/var/lib/nagios/.ssh/id_rsa_%s" % username)
    except(Exception) as e:
        report(2, str(e))
    report(0, 'Ssh account %s is up' % username)

def check_sftp(host, port, username):
    ssh = SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(host, port, username=username, key_filename="/var/lib/nagios/.ssh/id_rsa_%s" % username)
        ftp = ssh.open_sftp()
    except(Exception) as e:
        report(2, str(e))
    report(0, 'Sftp account %s is up' % username)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-C",
                        "--check",
                        type=str,
                        help="Type of service to check: ssh or sftp.")
    parser.add_argument("-H",
                        "--host",
                        help="Hostname of the ssh/sftp server to check.",
                        type=str,
                        default='lizard.tails.boum.org')
    parser.add_argument("-P",
                        "--port",
                        help="Port to connect to.",
                        type=int,
                        default=3006)
    parser.add_argument("-U",
                        "--username",
                        type=str,
                        help="Username used to connect to the ssh/sftp service.")
    args = parser.parse_args()
    assert re.compile(r'^ssh|sftp$').match(args.check), \
        "Check must be either ssh or sftp: %s" % args.check
    if args.check == 'ssh':
        check_ssh(args.host, args.port, args.username)
    elif args.check == 'sftp':
        check_sftp(args.host, args.port, args.username)
