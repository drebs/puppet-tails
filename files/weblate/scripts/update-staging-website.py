#!/usr/bin/env python3
#
# A script to update the staging website. It calls other shell scripts to do
# each part of the job:
#
#   - Save suggestions from Weblate database into the Git repository.
#   - Do some sanitization to avoid Ikiwiki from breaking.
#   - Run Ikiwiki build --refresh, and then --rebuild if the first one failed.
#
# Run as user "weblate"!
#
# Logging
# -------
#
# We log stdout and stderr to 2 different log files:
#
#   - /var/log/weblate/update-staging-website.log
#   - /var/www/staging/update-staging-website.txt
#       ^
#       '-- This one is aimed to translators so it is made available on:
#
#               https://staging.tails.boum.org/update-staging-website.txt
#
# Ikiwiki output is suppressed from the public log because it is usually very
# long and can be hard for translators to understand.
#
# Stderr pass-through
# -------------------
#
# We let stderr pass-through, so when this script is run by cron e-mail
# notifications will send errors to service admins.


import datetime
import os
import selectors
import subprocess
import sys


STAGING_WEB_DIR = "/var/www/staging"
STAGING_REPO_DIR = "/var/lib/weblate/repositories/vcs/staging"
PUBLIC_LOGFILE_BASENAME = "update-staging-website.txt"
PUBLIC_LOGFILE = STAGING_WEB_DIR + "/" + PUBLIC_LOGFILE_BASENAME
SYSTEM_LOGFILE = "/var/log/weblate/update-staging-website.log"


# make sure output is logged in real time
os.environ['PYTHONUNBUFFERED'] = "1"

public = open(PUBLIC_LOGFILE, "w")  # public logs will only contain output of last run
system = open(SYSTEM_LOGFILE, "a")  # system logs will contain every thing


def _now():
    """
    Return current timestamp in ISO format for use in log files.
    """
    return datetime.datetime.now().isoformat()


def _log(msg, logs=[public, system]):
    """
    Log msg to public and system log files.
    """
    now = _now()
    for f in logs:
        f.write(now + " - " + msg)



def _run(cmd, logs=[public, system], stderr_passthrough=True, suppress_nonzero_reports=False):
    """
    Run a commmand and log full output to given files.

    Args:

        cmd (str): The command to be run.

        logs (list): A list of file handlers to write stdout and stderr to.

        stderr_passthrough (bool): Should we let stderr pass through?

        suppress_nonzero_reports (bool): If stderr_passthrough is False and the
            command returns nonzero, we will report the failing command to stderr
            unless this is True.

    Returns:

        returncode (int): The return code of the command.
    """

    _log("Running command: " + cmd + "\n")
    proc = subprocess.Popen(cmd.split(' '),
                            stdout = subprocess.PIPE,
                            stderr = subprocess.PIPE,
                            universal_newlines = True,
                            cwd = STAGING_REPO_DIR,
                            )

    sel = selectors.DefaultSelector()
    sel.register(proc.stdout, selectors.EVENT_READ)
    sel.register(proc.stderr, selectors.EVENT_READ)

    # use selectors to maintain some order in the output
    finished = False
    while not finished:
        for key, _ in sel.select():
            data = key.fileobj.readline()
            if data:
                finished = False
                _log(data, logs=logs)
            else:
                finished = True
            # pass-through stderr to ensure service admins get notified by cron
            if key.fileobj is proc.stderr and stderr_passthrough:
                print(data, end="", file=sys.stderr)

    # make sure we have a status code to report back
    proc.wait()

    if not stderr_passthrough and proc.returncode != 0 and not suppress_nonzero_reports:
        print(":: Command '" + cmd + "' exited with return code: " + str(proc.returncode), file=sys.stderr)

    _log(":: Command exited with return code: " + str(proc.returncode) + "\n")

    return proc.returncode


def main():
    _log("Starting script run!\n")

    _run("/var/lib/weblate/scripts/save-suggestions.py " + STAGING_REPO_DIR)
    _run("./bin/check-po-msgfmt --sanitize")
    _run("./bin/sanity-check-website")

    # run `ikiwiki --refresh` and, if it fails, `ikiwiki --rebuild`
    status = _run("ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --refresh",
                  logs=[system],
                  stderr_passthrough=False,
                  suppress_nonzero_reports=True)
    if status != 0:
        status =_run("ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --rebuild", logs=[system], stderr_passthrough=False)

    _log("Finished script run!\n")


if __name__ == "__main__":
    main()
