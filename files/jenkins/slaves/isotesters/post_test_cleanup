#!/bin/sh

set -e
set -u
set -x

WORKSPACE="$1"
TEMP_DIR="$2"

[ ! -z "$WORKSPACE" ] || exit 2
[ ! -z "$TEMP_DIR" ] || exit 3
[ -d "$WORKSPACE" ] || exit 4
[ -d "$TEMP_DIR" ] || exit 5

sudo -n chown -R jenkins "${TEMP_DIR}"/*

mkdir -p "${WORKSPACE}/build-artifacts"

# Move sikuli_candidates first and separately to avoid the pictures
# in there to be caught by "-name '*.png'" in the next command
# and thus copied to the root of build-artifacts/
find "${TEMP_DIR}" \
	\( -type d -a -name sikuli_candidates \) -print0 | \
	xargs --null --no-run-if-empty \
		mv --target-directory="${WORKSPACE}/build-artifacts/"

find "${TEMP_DIR}" \
	\( -name '*.png' -o -name '*.mkv' -o -name '*.pcap' \
	   -o -name debug.log -o -name cucumber.json        \
	   -o -name '*_chutney-data' -o -name '*.htpdate'   \
	   -o -name '*.journal' -o -name '*.tor'            \
	\) \
	-print0 | \
	xargs --null --no-run-if-empty \
		cp -r --target-directory="${WORKSPACE}/build-artifacts/"

sudo -n rm -rf "${TEMP_DIR}"/*
