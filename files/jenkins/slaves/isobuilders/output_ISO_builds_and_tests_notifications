#!/usr/bin/python3

# This script is called by the configure_notification_recipient
# jenkins-job-builder builder, defined in jenkins-jobs.git.

import os
import requests
import sys
from sh import git


API_URL = 'https://gitlab.tails.boum.org/api/v4/projects/tails%2Ftails/issues/{}'


def branch_last_author(git_repo_root, git_commit):
    git_directory = git_repo_root + '/.git'
    return git('--no-pager',
               '--git-dir',
               git_directory,
               'show',
               '-s',
               '--pretty=format:\'%ae\'',
               git_commit).splitlines()


def calculate_notification_vars(disabled_for_builds, disabled_for_tests, recipient):
    build_recipient_var = 'NOTIFY_BUILD_TO='
    test_recipient_var = 'NOTIFY_TEST_TO='
    if not disabled_for_builds:
        build_recipient_var = "NOTIFY_BUILD_TO=" + str(recipient[0]).strip('\'')
    if not disabled_for_tests:
        test_recipient_var = "NOTIFY_TEST_TO=" + str(recipient[0]).strip('\'')
    return build_recipient_var + "\n" + test_recipient_var


def needs_validation(apikeyfile, ticket):
    with open(apikeyfile) as apikey_file:
        apikey = apikey_file.read().rstrip()
    headers = {'Private-Token': apikey}
    json = requests.get(API_URL.format(ticket), headers=headers).json()
    return 'Needs Validation' in json['labels']



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--apikey_file",
        default=None,
        help="Path to a file containing the GitLab API key.")
    parser.add_argument(
        "--no_notify_builds",
        action='store_true',
        default=False,
        help="Disable notification for build jobs.")
    parser.add_argument(
        "--no_notify_tests",
        action='store_true',
        default=False,
        help="Disable notifications for test jobs.")
    parser.add_argument(
        "--rfqa_only",
        action='store_true',
        default=False,
        help="Notify only if related ticket is in the 'Needs Validation' status.")
    args = parser.parse_args()

    recipient = branch_last_author(
                    git_repo_root=os.environ['WORKSPACE'],
                    git_commit=os.environ['GIT_COMMIT'])
    default_notifications_vars = 'NOTIFY_BUILD_TO=\nNOTIFY_TEST_TO='
    if args.rfqa_only:
        if args.apikey_file is None:
            print("Err: you must specify apikey_file.")
            sys.exit(2)
        if int(os.environ['TAILS_TICKET']) != 0 and \
            needs_validation(
                apikeyfile=args.apikey_file,
                ticket=os.environ['TAILS_TICKET']):
            print(calculate_notification_vars(
                disabled_for_builds=args.no_notify_builds,
                disabled_for_tests=args.no_notify_tests,
                recipient=recipient))
        else:
            print(default_notifications_vars)
    else:
        if int(os.environ['TAILS_TICKET']) != 0:
            print(calculate_notification_vars(
                disabled_for_builds=args.no_notify_builds,
                disabled_for_tests=args.no_notify_tests,
                recipient=recipient))
        else:
            print(default_notifications_vars)
