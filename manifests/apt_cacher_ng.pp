# Manage apt-cacher-ng
class tails::apt_cacher_ng () {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Resources

  package { 'apt-cacher-ng':
    ensure  => present,
  }

  file { '/etc/apt-cacher-ng/backends_debian':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "http://ftp.us.debian.org/debian/\n",
    require => Package['apt-cacher-ng'],
  }

  service { 'apt-cacher-ng':
    subscribe => File['/etc/apt-cacher-ng/backends_debian'],
    require   => Package['apt-cacher-ng']
  }
}
