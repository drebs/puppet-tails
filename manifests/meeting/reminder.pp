# Set up a cronjob for a meeting

define tails::meeting::reminder (
  Tuple[Pattern[/.+@.+/], 1, default] $addresses,
  String $subject,
  String $template,
  Integer $day_of_month,
  Enum['present', 'absent'] $ensure     = 'present',
  Tuple[Integer, 1, default] $reminders = [10],
  Pattern[/.+@.+/] $fromemailaddress    = 'noreply@tails.boum.org',
  Boolean $skip_friday_to_sunday        = false,
  Boolean $append_date_to_subject       = false,
  $cron_hour                            = 8,
  $cron_minute                          = 15,
  $months                               = '*',
  Optional[String] $append_web_page     = undef,
) {

  #
  # Sanity checks
  #

  validate_email_address($fromemailaddress)
  $addresses.each |String $address| {
    validate_email_address($address)
  }

  #
  # Resources
  #

  $reminders_str = join($reminders, ',')
  $addresses_str = join($addresses, ',')

  file { "${tails::meeting::homedir}/${template}":
    ensure => $ensure,
    owner  => $tails::meeting::user,
    group  => 'root',
    mode   => '0600',
    source => "puppet:///modules/tails/meeting/${template}"
  }

  $skip_friday_to_sunday_arg = $skip_friday_to_sunday ? {
    true    => '--skip-friday-to-sunday',
    default => '',
  }

  $append_date_to_subject_arg = $append_date_to_subject ? {
    true    => '--append-date-to-subject',
    default => '',
  }

  $append_web_page_args = $append_web_page ? {
    undef   => '',
    default => "--append-web-page ${append_web_page}",
  }

  cron { "meeting-reminder-${name}":
    ensure  => $ensure,
    command => "'${tails::meeting::script_path}' --reminder '${reminders_str}' --addresses '${addresses_str}' --day '${day_of_month}' --from '${fromemailaddress}' --subject '${subject}' --template '${tails::meeting::homedir}/${template}' ${skip_friday_to_sunday_arg} ${append_date_to_subject_arg} ${append_web_page_args}", # lint:ignore:140chars -- command
    user    => $tails::meeting::user,
    hour    => $cron_hour,
    minute  => $cron_minute,
    month   => $months,
    require => [
      Class['::tails::meeting'],
      File["${tails::meeting::homedir}/${template}"],
    ],
  }
}
