# Manage a nginx vhost that reverse proxies to a weblate instance.
class tails::weblate::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'translate.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'translate.lizard',
  Stdlib::Port $upstream_port     = 80,
  ) {

  $upstream_address = "${upstream_hostname}:${upstream_port}"

  nginx::vhostsd { $public_hostname:
    content => template('tails/weblate/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included['tails_weblate_reverse_proxy'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { 'tails_weblate_reverse_proxy':
    content => template('tails/weblate/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
