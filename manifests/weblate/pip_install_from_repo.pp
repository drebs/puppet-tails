# Install a Python package from its upstream repository using pip
define tails::weblate::pip_install_from_repo (
  Optional[String] $version,
  Optional[String] $url,
  Optional[String] $git_tag = $version,
  String $repo_type = 'git',
  Enum['present', 'absent'] $ensure = 'present',
) {

  # NOTE: This resource is used by tails::weblate and we currently have no
  #       other way of letting Apache2 know of code changes, so we
  #       conditionally notify it here. Maybe it makes sense to refactor
  #       this resource into somewhere where the notification looks less
  #       hacky.
  $notify = (defined(Package['apache2']) and defined(Package['libapache2-mod-wsgi-py3']) and defined(Service['apache2'])) ? {
    true    => Service['apache2'],
    default => undef,
  }

  case $ensure {
    'absent': {
      exec { "pip_uninstall_${name}":
        command =>  "/usr/bin/pip3 uninstall --yes ${name}",
        require => Package[$tails::weblate::python_dependencies::pip_packages],
        unless  => "/usr/bin/test -z \"\$( /usr/bin/pip3 show '${name}' )\" ",
        notify  => $notify,
      }
    }
    default: {
      exec { "pip_install_${name}":
        command => "/usr/bin/pip3 install --upgrade --no-deps ${repo_type}+${url}@${git_tag}#egg=${name}-${version}",
        require => Package[$tails::weblate::python_dependencies::pip_packages],
        unless  => "/usr/bin/test '${version}' = \"\$(/usr/bin/pip3 show '${name}' | /usr/bin/awk -F': ' '/^Version: / {print \$2}')\"",
        notify  => $notify,
      }
    }
  }
}
