# Manage credentials for users who admin weblate
define tails::weblate::admin () {

  user::groups::manage_user { "${name}_in_weblate":
    user    => $name,
    group   => 'weblate',
    require => Group['weblate'],
  }

  user::groups::manage_user { "${name}_in_weblate_admin":
    user    => $name,
    group   => 'weblate_admin',
    require => Group['weblate_admin'],
  }

}
