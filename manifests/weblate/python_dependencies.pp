# Manage Weblate dependencies that are in not in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::python_dependencies () {

  $pip_packages = [
    'python3-pip',
  ]

  ensure_packages($pip_packages)

  class { 'tails::profile::rss2email':
    user         => 'weblate-rss2email',
    default_from => 'translate@lizard.tails.boum.org',
    default_to   => 'tails-weblate@boum.org',
  }

  # Dependencies for Weblate 3.11.3

  tails::weblate::python_module { 'amqp':
    version => '2.6.1',  # >=2.6.0, <=2.6.1, <2.7
    git_tag => 'v2.6.1',
    url     => 'https://github.com/celery/py-amqp',
    require => [Exec['pip_install_vine']],
  }

  tails::weblate::python_module { 'asgiref':
    version => '3.3.0',  # >=3.2.10, <4
    url     => 'https://github.com/django/asgiref/',
  }

  tails::weblate::python_module { 'backports.functools-lru-cache':
    version => '1.6.1',
    git_tag => 'v1.6.1',
    url     => 'https://github.com/jaraco/backports.functools_lru_cache',
  }

  tails::weblate::python_module { 'billiard':
    version => '3.6.3.0',  # >=3.6.3.0, <4.0
    url     => 'https://github.com/celery/billiard',
  }

  tails::weblate::python_module { 'bleach':
    version => '3.1.5',  # >=3.1.1, <3.2.0
    git_tag => 'v3.1.5',
    url     => 'https://github.com/mozilla/bleach',
    require => [Exec['pip_install_six']],
  }

  tails::weblate::python_module { 'borgbackup':
    version => '1.1.14',  # >=1.1.9, <1.2.0
    url     => 'https://github.com/borgbackup/borg',
  }

  tails::weblate::python_module { 'celery':
    version => '4.4.7',  # !=4.4.1, >=4.2.0, <4.5
    git_tag => 'v4.4.7',
    url     => 'https://github.com/celery/celery',
    require => [Exec['pip_install_billiard'], Exec['pip_install_kombu'], Exec['pip_install_redis'], Exec['pip_install_vine']],
  }

  tails::weblate::python_module { 'celery-batches':
    version => '0.3',  # >=0.2, <0.4
    git_tag => 'v0.2',
    url     => 'https://github.com/percipient/celery-batches',
    require => [Exec['pip_install_celery']],
  }

  tails::weblate::python_module { 'certifi':
    version => '2020.6.20',  # >=2017.4.17
    git_tag => '2020.06.20',
    url     => 'https://github.com/certifi/python-certifi',
  }

  tails::weblate::python_module { 'chardet':
    version => '3.0.4',  # >=3.0.2, <4
    url     => 'https://github.com/chardet/chardet',
  }

  tails::weblate::python_module { 'cheroot':
    version => '8.4.5',
    git_tag => 'v8.4.5',
    url     => 'https://github.com/cherrypy/cheroot',
    require => [Exec['pip_install_backports.functools-lru-cache'], Exec['pip_install_jaraco.functools'], Exec['pip_install_more-itertools'], Exec['pip_install_selectors2'], Exec['pip_install_six']],  # lint:ignore:140chars -- automatically generated
  }

  tails::weblate::python_module { 'cython':
    version => '0.29.21',  # >=0.29.14, <0.30
    url     => 'https://github.com/cython/cython',
  }

  tails::weblate::python_module { 'defusedxml':
    version => '0.6.0',  # >=0.5.0rc1
    git_tag => 'v0.6.0',
    url     => 'https://github.com/tiran/defusedxml',
  }

  tails::weblate::python_module { 'diff-match-patch':
    version => '20181111',  # ==20181111
    git_tag => 'v20181111',
    url     => 'https://github.com/diff-match-patch-python/diff-match-patch',
  }

  tails::weblate::python_module { 'django':
    version => '2.2.19',  # >=2.2, <3.0, <3.1
    url     => 'https://github.com/django/django',
    require => [Exec['pip_install_asgiref']],
  }

  tails::weblate::python_module { 'django-appconf':
    version => '1.0.4',  # >=1.0.0, <1.1
    git_tag => 'v1.0.4',
    url     => 'https://github.com/django-compressor/django-appconf',
    require => [Exec['pip_install_django']],
  }

  tails::weblate::python_module { 'django-compressor':
    version => '2.4',  # >=2.3, <2.5
    url     => 'https://github.com/django-compressor/django-compressor',
    require => [Exec['pip_install_django-appconf'], Exec['pip_install_rjsmin'], Exec['pip_install_six']],
  }

  tails::weblate::python_module { 'django-crispy-forms':
    version => '1.8.1',  # >=1.8.0, <1.9.0
    url     => 'https://github.com/django-crispy-forms/django-crispy-forms',
  }

  tails::weblate::python_module { 'django-redis':
    version => '4.11.0',  # >=4.10.0, <4.12.0
    url     => 'https://github.com/jazzband/django-redis',
    require => [Exec['pip_install_django'], Exec['pip_install_redis']],
  }

  tails::weblate::python_module { 'djangorestframework':
    version => '3.11.2',  # >=3.9.0, <3.12.0
    url     => 'https://github.com/encode/django-rest-framework',
    require => [Exec['pip_install_django']],
  }

  tails::weblate::python_module { 'filelock':
    version => '3.0.12',  # >=3.0.0, <3.1.0
    git_tag => 'v3.0.12',
    url     => 'https://github.com/benediktschmitt/py-filelock',
  }

  tails::weblate::python_module { 'gitdb':
    version => '4.0.5',  # >=4.0.1, <5
    url     => 'https://github.com/gitpython-developers/gitdb',
    require => [Exec['pip_install_smmap']],
  }

  tails::weblate::python_module { 'gitpython':
    version => '3.1.11',  # >=2.1.15, <3.2.0
    url     => 'https://github.com/gitpython-developers/GitPython',
    require => [Exec['pip_install_gitdb']],
  }

  tails::weblate::python_module { 'hiredis':
    version => '1.0.1',  # >=1.0.1, <1.1.0
    git_tag => 'v1.0.1',
    url     => 'https://github.com/redis/hiredis-py',
  }

  tails::weblate::python_module { 'html2text':
    version => '2020.1.16',  # >=2019.8.11, <2020.1.17
    url     => 'https://github.com/Alir3z4/html2text/',
  }

  tails::weblate::python_module { 'idna':
    version => '2.10',  # >=2.5, <3
    git_tag => 'v2.10',
    url     => 'https://github.com/kjd/idna',
  }

  tails::weblate::python_module { 'importlib-metadata':
    version => '2.0.0',  # >=0.18
    git_tag => 'v2.0.0',
    url     => 'https://gitlab.com/python-devs/importlib_metadata',
    require => [Exec['pip_install_zipp']],
  }

  tails::weblate::python_module { 'jaraco.functools':
    version => '2.0',  # ==2.0
    url     => 'https://github.com/jaraco/jaraco.functools',
    require => [Exec['pip_install_more-itertools']],
  }

  tails::weblate::python_module { 'jellyfish':
    version => '0.7.2',  # >=0.6.1, <0.8.0
    url     => 'https://github.com/jamesturk/jellyfish',
  }

  tails::weblate::python_module { 'kombu':
    version => '4.6.11',  # !=4.6.4, !=4.6.5, >=4.6.0, <=4.6.11
    git_tag => 'v4.6.11',
    url     => 'https://github.com/celery/kombu',
    require => [Exec['pip_install_amqp'], Exec['pip_install_importlib-metadata'], Exec['pip_install_redis']],
  }

  tails::weblate::python_module { 'lxml':
    version => '4.6.1',  # !=4.3.1, >=4.2
    git_tag => 'lxml-4.6.1',
    url     => 'https://github.com/lxml/lxml',
  }

  tails::weblate::python_module { 'misaka':
    version => '2.1.1',  # >=2.1.0, <2.2.0
    git_tag => 'v2.1.1',
    url     => 'https://github.com/FSX/misaka',
  }

  tails::weblate::python_module { 'more-itertools':
    version => '8.6.0',  # >=2.6
    git_tag => 'v8.6.0',
    url     => 'https://github.com/more-itertools/more-itertools',
  }

  tails::weblate::python_module { 'openpyxl':
    version   => '2.6.4',  # !=3.0.2, >=2.6.0, <3.0, <3.1
    url       => 'https://foss.heptapod.net/openpyxl/openpyxl',
    repo_type => 'hg',
  }

  tails::weblate::python_module { 'pycairo':
    version => '1.19.1',  # >=1.11.1, >=1.15.3, <=1.19.1
    git_tag => 'v1.19.1',
    url     => 'https://github.com/pygobject/pycairo',
  }

  tails::weblate::python_module { 'pygobject':
    version => '3.38.0',  # >=3.27.0
    url     => 'https://gitlab.gnome.org/GNOME/pygobject.git',
  }

  tails::weblate::python_module { 'python-dateutil':
    version => '2.8.1',  # >=2.8.1
    url     => 'https://github.com/dateutil/dateutil',
    require => [Exec['pip_install_six']],
  }

  tails::weblate::python_module { 'python3-openid':
    version => '3.2.0',  # >=3.0.10
    git_tag => 'v3.2.0',
    url     => 'https://github.com/necaris/python3-openid',
    require => [Exec['pip_install_defusedxml']],
  }

  tails::weblate::python_module { 'redis':
    version => '3.5.3',  # >=3.2.0
    url     => 'https://github.com/andymccurdy/redis-py',
  }

  tails::weblate::python_module { 'requests':
    version => '2.23.0',  # >=2.20.0, <2.24.0
    git_tag => 'v2.23.0',
    url     => 'https://github.com/psf/requests',
    require => [Exec['pip_install_certifi'], Exec['pip_install_chardet'], Exec['pip_install_idna'], Exec['pip_install_urllib3']],
  }

  tails::weblate::python_module { 'rjsmin':
    version => '1.1.0',  # ==1.1.0
    url     => 'https://github.com/ndparker/rjsmin',
  }

  tails::weblate::python_module { 'ruamel.yaml':
    version   => '0.16.12',  # >=0.15.0
    url       => 'http://hg.code.sf.net/p/ruamel-yaml/code',
    repo_type => 'hg',
  }

  tails::weblate::python_module { 'selectors2':
    version => '2.0.2',
    url     => 'https://github.com/sethmlarson/selectors2',
  }

  tails::weblate::python_module { 'sentry-sdk':
    version => '0.14.4',  # >=0.13.0, <0.15.0
    url     => 'https://github.com/getsentry/sentry-python',
  }

  tails::weblate::python_module { 'setuptools':
    version => '50.3.2.post20201105',  # >=36.0.1, >49.2
    git_tag => 'v50.3.2',
    url     => 'https://github.com/pypa/setuptools',
  }

  tails::weblate::python_module { 'siphashc':
    version => '1.3',  # >=1.2, <2.0
    git_tag => 'v1.3',
    url     => 'https://github.com/WeblateOrg/siphashc',
  }

  tails::weblate::python_module { 'six':
    version => '1.14.0',  # >=1.12.0, >=1.13.0, <1.15.0
    url     => 'https://github.com/benjaminp/six',
  }

  tails::weblate::python_module { 'smmap':
    version => '3.0.4',  # >=3.0.1, <4
    git_tag => 'v3.0.4',
    url     => 'https://github.com/gitpython-developers/smmap',
  }

  tails::weblate::python_module { 'social-auth-app-django':
    version => '3.1.0',  # ==3.1.0
    url     => 'https://github.com/python-social-auth/social-app-django',
    require => [Exec['pip_install_six'], Exec['pip_install_social-auth-core']],
  }

  tails::weblate::python_module { 'social-auth-core':
    version => '3.2.0',  # ==3.2.0
    url     => 'https://github.com/python-social-auth/social-core',
    require => [Exec['pip_install_defusedxml'], Exec['pip_install_python3-openid'], Exec['pip_install_requests'], Exec['pip_install_six']],
  }

  tails::weblate::python_module { 'translate-toolkit':
    version => '2.5.1',  # >=2.4.0, <2.6.0
    url     => 'https://github.com/translate/translate',
  }

  tails::weblate::python_module { 'translation-finder':
    version => '1.8',  # ==1.8, <=2.2
    url     => 'https://github.com/WeblateOrg/translation-finder.git',
  }

  tails::weblate::python_module { 'ua-parser':
    version => '0.10.0',  # >=0.10.0
    url     => 'https://github.com/ua-parser/uap-python',
  }

  tails::weblate::python_module { 'urllib3':
    version => '1.25.11',  # !=1.25.0, !=1.25.1, >=1.21.1, <1.26
    url     => 'https://github.com/urllib3/urllib3',
  }

  tails::weblate::python_module { 'user-agents':
    version => '2.1',  # >=2.0, <2.2
    git_tag => 'v2.1.0',
    url     => 'https://github.com/selwin/python-user-agents',
    require => [Exec['pip_install_ua-parser']],
  }

  tails::weblate::python_module { 'vine':
    version => '1.3.0',  # ==1.3.0, <=1.3.0
    git_tag => 'v1.3.0',
    url     => 'https://github.com/celery/vine',
  }

  tails::weblate::python_module { 'whoosh':
    version => '2.7.4',  # ==2.7.4
    git_tag => 'v2.7.4',
    url     => 'https://github.com/whoosh-community/whoosh',
  }

  tails::weblate::python_module { 'zipp':
    version => '1.2.0',  # >=0.5, ==1.2.0
    git_tag => 'v1.2.0',
    url     => 'https://github.com/jaraco/zipp',
  }

}
