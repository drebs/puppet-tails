# A copy of the Tails website to preview translations
class tails::weblate::staging_website(
  Stdlib::Absolutepath $mutable_data_dir,
  Hash $po_slave_languages,
) {

  include tails::website::builder

  $src_dir = "${mutable_data_dir}/repositories/vcs/staging/wiki/src"
  $git_dir = ''
  $web_dir = '/var/www/staging'
  $url = 'https://staging.tails.boum.org'
  $cgi_url = "${url}/ikiwiki.cgi"
  $is_staging = true
  $underlays = []

  file { "${mutable_data_dir}/config/ikiwiki.setup":
    content => template('tails/website/ikiwiki.setup.erb'),
    mode    => '0644',
    owner   => weblate,
    group   => weblate_admin,
  }

}
