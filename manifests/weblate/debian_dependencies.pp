# Manage Weblate dependencies that are in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::debian_dependencies () {

  $packages = [
    'ccze', 'ipython3',  # for more convenient debugging
    'mercurial',  # so we can use pip to install from hg repos
    # Dependencies for Weblate 3.11.3
    'gir1.2-pango-1.0',
    'libacl1-dev',
    'libcairo2-dev',
    'libgirepository1.0-dev',
    'libglib2.0-dev',
    'libssl-dev',
    'libxml2-dev',
    'libxslt1-dev',
    'pkg-config',
    'python3-cryptography',
    'python3-et-xmlfile',
    'python3-jdcal',
    'python3-jwt',
    'python3-levenshtein',
    'python3-memcache',
    'python3-mysqldb',
    'python3-oauthlib',
    'python3-openid',
    'python3-packaging',
    'python3-pil',
    'python3-pyuca',
    'python3-rcssmin',
    'python3-requests-oauthlib',
    'python3-sqlparse',
    'python3-tz',
    'python3-webencodings',
    'python3-yaml',
    'sqlite3',
  ]

  ensure_packages($packages)

}
