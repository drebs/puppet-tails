# Install a Python module
define tails::weblate::python_module (
  Optional[String] $version,
  Optional[String] $url,
  Optional[String] $git_tag = $version,
  String $repo_type = 'git',
  Enum['present', 'absent'] $ensure = 'present',
) {

  tails::weblate::pip_install_from_repo { $name:
    ensure    => $ensure,
    version   => $version,
    url       => $url,
    git_tag   => $git_tag,
    repo_type => $repo_type,
  }

  if $url =~ /^https:\/\/github\.com/ {                                                # GitHub
    $feed_url = "${url}/releases.atom"
  } elsif $url =~ /^https:\/\/gitlab\./ or $url =~ /^https:\/\/foss\.heptapod\.net/ {  # GitLab
    $feed_url = "${url}/-/tags?format=atom"
  } else {
    $feed_url = undef
  }

  if $feed_url != undef {
    tails::profile::rss2email::feed { "Weblate Python dependency ${name} release security feed":
      url     => $feed_url,
      filters => { 'drop_non_security' => $ensure },
    }
  }
}
