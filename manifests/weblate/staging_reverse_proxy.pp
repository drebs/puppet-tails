# Manage a nginx vhost that reverse proxies to a weblate instance.
class tails::weblate::staging_reverse_proxy (
  Stdlib::Fqdn $public_hostname     = 'staging.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname   = 'translate',
  Stdlib::Port $upstream_port       = 80,
  ) {

  $nginx_includename = 'tails_staging_reverse_proxy'
  $nginx_nophp = true
  $upstream_address = "${upstream_hostname}:${upstream_port}"

  include tails::website::webserver::rewrite_rules

  $additional_vhost_config = 'include /etc/nginx/include.d/tails_website_rewrite_rules.conf;'
  nginx::vhostsd { $public_hostname:
    content => template('tails/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included[$nginx_includename],
      Nginx::Included['tails_website_rewrite_rules'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { $nginx_includename:
    content => template('tails/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
