# Scripts for integration with Tails website
class tails::weblate::scripts(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $code_git_checkout,
  String $logs_dir,
  Hash $production_slave_languages,
  Hash $weblate_additional_languages,
) {

  file { "${mutable_data_dir}/scripts":
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2770',
  }

  # script for upgrading the core pages component list in Weblate

  file { "${mutable_data_dir}/scripts/update_core_pages.py":
    source  => 'puppet:///modules/tails/weblate/scripts/update_core_pages.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File[
      "${mutable_data_dir}/config/updateCorePages.conf",
      "${mutable_data_dir}/scripts/tailsWeblate.py"
    ],
  }

  file { "${mutable_data_dir}/config/updateCorePages.conf":
    source => 'puppet:///modules/tails/weblate/config/updateCorePages.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  # scripts for creating temporary git repository with all suggestions for staging wiki

  file { "${mutable_data_dir}/scripts/save-suggestions.py":
    source  => 'puppet:///modules/tails/weblate/scripts/save-suggestions.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File["${mutable_data_dir}/config/saveSuggestions.conf"],
  }

  file { "${mutable_data_dir}/config/saveSuggestions.conf":
    source => 'puppet:///modules/tails/weblate/config/saveSuggestions.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  # script for upgrading git repository in weblate

  file { "${mutable_data_dir}/scripts/update_weblate_components.py":
    source  => 'puppet:///modules/tails/weblate/scripts/update_weblate_components.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File[
      "${mutable_data_dir}/config/updateWeblateComponents.conf",
      "${mutable_data_dir}/scripts/tailsWeblate.py",
      "${mutable_data_dir}/scripts/merge_canonical_changes.py"
    ],
  }

  file { "${mutable_data_dir}/config/updateWeblateComponents.conf":
    source => 'puppet:///modules/tails/weblate/config/updateWeblateComponents.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  # script for updating the weblate git repository from main git

  file { "${mutable_data_dir}/scripts/merge_canonical_changes.py":
    source  => 'puppet:///modules/tails/weblate/scripts/merge_canonical_changes.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File["${mutable_data_dir}/config/mergeCanonicalChanges.conf"],
  }

  file { "${mutable_data_dir}/config/mergeCanonicalChanges.conf":
    source => 'puppet:///modules/tails/weblate/config/mergeCanonicalChanges.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  file { "${mutable_data_dir}/config/langs.json":
    content => template('tails/weblate/langs.json.erb'),
    mode    => '0640',
    owner   => weblate,
    group   => weblate_admin,
  }

  # script for updating the weblate git repository from main git

  file { "${mutable_data_dir}/scripts/merge_weblate_changes.py":
    source  => 'puppet:///modules/tails/weblate/scripts/merge_weblate_changes.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File[
      "${mutable_data_dir}/config/mergeWeblateChanges.conf",
      "${mutable_data_dir}/scripts/merge_canonical_changes.py"
    ],
  }

  file { "${mutable_data_dir}/config/mergeWeblateChanges.conf":
    source => 'puppet:///modules/tails/weblate/config/mergeWeblateChanges.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  # script to check/update Weblate components against git.

  file { "${mutable_data_dir}/scripts/weblate_status.py":
    source => 'puppet:///modules/tails/weblate/scripts/weblate_status.py',
    mode   => '0750',
    owner  => weblate,
    group  => weblate_admin,
  }

  # commonly used functions to handle Weblate components.

  file { "${mutable_data_dir}/scripts/tailsWeblate.py":
    source => 'puppet:///modules/tails/weblate/scripts/tailsWeblate.py',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  # script to check Weblate role permissions.

  file { "${mutable_data_dir}/scripts/weblate_permissions.py":
    source  => 'puppet:///modules/tails/weblate/scripts/weblate_permissions.py',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File[
      "${mutable_data_dir}/config/weblate_permissions.conf",
      "${mutable_data_dir}/config/weblate_permissions.yaml",
      "${mutable_data_dir}/scripts/tailsWeblate.py"
    ],
  }

  file { "${mutable_data_dir}/config/weblate_permissions.conf":
    source => 'puppet:///modules/tails/weblate/config/weblate_permissions.conf',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  file { "${mutable_data_dir}/config/weblate_permissions.yaml":
    source => 'puppet:///modules/tails/weblate/config/weblate_permissions.yaml',
    mode   => '0640',
    owner  => weblate,
    group  => weblate_admin,
  }

  cron { 'Check & enforce Weblate permissions model':
    command => "${mutable_data_dir}/scripts/weblate_permissions.py --enforce",
    user    => weblate,
    hour    => 5,
    minute  => 21,
    require => File[
      "${mutable_data_dir}/scripts/weblate_permissions.py"
    ],
  }

  # Cronjobs

  file { "${mutable_data_dir}/scripts/update-staging-website.py":
    source => 'puppet:///modules/tails/weblate/scripts/update-staging-website.py',
    mode   => '0750',
    owner  => weblate,
    group  => weblate_admin,
  }

  cron { 'weblate update staging':
    command => "${mutable_data_dir}/scripts/update-staging-website.py",
    user    => weblate,
    hour    => 3,
    minute  => 21,
    require => File["${mutable_data_dir}/scripts/update-staging-website.py"],
  }

  ensure_packages([ 'bash' ])

  file { "${mutable_data_dir}/scripts/cron.sh":
    source  => 'puppet:///modules/tails/weblate/scripts/cron.sh',
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => [
      Vcsrepo[
        "${mutable_data_dir}/repositories/vcs/tails/index",
        "${mutable_data_dir}/repositories/integration",
        $code_git_checkout,
      ],
      File[
        $logs_dir,
        "${mutable_data_dir}/scripts/merge_canonical_changes.py",
        "${mutable_data_dir}/scripts/update_weblate_components.py",
        "${mutable_data_dir}/scripts/merge_weblate_changes.py",
      ],
      Package['bash'],
    ],
  }

  cron { 'weblate cronjobs':
    command => "cd ${mutable_data_dir}/scripts && [ -f ${mutable_data_dir}/config/.maintenance ] || ./cron.sh",
    user    => weblate,
    minute  => '15',
    require => File["${mutable_data_dir}/scripts/cron.sh"],
  }

}
