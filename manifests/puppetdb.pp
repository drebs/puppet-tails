# Manage PuppetDB
class tails::puppetdb () {

  apt::sources_list { 'unstable.list':
    ensure  => present,
    content => "deb http://ftp.us.debian.org/debian/ unstable main\n",
  }

  apt::preferences_snippet { 'clojure-libs':
    ensure   => present,
    package  => 'lib*-clojure',
    pin      => 'release o=Debian,n=buster',
    priority => '991',
  }

  apt::preferences_snippet { 'java-libs':
    ensure   => present,
    package  => 'lib*-java',
    pin      => 'release o=Debian,n=buster',
    priority => '991',
  }

  $pinned_to_sid = [
    'libcomidi-clojure',
    'libdujour-version-check-clojure',
    'libpantomime-clojure',
    'libpuppetlabs-http-client-clojure',
    'libpuppetlabs-ring-middleware-clojure',
    'libssl-utils-clojure',
    'libtrapperkeeper-metrics-clojure',
    'libtrapperkeeper-status-clojure',
    'libtrapperkeeper-webserver-jetty9-clojure',
    'libtika-java',
  ]
  apt::preferences_snippet { $pinned_to_sid:
    ensure   => present,
    pin      => 'release o=Debian,n=sid',
    priority => '991',
  }

  package { 'postgresql': }
  apt::package { 'puppetdb':
    pin     => 'release o=Debian,n=sid',
    require => [
      Package['postgresql'],
      Apt::Preferences_snippet['clojure-libs', 'java-libs', $pinned_to_sid],
    ],
  }

  exec { 'copy TLS files':
    command => "cp -a /var/lib/puppet/ssl/certs/${::fqdn}.pem /etc/puppetdb/cert.pem && cp -a /var/lib/puppet/ssl/private_keys/${::fqdn}.pem /etc/puppetdb/private_key.pem && cp -a /var/lib/puppet/ssl/ca/ca_crt.pem /etc/puppetdb/ca_crt.pem && chown puppetdb:puppetdb /etc/puppetdb/*.pem", # lint:ignore:140chars -- command
    require => [
      Class['::puppet::master'],
      Apt::Package['puppetdb'],
    ],
  }

  file_line { 'ssl-host':
    path   => '/etc/puppetdb/conf.d/jetty.ini',
    line   => "ssl-host = ${::fqdn}",
    match  => '#?\s*ssl-host\s*=\s*',
    notify => Service['puppetdb'],
  }

  file_line { 'ssl-port':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-port = 8081',
    match   => '#?\s*ssl-port\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-key':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-key = /etc/puppetdb/private_key.pem',
    match   => '#?\s*ssl-key\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-cert':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-cert = /etc/puppetdb/cert.pem',
    match   => '#?\s*ssl-cert\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-ca-cert':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-ca-cert = /etc/puppetdb/ca_crt.pem',
    match   => '#?\s*ssl-ca-cert\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  service { 'puppetdb':
    ensure   => running,
    enable   => true,
    provider => systemd,
    require  => File_line['ssl-port', 'ssl-key', 'ssl-cert', 'ssl-ca-cert'],
  }

  # Workaround Debian#881584
  file { '/etc/logrotate.d/puppetdb':
    ensure  => absent,
    require => Apt::Package['puppetdb'],
  }

}
