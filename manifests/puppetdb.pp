# Manage PuppetDB
class tails::puppetdb () {

  $pinned_version = {
    'libtrapperkeeper-webserver-jetty9-clojure' => '1.7.0-3',
  }
  $pinned_version.each |String $package, String $version| {
    apt::pin { $package:
      packages => $package,
      version  => $version,
      priority => 1000,
    }
  }

  package { 'glassfish-javaee': }
  package { 'postgresql': }
  package { 'puppetdb':
    require => [
      Package['postgresql'],
      Apt::Pin['puppetdb'],
    ],
  }
  apt::pin { 'puppetdb':
    ensure   => absent,
    packages => 'puppetdb',
    version  => '6.2.0-4',
    priority => 1000,
  }

  exec { 'copy TLS files':
    command => "cp -a /var/lib/puppet/ssl/certs/${::fqdn}.pem /etc/puppetdb/cert.pem && cp -a /var/lib/puppet/ssl/private_keys/${::fqdn}.pem /etc/puppetdb/private_key.pem && cp -a /var/lib/puppet/ssl/ca/ca_crt.pem /etc/puppetdb/ca_crt.pem && chown puppetdb:puppetdb /etc/puppetdb/*.pem", # lint:ignore:140chars -- command
    require => [
      Class['::puppet::master'],
      Package['puppetdb'],
    ],
  }

  file_line { 'ssl-host':
    path   => '/etc/puppetdb/conf.d/jetty.ini',
    line   => "ssl-host = ${::fqdn}",
    match  => '#?\s*ssl-host\s*=\s*',
    notify => Service['puppetdb'],
  }

  file_line { 'ssl-port':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-port = 8081',
    match   => '#?\s*ssl-port\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-key':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-key = /etc/puppetdb/private_key.pem',
    match   => '#?\s*ssl-key\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-cert':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-cert = /etc/puppetdb/cert.pem',
    match   => '#?\s*ssl-cert\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  file_line { 'ssl-ca-cert':
    path    => '/etc/puppetdb/conf.d/jetty.ini',
    line    => 'ssl-ca-cert = /etc/puppetdb/ca_crt.pem',
    match   => '#?\s*ssl-ca-cert\s*=\s*',
    require => Exec['copy TLS files'],
    notify  => Service['puppetdb'],
  }

  service { 'puppetdb':
    ensure   => running,
    enable   => true,
    provider => systemd,
    require  => File_line['ssl-port', 'ssl-key', 'ssl-cert', 'ssl-ca-cert'],
  }
}
