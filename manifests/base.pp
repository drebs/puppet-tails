# Base class for team-admin'd systems

class tails::base (
  Boolean $is_puppet_master   = false,
  Boolean $manage_grub        = true,
  Boolean $monitoring_agent   = true,
  String $root_mail_recipient = 'tails-sysadmins@boum.org',
  Boolean $shorewall          = false,
  String $sshd_tcp_forwarding = 'no',
  Boolean $has_nvme           = false,
) {

  #
  # Sanity checks
  #

  validate_email_address($root_mail_recipient)

  if $::operatingsystem != 'Debian' {
    fail('This module only supports Debian.')
  }

  #
  # Include external classes
  #

  include augeas
  include bash
  include etckeeper
  include haveged
  include loginrecords
  class { '::molly_guard': always_query_hostname => true }
  class { 'sshd':
    tcp_forwarding => $sshd_tcp_forwarding,
  }
  if ! $is_puppet_master {
    if $::fqdn =~ /^isotester[0-9]+\./ {
      include puppet::oneshot
    } else {
      include puppet::cron
    }
  }
  class { 'sudo':
    config_file_replace => false,
  }
  if $::osfamily == 'Debian' {
    include ::tails::apt
  }
  if $manage_grub {
    include ::tails::grub
  }

  if $monitoring_agent {
    include ::tails::monitoring::agent
  } else {
    # Systems that use tails::monitoring::satellite can't re-declare t::m::agent
    if $::fqdn != 'monitor.lizard' and $::fqdn != 'ecours.tails.boum.org' {
      class { '::tails::monitoring::agent': ensure => absent }
    }
  }

  if $shorewall {
    include ::shorewall
    include ::tails::shorewall
  } else {
    package { ['shorewall', 'shorewall-core']: ensure => purged }
  }

  include ::tor::daemon
  include vim

  #
  # Packages
  #

  $base_packages     = [
    apparmor,
    apparmor-profiles-extra,
    apparmor-utils,
    bzip2,
    ca-certificates,
    gnupg,
    htop,
    iotop,
    iproute,
    iputils-ping,
    less,
    linux-image-amd64,
    lvm2,
    nload,
    pinentry-curses,
    rdiff-backup,
    safe-rm,
    sash,
    screen,
    sysstat,
    virt-what,
    w3m,
    zsh,
  ]
  $physical_packages = [
    hwloc-nox,
    intel-microcode,
    memlockd,
    'memtest86+',
    numactl,
    parted,
  ]

  ensure_packages($base_packages)

  package { 'pinentry-gtk2': ensure => absent }
  package { 'apparmor-profiles': ensure => absent }

  case $::virtual {
    physical: {
      include smartmontools
      ensure_packages($physical_packages) }
    default:  {
      package { 'smartmontools': ensure => absent }
    }
  }

  #
  # Email settings
  #

  class { '::postfix':
    root_mail_recipient => $root_mail_recipient,
    manage_tls_policy   => 'yes',
  }

  postfix::config {
    'smtp_tls_CApath':
      value   => '/etc/ssl/certs',
      require => [Package['ca-certificates'], Exec['c_rehash']];
    'smtp_tls_ciphers':              value => 'high';
    'smtp_tls_mandatory_protocols':  value => 'TLSv1.2';
    'smtp_tls_mandatory_ciphers':    value => 'high';
    'smtp_tls_mandatory_exclude_ciphers':
      value => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA';
    'smtp_tls_protocols':            value => 'TLSv1.2';
  }

  $letsencrypt_cafile = 'Lets-Encrypt-Authority-X3.pem'
  file { "/etc/ssl/certs/${letsencrypt_cafile}":
    source => "puppet:///modules/tails/ssl/certs/${letsencrypt_cafile}",
    owner  => root,
    group  => root,
    mode   => '0644',
  }
  -> postfix::tlspolicy_snippet {
    'boum.org:25':
      value => "secure tafile=/etc/ssl/certs/${letsencrypt_cafile}";
    '[boum.org]:25':
      value => "secure tafile=/etc/ssl/certs/${letsencrypt_cafile}";
    'boum.org:587':
      value => "secure tafile=/etc/ssl/certs/${letsencrypt_cafile}";
    '[boum.org]:587':
      value => "secure tafile=/etc/ssl/certs/${letsencrypt_cafile}";
  }

  exec { 'c_rehash': refreshonly => true }

  # IPv6 is disabled on some of our systems
  postfix::config {
    'mynetworks':     value => '127.0.0.0/8';
    'inet_protocols': value => 'ipv4';
  }

  #
  # Host keys
  #

  sshkey { 'git.tails.boum.org':
    ensure => present,
    type   => 'rsa',
    key    => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAzNSbEF/Qb421EZ353Vt2KhXGJbs1DO4vDoGWn3wyBxODsFluafNytJTI0Q24GmvNTHpovWyI079pluUubzHxplMmE+3xYyTMRzXU4p6oMHS2VU594b1rdjZjmPrB0TuthaZRaeVljQlwD6ormDoDZ8nntFWEHgkKEa/ca3dKNbABJgnm+MfRIWYc6oddlVUpy2CbjPy2N34etmMtAr1XdZBYoHurYUH8Z8Bmrrx+AMlsLUl+vDYnM3oLcOoBRi1OXrmIX4M7Ep8JaJpg23xEsu3VDc7kzCw8cxRydQh41ohZLo4WsTCNZ70mheJlkLjg09GnGEOyXw9dwe5VGTKWDQ==', # lint:ignore:140chars -- SSH key
  }

  #
  # Miscellaneous settings
  #

  user { 'root': ensure => present }
  file { '/root':
    ensure => directory,
    owner  => root,
    group  => staff,
    mode   => '0750',
  }

  augeas { 'rcS-FSCKFIX':
    context => '/files/etc/default/rcS', changes => 'set FSCKFIX yes';
  }

  sudo::conf {'group-sudo':
    content => "%sudo	ALL=(ALL:ALL) NOPASSWD : ALL",
  }

  sysctl::value { 'kernel.panic': value => 10 }
  sysctl::value { 'kernel.perf_event_paranoid': value => 2 }
  sysctl::value { 'kernel.unprivileged_bpf_disabled': value => 1 }

  class { '::timezone': region => 'Etc', locality => 'UTC' }

  package { 'acpid':        ensure => purged }
  package { 'debian-faq':   ensure => purged }
  package { 'doc-debian':   ensure => purged }
  package { 'icinga2-doc':  ensure => purged }
  package { 'task-english': ensure => purged }

  # Use the deadline I/O scheduler for all non-rotational disks
  if $::virtual == 'physical' {
    file { '/etc/systemd/system/tweak-io-scheduler.service':
      owner   => root,
      group   => root,
      mode    => '0644',
      content => '[Unit]
Description=Tweak per-disk block I/O scheduler

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
ExecStart=/bin/sh -c \'for sysblock in /sys/block/nvme* /sys/block/sd* ; do \
                          if [ -d "$sysblock" ] && [ $(cat $sysblock/queue/rotational) = 0 ] ; then \
                             echo deadline > $sysblock/queue/scheduler ; \
                          fi ; \
                       done\'
[Install]
WantedBy=multi-user.target
',
    }
    service { 'tweak-io-scheduler':
      ensure    => running,
      provider  => systemd,
      require   => File['/etc/systemd/system/tweak-io-scheduler.service'],
      subscribe => File['/etc/systemd/system/tweak-io-scheduler.service'],
    }
    exec { 'systemctl enable tweak-io-scheduler.service':
      creates => '/etc/systemd/system/multi-user.target.wants/tweak-io-scheduler.service',
      require => Service[tweak-io-scheduler],
    }
  }

  # Enable TRIM/discard for LVM (#11788)
  augeas { 'lvm.conf_issue_discards':
    context => '/files/etc/lvm/lvm.conf',
    changes => 'set devices/dict/issue_discards/int 1',
    require => Package[lvm2],
  }

  # Manage CPU frequency settings
  if $::virtual == 'physical' {
    include ::tails::cpufreq
  }

  # Install tools to control NMVe drives when needed
  if $has_nvme {
    ensure_packages(['nvme-cli'])
  }

  file { [
    '/opt/puppetlabs',
    '/opt/puppetlabs/puppet',
    '/opt/puppetlabs/puppet/cache',
    '/opt/puppetlabs/puppet/share',
    '/opt/puppetlabs/puppet/share/augeas',
  ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  package { 'ntp': ensure => absent }
  include ::tails::timesyncd

  # Tor
  tor::daemon::hidden_service { 'ssh-hidden':
    ports => [ '22' ],
  }

}
