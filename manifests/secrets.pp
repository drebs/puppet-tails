class tails::secrets (
  Stdlib::Absolutepath $basedir = '/mnt/crypt',
  ) {

  $placeholder = "${basedir}/.placeholder"

  file {
    $basedir:
      ensure => directory,
      mode   => '0755',
      owner  => root,
      group  => root;
    $placeholder:
      ensure  => present,
      mode    => '0644',
      owner   => root,
      group   => root,
      require => Mount[$basedir];
  }

}
