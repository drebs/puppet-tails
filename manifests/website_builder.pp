# Manage what's needed to build the Tails website
class tails::website_builder () {

  ### Sanity checks

  if $::lsbdistcodename !~ /^stretch|sid$/ {
    fail('This module only supports Debian Stretch and sid.')
  }

  ### Resources

  if !defined(Apt::Sources_list["tails-builder-${::lsbdistcodename}.list"]) {
    apt::sources_list { "tails-builder-${::lsbdistcodename}.list":
      content => "deb http://deb.tails.boum.org/ builder-${::lsbdistcodename} main\n",
    }
  }

  $packages = [
    'ikiwiki',
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'perlmagick',
    'po4a',
  ]

  ensure_packages($packages)

}
