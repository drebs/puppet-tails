class tails::pdns (
  String $mysql_password,
  String $mysql_root_password,
  String $mysql_primary_server,
  String $mysql_xerox_password,
  String $mysql_server_id,
  String $pdns_api_key,
  Boolean $is_master,
  String $default_soa_name    = 'tails-ns-1.boum.org',
) {

  class { 'powerdns':
    db_password           => $mysql_password,
    db_root_password      => $mysql_root_password,
    custom_repo           => true,
    backend_install       => false,
    backend_create_tables => false,
  }

  # don't use debian default pdns config
  file { '/etc/powerdns/pdns.d/pdns.local.gmysql.conf':
    ensure => absent,
  }

  if $is_master {
    class {'::mysql::server':
      root_password    => $mysql_root_password,
      override_options => {
        'mysqld' => {
          'bind_address'            => '0.0.0.0',
          'server_id'               => $mysql_server_id,
          'log-slave-updates'       => 'ON',
          'sync_binlog'             => 1,
          'log-bin'                 => '/var/log/mysql-bin',
          'read_only'               => 'OFF',
          'binlog-format'           => 'ROW',
          'log-error'               => '/var/log/mysql/error.log',
          'innodb_buffer_pool_size' => '75M',
        },
      },
    }

    mysql_user { 'xerox@%':
      ensure        => 'present',
      password_hash => mysql_password($mysql_xerox_password)
    }

    mysql_grant { 'xerox@%/*.*':
      ensure     => 'present',
      privileges => ['REPLICATION SLAVE'],
      table      => '*.*',
      user       => 'xerox@%',
    }

    powerdns::config { 'api':
      ensure  => present,
      setting => 'api',
      value   => 'yes',
      type    => 'authoritative',
    }

    powerdns::config { 'api-key':
      ensure  => present,
      setting => 'api-key',
      value   => $pdns_api_key,
      type    => 'authoritative',
    }

    powerdns::config { 'default-soa-name':
      ensure  => present,
      setting => 'default-soa-name',
      value   => $default_soa_name,
      type    => 'authoritative',
    }
  }

  else {

    class {'::mysql::server':
      root_password    => $mysql_root_password,
      override_options => {
        'mysqld' => {
          'server_id'               => $mysql_server_id,
          'read_only'               => 'ON',
          'log-error'               => '/var/log/mysql/error.log',
          'innodb_buffer_pool_size' => '75M',
        },
      },
    }

    exec { 'change master':
      path    => '/usr/bin:/usr/sbin:/bin',
      command => "mysql --defaults-extra-file=/root/.my.cnf -e \"STOP SLAVE; CHANGE MASTER TO MASTER_HOST = '${mysql_primary_server}', MASTER_USER = 'xerox', MASTER_PASSWORD = '${mysql_xerox_password}', MASTER_USE_GTID = slave_pos; START SLAVE;\"",
      unless  => "mysql --defaults-extra-file=/root/.my.cnf -e 'SHOW SLAVE STATUS\G' | grep 'Slave_SQL_Running: Yes'",
    }
  }

}
