# Manage a private Prosody XMPP server behind a Tor onion service
class tails::prosody (
  Array[String] $admins,
  Stdlib::Absolutepath $prosody_config_dir = '/etc/prosody',
) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Resources

  tor::daemon::hidden_service { 'xmpp-hidden':
    ports   => [ '5222' ],
  }

  ensure_packages([
    'prosody', 'ssl-cert', 'ca-certificates', 'lua-dbi-sqlite3'
  ])

  user { 'prosody_admin':
    ensure => present,
    system => true,
    gid    => 'prosody_admin',
  }

  group { 'prosody_admin':
    ensure => present,
    system => true,
  }

  tails::prosody::admin { $admins: }

  sudo::conf { 'prosody-admin':
    source  => 'puppet:///modules/tails/prosody/sudo/prosody-admin',
    require => Group['prosody_admin'],
  }

  $config_dirs = [
    $prosody_config_dir,
    "${prosody_config_dir}/certs",
    "${prosody_config_dir}/conf.avail",
    "${prosody_config_dir}/conf.d",
  ]

  apt::dpkg_statoverride { $config_dirs:
    user  => 'prosody_admin',
    group => 'prosody',
    mode  => '2750',
  }

  $config_files = [
    "${prosody_config_dir}/prosody.cfg.lua",
    "${prosody_config_dir}/migrator.cfg.lua",
  ]

  apt::dpkg_statoverride { $config_files:
    user  => 'prosody_admin',
    group => 'prosody',
    mode  => '640',
  }

}
