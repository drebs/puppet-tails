# Manage a custom Diffie-Hellman group
define tails::dhparam (
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Absolutepath $path        = $name,
  Integer $size                     = 2048,
  Pattern[/\A[a-z_]+\z/] $owner     = 'root',
  Pattern[/\A[a-z_]+\z/] $group     = 'root',
  Stdlib::Filemode $mode            = '0644',
) {

  validate_integer($size, '', 1) # positive integer

  exec { "generate_dhparam_${path}":
    command => "openssl dhparam -out ${path} ${size}",
    user    => $owner,
    group   => $group,
    creates => $path,
  }

  file { $path:
      ensure  => $ensure,
      owner   => $owner,
      group   => $group,
      mode    => $mode,
      require => Exec["generate_dhparam_${path}"];
  }

}
