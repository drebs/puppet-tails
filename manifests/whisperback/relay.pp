# Manage a SMTP relay used by WhisperBack to send email to Tails help desk
class tails::whisperback::relay {

  include tails_secrets_whisperback

  augeas { 'loopback_interface' :
    context => '/files/etc/network/interfaces',
    changes =>
    [
      "set auto[child::1 = 'lo']/1 lo",
      "set iface[. = 'lo'] lo",
      "set iface[. = 'lo']/family inet",
      "set iface[. = 'lo']/method loopback",
      "set iface[. = 'lo']/up 'ip addr add 127.0.0.2/32 dev lo'",
    ],
  }

  $secrets_basedir = $tails_secrets_whisperback::basedir
  $onion_src_dir   = $tails_secrets_whisperback::onion_dir
  $onion_dir       = '/var/lib/tor/tails_whisperback_relay'
  $ssl_src_dir     = $tails_secrets_whisperback::ssl_dir
  $ssl_dir         = '/etc/ssl/tails_whisperback_relay'
  $postfix_instance_conf_dir = '/etc/postfix-hidden'

  mount {
    $onion_dir:
      ensure  => 'mounted',
      device  => $onion_src_dir,
      fstype  => 'none',
      options => 'bind',
      require => [
        File[$onion_src_dir],
        File[$onion_dir],
      ];
    $ssl_dir:
      ensure  => 'mounted',
      device  => $ssl_src_dir,
      fstype  => 'none',
      options => 'bind',
      require => [
        File[$ssl_src_dir],
        File[$ssl_dir],
      ];
  }

  include tor::daemon

  tor::daemon::hidden_service { 'tails_whisperback_relay':
    ports   => [ '25 127.0.0.2:25' ],
    require => [
      Service['postfix@postfix-hidden'],
      Mount[$onion_dir],
    ],
  }

  postfix::config {
    'multi_instance_wrapper':
      # lint:ignore:single_quote_string_with_variables -- Postfix variable
      value => '${command_directory}/postmulti -p --}';
      # lint:endignore
    'multi_instance_enable':
      value => 'yes';
    'multi_instance_directories':
      value   => $postfix_instance_conf_dir,
      require => File[$postfix_instance_conf_dir];
  }

  service { 'postfix@postfix-hidden':
    ensure  => running,
    enable  => true,
    require => [
      Service['postfix'],
      Postfix::Config['multi_instance_directories'],
      File["${postfix_instance_conf_dir}/dynamicmaps.cf"],
    ],
  }

  file { $postfix_instance_conf_dir:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755';
  }

  # Mountpoints
  file { [ $onion_dir, $ssl_dir ]: ensure => directory; }

  postfix::hash { "${postfix_instance_conf_dir}/access":
    source  => 'puppet:///modules/tails/whisperback/relay/access',
    require => File[$postfix_instance_conf_dir]
  }

  file {
    "${postfix_instance_conf_dir}/aliases":
      content => template('tails/whisperback/relay/aliases.erb'),
      require => File[$postfix_instance_conf_dir],
      notify  => Exec["generate ${postfix_instance_conf_dir}/aliases.db"];
    "${postfix_instance_conf_dir}/aliases.db":
      ensure  => present,
      require => [
        File["${postfix_instance_conf_dir}/aliases"],
        Exec["generate ${postfix_instance_conf_dir}/aliases.db"]
      ];
  }
  exec { "generate ${postfix_instance_conf_dir}/aliases.db":
    command     => "postalias ${postfix_instance_conf_dir}/aliases",
    refreshonly => true,
    require     => Package['postfix'],
    subscribe   => File["${postfix_instance_conf_dir}/aliases"],
  }

  file {
    "${postfix_instance_conf_dir}/master.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/tails/whisperback/relay/master.cf',
      notify  => Service['postfix@postfix-hidden'],
      require => Package['postfix'];

    "${postfix_instance_conf_dir}/main.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/tails/whisperback/relay/main.cf',
      notify  => Service['postfix@postfix-hidden'],
      require => [
        Package['postfix'],
        Mount[$ssl_dir],
        File['/var/lib/postfix-hidden'],
        File['/var/spool/postfix-hidden'],
        File["${postfix_instance_conf_dir}/access.db"],
        File["${postfix_instance_conf_dir}/aliases.db"],
      ];
    '/var/lib/postfix-hidden':
      ensure => directory,
      owner  => 'postfix',
      group  => 'postfix',
      mode   => '0755';
    '/var/spool/postfix-hidden':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    "${postfix_instance_conf_dir}/dynamicmaps.cf":
      ensure => link,
      target => '/etc/postfix/dynamicmaps.cf',
      notify => Service['postfix@postfix-hidden'];
  }

  exec {
    'copy_postfix_chroot':
      command => 'cp -a /var/spool/postfix/etc /var/spool/postfix-hidden/etc',
      onlyif  => 'test ! -d /var/spool/postfix-hidden/etc',
      require => File['/var/spool/postfix-hidden'];
  }

  exec {
    'compile_sasl_passwd':
      command     => "postmap ${tails_secrets_whisperback::postfix_dir}/sasl_passwd",
      subscribe   => File["${tails_secrets_whisperback::postfix_dir}/sasl_passwd"],
      refreshonly => true,
  }

  @@::tails::monitoring::service::postfix_mailqueue { "hidden_mailqueue@${::tails::monitoring::config::nodename}":
    nodename   => $::tails::monitoring::config::nodename,
    zone       => $::tails::monitoring::config::zone,
    tag        => $::tails::monitoring::config::parent_zone,
    config_dir => $postfix_instance_conf_dir,
  }

}
