# Manage rewrite rules shared by all Tails website instances
class tails::website::webserver::rewrite_rules (
  Enum['present', 'absent'] $ensure = 'present',
  Hash $po_slave_languages          = $tails::website::params::production_slave_languages,
) inherits tails::website::params {

  nginx::included { 'tails_website_rewrite_rules':
    ensure  => $ensure,
    content => template('tails/website/nginx/rewrite_rules.conf.erb'),
  }

}
