# Manage systemd-resolved
class tails::systemd::resolved () {
  service { 'systemd-resolved':
    ensure => 'running',
    enable => true,
  }

  file { '/etc/resolv.conf':
    ensure => link,
    target => '/run/systemd/resolve/resolv.conf',
  }
}
