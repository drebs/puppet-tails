# Manage Tails' Redmine instance
class tails::redmine (
  String $mailhandler_api_key,
  Stdlib::Httpsurl $url = 'https://labs.riseup.net/code',
) {

  include ::mysql::server

  file { '/var/cache/debconf/redmine.preseed':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0600',
    source => 'puppet:///modules/tails/redmine/redmine.preseed',
  }

  package { 'redmine':
    ensure       => installed,
    responsefile => '/var/cache/debconf/redmine.preseed',
    require      => File['/var/cache/debconf/redmine.preseed'],
  }

  # Dependencies of the redmine_git_hosting plugin
  ensure_packages([
    'ruby-dalli',
    'ruby-github-markup',
    'ruby-grack',
    'ruby-haml-rails',
    'ruby-hiredis',
    'ruby-redis',
    'ruby-rugged',
    'ruby-will-paginate',
  ])

  augeas {
    'logrotate_redmine':
      context => '/files/etc/logrotate.d/redmine/rule',
      changes => [
        'set file /var/log/redmine/*/*.log',
        'set rotate 7',
        'set schedule daily',
        'set compress compress',
        'set missingok missingok',
        'set ifempty notifempty',
        'set copytruncate copytruncate',
      ],
      require => Package['redmine'],
  }

  apt::dpkg_statoverride { '/usr/share/redmine/log':
    user  => 'root',
    group => 'www-data',
    mode  => '775',
  }

  file { '/etc/logrotate.d/redmine_git_hosting':
    source => 'puppet:///modules/tails/redmine/logrotate.d/redmine_git_hosting',
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  file { '/usr/share/redmine/public/themes/Modula Mojito':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/tails/redmine/themes/Modula_Mojito';
  }

  # Compatibility for projects that were configured in the pre-Gitolite era
  file { '/srv/gitosis/repositories':
    ensure => link,
    target => '/var/lib/gitolite/repositories',
  }

  # $HOME needs to be /home/www-data for the git-hosting plugin stuff in
  # ~/.ssh
  user { 'www-data':
    ensure => present,
    groups => ['gitolite'],
    home   => '/home/www-data',
    shell  => '/bin/sh',
    system => true,
    uid    => 33,
    gid    => 'www-data',
  }

  postfix::mailalias { 'redmine':
    recipient => "|/usr/share/redmine/extra/mail_handler/rdm-mailhandler.rb --url '${url}' --key '${mailhandler_api_key}' --allow-override all", # lint:ignore:140chars -- command
  }

  file { '/var/www/robots.txt':
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0664',
    source => 'puppet:///modules/tails/redmine/robots.txt',
  }

  include tails::backupninja
  file { '/etc/backup.d/10.mysql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/mysql
databases = all
configfile = /etc/mysql/debian.cnf
",
    require => Package['backupninja'],
  }

  sudo::conf { 'gitolite_as_www-data':
    content => 'gitolite       ALL=(www-data)  NOPASSWD:ALL'
  }

  sudo::conf { 'www-data_as_gitolite':
    content => 'www-data       ALL=(gitolite)  NOPASSWD:ALL'
  }

}
