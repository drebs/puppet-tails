class tails::apt::repository::torproject (
  Enum['present', 'absent'] $ensure = 'present',
) {
  apt::sources_list { 'torproject.list':
    ensure  => $ensure,
    content => template('tails/apt/repository/torproject.list.erb'),
  }
}
