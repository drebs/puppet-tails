class tails::apt::repository::torproject::obfs4proxy (
  Enum['present', 'absent'] $ensure = 'present',
) {
  apt::sources_list { 'torproject-obfs4proxy.list':
    ensure  => $ensure,
    content => "deb http://deb.torproject.org/torproject.org obfs4proxy main\n",
  }
}
