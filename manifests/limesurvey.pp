# Manage a LimeSurvey service
class tails::limesurvey (
  Array[String] $admins,
  String $db_password,
  String $db_user                          = 'limesurvey',
  String $db_name                          = 'limesurvey',
  Stdlib::Absolutepath $apache_data_dir    = '/var/www/limesurvey',
  Stdlib::Absolutepath $mutable_data_dir   = '/var/lib/limesurvey',
  String $upstream_git_remote              = 'https://github.com/LimeSurvey/LimeSurvey.git',
  Stdlib::Absolutepath $upstream_git_local = '/var/lib/limesurvey/git',
) {

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::limesurvey module only supports Debian Stretch.')
  }

  ### Resources

  # Packages

  $packages = [
    'apache2',
    'libapache2-mod-removeip',
    'ncdu',
    'php',
    'php-gd',
    'php-imagick',
    'php-mysql',
    'php-mbstring',
    'php-xml',
  ]

  ensure_packages($packages)

  # Users and groups

  user { 'limesurvey':
    ensure => present,
    system => true,
    home   => $mutable_data_dir,
    gid    => 'limesurvey',
    shell  => '/bin/bash',
  }

  group { 'limesurvey':
    ensure => present,
    system => true,
  }

  group { 'limesurvey_admin':
    ensure => present,
    system => true,
  }

  tails::limesurvey::admin { $admins: }

  # sudo credentials

  sudo::conf { 'limesurvey-admin':
    source  => 'puppet:///modules/tails/limesurvey/sudo/limesurvey-admin',
    require => Group['limesurvey_admin'],
  }

  # Mutable data

  file { $mutable_data_dir:
    ensure => directory,
    owner  => limesurvey,
    group  => limesurvey,
    mode   => '2755',
  }

  # Code

  exec { 'clone upstream Git repository':
    command => "git clone --bare '${upstream_git_remote}' '${upstream_git_local}'",
    user    => limesurvey,
    creates => $upstream_git_local,
  }

  file { $apache_data_dir:
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0750',
    require => Package[apache2],
  }

  exec { 'initialize production Git working copy':
    command => "git clone '${upstream_git_local}' '${apache_data_dir}'",
    user    => 'www-data',
    creates => "${apache_data_dir}/.git",
    require => [
      File[$apache_data_dir],
      Exec['clone upstream Git repository'],
    ],
  }

  # Service

  service { 'apache2':
    ensure  => running,
    require => Package[apache2],
  }

  # Configuration

  file { "${mutable_data_dir}/config":
    ensure => directory,
    owner  => root,
    group  => 'limesurvey_admin',
    mode   => '2775',
  }

  file { "${mutable_data_dir}/config/apache-vhost.conf":
    ensure => present,
    owner  => root,
    group  => 'limesurvey_admin',
    mode   => '0664',
  }

  file { '/etc/apache2/sites-available/000-default.conf':
    ensure => symlink,
    target => "${mutable_data_dir}/config/apache-vhost.conf",
    notify => Service[apache2],
  }

  exec { 'a2enmod removeip':
    creates => '/etc/apache2/mods-enabled/removeip.load',
    require => Package['libapache2-mod-removeip'],
  }

  # Database

  include mysql::server

  mysql::conf { 'strict_mode':
    ensure  => present,
    section => 'mysqld',
    config  => {
      sql_mode => 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,STRICT_ALL_TABLES',
    }
  }

  mysql_database { $db_name:
    ensure => present,
  }

  mysql_user { "${db_user}@localhost":
    password_hash => mysql_password($db_password),
    require       => Mysql_database[$db_name],
  }
  mysql_grant { "${db_user}@localhost/${db_name}":
    privileges => all,
    require    => Mysql_user["${db_user}@localhost"],
  }

  # Backups

  include tails::backupninja
  file { '/etc/backup.d/10.mysql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/mysql
databases = all
configfile = /etc/mysql/debian.cnf
",
    require => Package['backupninja'],
  }

  # Releases monitoring

  include tails::limesurvey::releases_monitor

}
