# Wrapper around the gitolite class with Tails-specific functionality and stuff
class tails::gitolite (
  Boolean $with_git_annex           = false,
  Boolean $git_annex_use_backport   = false,
  String $git_annex_with_recommends = 'default',
  String $ssh_pubkey_name           = 'gitolite@puppet-git.lizard',
) {

  apt::sources_list { 'wheezy.list':
    ensure  => present,
    content => "deb http://ftp.us.debian.org/debian/ wheezy main\n",
  }
  apt::preferences_snippet { 'gitolite':
    ensure   => present,
    pin      => 'release o=Debian,n=wheezy',
    priority => '991',
  }

  class { '::gitolite':
    admin_key         => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+mdQZg1zWYTOwYvs9Anhn+SIAVsg47gj/nGdjVTZu9hehX9GCeU40OwI0qKIASpegweELwHjuRZ5Fhx9UGKYlWKa+fT/xpYg7wGeXM65N4KmzcppyYHjErerGPNbA4glAsmUmmv/IG69hgHVhezclu64lj6KeIYvyD6kEv1mu4yts584tLOip1Sig3XS4dyB5BRqDhD60CN5qQ55kYCLRxw6QfResl+Dqw6GDa8FOuHik0IESGaRAHqVeR+NC+xo2jMj05yi2WclLMIuV6orM9NI3WxCyHDpGXKeT5huR1OJeCW9mzkq0Z1bFswioX2NJsYO9VOunSsmp+XQkOw3y8mTYToCi8DWlAJ3yeu7aB51ofwgDfBY+gyyW/X8wrqzCTJ9VMScgfBm/4GLaJns8hYxtsBVYT6P+syC7kqVs+v2cr2pHQpleLb50MRVnuFPTILOdRMME7SOPqIuwlDDes1ItVHm5oR177q5BI9ywC2Anu/S26i+tE+8g0080voNNS5Pykbhz+o4S8DuO2pA0+ijzYB4t3Ic6bcUcC8RE7G5LG5V3Go8/pewvZUEXlGn7H7ti3raPjMZY5crrrU4rS851TD/wuu6kZdU1k3IFrxUFW5wt4IKS6sc5tZw8HO5jZc4WjZLbceoqiGW5TuTmORjLxseGbk7X8sMEjP9rpQ== intrigeri@ensifera', # lint:ignore:140chars -- SSH key
    gituser           => gitolite,
    deploy_gitoliterc => false,
  }
  file { '/etc/systemd/system/git-daemon.service':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => '[Unit]
Description=Git Daemon Instance

[Service]
User=gitolite
Group=gitolite
ExecStart=/usr/lib/git-core/git-daemon --reuseaddr --base-path=/var/lib/gitolite/repositories /var/lib/gitolite/repositories

[Install]
WantedBy=multi-user.target
',
  }

  service { 'git-daemon':
    ensure    => running,
    provider  => systemd,
    require   => [
      Package['git'],
      File['/etc/systemd/system/git-daemon.service']
    ],
    subscribe => File['/etc/systemd/system/git-daemon.service'],
  }
  exec { 'systemctl enable git-daemon.service':
    creates => '/etc/systemd/system/multi-user.target.wants/git-daemon.service',
  }

  # Make it easier to rotate logs ourselves
  file_line { 'single_log_file':
    path  => '/var/lib/gitolite/.gitolite.rc',
    match => '#?\s*\$GL_LOGT\s*=\s*".*";',
    line  => "\$GL_LOGT = \"\$GL_ADMINDIR/logs/gitolite.log\";",
  }

  # Rotate logs
  file { '/etc/logrotate.d/gitolite':
    owner  => root,
    group  => root,
    mode   => '0644',
    source => [
      'puppet:///modules/site_tails/gitolite/logrotate',
      'puppet:///modules/tails/gitolite/logrotate'
    ],
  }

  # Mirroring support
  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => gitolite,
    home    => '/var/lib/gitolite',
  }
  file { '/var/lib/gitolite/.ssh/config':
    source => 'puppet:///modules/tails/gitolite/ssh/config',
    owner  => gitolite,
    group  => gitolite,
    mode   => '0600',
  }

  # git-annex support
  if $with_git_annex == true {

    $gitolite_local_lib = '/usr/local/lib/gitolite'
    $adc_path           = "${gitolite_local_lib}/adc"
    $adc_ua_path        = "${adc_path}/ua"

    class { '::tails::git_annex':
      use_backport    => $git_annex_use_backport,
      with_recommends => $git_annex_with_recommends,
    }

    file { [$gitolite_local_lib, $adc_path, $adc_ua_path]:
      ensure => directory,
      owner  => root,
      group  => gitolite,
      mode   => '0750',
    }

    file_line { 'support_git-annex':
      path    => '/var/lib/gitolite/.gitolite.rc',
      match   => '#?\s*\$GL_ADC_PATH\s*=\s*".*";',
      line    => "\$GL_ADC_PATH = \"${adc_path}\";",
      require => [
        File["${adc_ua_path}/git-annex-shell"],
        Package['git-annex'],
      ],
    }

    file { "${adc_ua_path}/git-annex-shell":
      source => '/usr/share/doc/gitolite/examples/adc/git-annex-shell',
      owner  => root,
      group  => gitolite,
      mode   => '0750',
    }

  }

  class { '::tails::gitolite::hooks::common': }
  class { '::tails::gitolite::hooks::jenkins_jobs': }
  class { '::tails::gitolite::hooks::tails': }

}
