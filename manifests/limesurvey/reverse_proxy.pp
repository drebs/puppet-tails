# Manage a nginx vhost that reverse proxies to a LimeSurvey instance.
class tails::limesurvey::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'survey.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'survey.lizard',
  Stdlib::Port $upstream_port     = 80,
  ) {

  $upstream_address = "${upstream_hostname}:${upstream_port}"

  nginx::vhostsd { $public_hostname:
    content => template('tails/limesurvey/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included['tails_limesurvey_reverse_proxy'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { 'tails_limesurvey_reverse_proxy':
    content => template('tails/limesurvey/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
