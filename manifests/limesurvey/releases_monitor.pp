# Monitor LimeSurvey security releases and notify the service admins when needed.
# The `$ensure' parameter is passed through to Vcsrepo resources.

class tails::limesurvey::releases_monitor (
  String $ensure                      = 'latest',
  Stdlib::Absolutepath $clone_basedir = "${::tails::limesurvey::mutable_data_dir}/monitor-releases",
  String $repo_origin                 = 'https://git-tails.immerda.ch/monitor-limesurvey-releases',
  String $repo_rev                    = 'master',
  String $repo_user                   = 'limesurvey',
  String $email_recipient             = 'root',
) {

  file { $clone_basedir:
    ensure => directory,
    owner  => $repo_user,
    group  => $repo_user,
    mode   => '0755',
  }

  $packages = [
    'libdpkg-perl',
    'libgit-repository-perl',
    'libmethod-signatures-simple-perl',
    'libmoo-perl',
    'libmoox-options-perl',
    'libpath-tiny-perl',
    'libstrictures-perl',
    'libtry-tiny-perl',
    'libtype-tiny-perl',
  ]
  ensure_packages($packages)

  vcsrepo { "${clone_basedir}/git":
    ensure   => $ensure,
    provider => git,
    source   => $repo_origin,
    revision => $repo_rev,
    user     => $repo_user,
    require  => File[$clone_basedir],
  }

  cron { 'tails-limesurvey-releases-monitor':
    user        => $repo_user,
    hour        => '*/6',
    minute      => '27',
    command     => "${clone_basedir}/git/bin/tails-monitor-limesurvey-releases --git-dir=${::tails::limesurvey::mutable_data_dir}/git --last-checked-cache-file=${clone_basedir}/last_checked_tag", # lint:ignore:140chars -- command
    require     => [ Package[$packages], Vcsrepo["${clone_basedir}/git"] ],
    environment => [ "MAILTO=${email_recipient}" ]
  }

}
