# Manage a Weblate service
class tails::weblate (
  Array[String] $admins,
  String $db_password,
  String $db_user                         = 'weblate',
  String $db_name                         = 'weblate',
  String $code_git_remote                 = 'https://github.com/WeblateOrg/weblate.git',
  Stdlib::Absolutepath $code_git_checkout = '/usr/local/share/weblate',
  String $code_git_revision               = 'weblate-2.10.1',
  Stdlib::Absolutepath $apache_data_dir   = '/var/www/weblate',
  Stdlib::Absolutepath $mutable_data_dir  = '/var/lib/weblate',
  Stdlib::Absolutepath $tmserver_data_dir = '/var/lib/tmserver',
) {

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::weblate module only supports Debian Stretch.')
  }

  ### Resources

  include tails::website_builder

  # Packages

  $packages = [
    'apache2',
    'ccze',
    'memcached',
    'libapache2-mod-wsgi',
    'python-dateutil',
    'python-defusedxml',
    'python-django',
    'python-django-compressor',
    'python-django-crispy-forms',
    'python-djangorestframework',
    'python-lxml',
    'python-memcache',
    'python-mysqldb',
    'python-oauthlib',
    'python-pil',
    'python-pyuca',
    'python-six',
    'python-social-auth',
    'python-whoosh',
    'translate-toolkit',
  ]

  ensure_packages($packages)

  # Users and groups

  user { 'tmserver':
    ensure => present,
    system => true,
    home   => $tmserver_data_dir,
    gid    => 'tmserver',
  }

  user { 'weblate':
    ensure => present,
    system => true,
    home   => $mutable_data_dir,
    gid    => 'weblate',
  }

  group { 'tmserver':
    ensure => present,
    system => true,
  }

  group { 'weblate':
    ensure => present,
    system => true,
  }

  group { 'weblate_admin':
    ensure => present,
    system => true,
  }

  tails::weblate::admin { $admins: }

  # sudo credentials

  sudo::conf { 'weblate-admin':
    source  => 'puppet:///modules/tails/weblate/sudo/weblate-admin',
    require => Group['weblate_admin'],
  }

  # static data served by Apache without going through wsgi

  file { $apache_data_dir:
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2755',
  }

  file { "${apache_data_dir}/media":
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2775',
  }

  file { "${apache_data_dir}/static":
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2775',
  }

  # Mutable data

  file { $mutable_data_dir:
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2750',
  }

  file { "${mutable_data_dir}/repositories":
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2770',
  }

  file { "${mutable_data_dir}/.ssh":
    ensure => directory,
    owner  => weblate,
    group  => weblate,
    mode   => '2770',
  }

  # Machine Translation Service

  file { $tmserver_data_dir:
    ensure => directory,
    owner  => tmserver,
    group  => 'weblate_admin',
    mode   => '2770',
  }

  # Code

  vcsrepo { $code_git_checkout:
    provider => git,
    source   => $code_git_remote,
    revision => $code_git_revision,
    user     => root,
  }

  # Service

  service { 'apache2':
    ensure  => running,
    require => Package[apache2],
  }

  service { 'memcached':
    ensure  => running,
    require => Package[memcached],
  }


  # Configuration

  file { "${mutable_data_dir}/config":
    ensure => directory,
    owner  => root,
    group  => 'weblate_admin',
    mode   => '2775',
  }

  file { "${mutable_data_dir}/config/apache-vhost.conf":
    ensure => present,
    owner  => root,
    group  => 'weblate_admin',
    mode   => '0664',
  }
  file { '/etc/apache2/sites-available/000-default.conf':
    ensure => symlink,
    target => "${mutable_data_dir}/config/apache-vhost.conf",
    notify => Service[apache2],
  }

  file { "${mutable_data_dir}/config/settings.py":
    ensure => present,
    owner  => root,
    group  => 'weblate_admin',
    mode   => '0664',
  }
  file { "${code_git_checkout}/weblate/settings.py":
    ensure  => symlink,
    target  => "${mutable_data_dir}/config/settings.py",
    require => Vcsrepo[$code_git_checkout],
  }

  # Cronjobs

  cron { 'weblate updategit':
    command => "cd ${mutable_data_dir}; ./manage.py updategit",
    user    => weblate,
    minute  => '*/5',
  }

  cron { 'weblate updateindex':
    command => "cd ${mutable_data_dir}; ./manage.py update_index",
    user    => weblate,
    minute  => '*/5',
  }

  cron { 'weblate updatechecks':
    command => "cd ${mutable_data_dir}; ./manage.py updatechecks --all",
    user    => weblate,
    minute  => '*/6',
  }

  cron { 'weblate loadpo':
    command => "cd ${mutable_data_dir}; ./manage.py loadpo --all",
    user    => weblate,
    minute  => '*/5',
  }

  cron { 'weblate commitpending':
    command => "cd ${mutable_data_dir}; ./manage.py commit_pending --all --age=96 --verbosity=0",
    user    => weblate,
    minute  => 3,
  }

  cron { 'weblate cleanup':
    command => "cd ${mutable_data_dir}; ./manage.py cleanuptrans",
    user    => weblate,
    hour    => 4,
  }

  # Database

  include mysql::server

  mysql::conf { 'strict_mode':
    ensure  => present,
    section => 'mysqld',
    config  => {
      sql_mode => 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,STRICT_ALL_TABLES',
    }
  }

  mysql_database { $db_name:
    ensure => present,
  }

  mysql_user { "${db_user}@localhost":
    password_hash => mysql_password($db_password),
    require       => Mysql_database[$db_name],
  }
  mysql_grant { "${db_user}@localhost/${db_name}":
    privileges => all,
    require    => Mysql_user["${db_user}@localhost"],
  }

  # Backups

  include tails::backupninja
  file { '/etc/backup.d/10.mysql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/mysql
databases = all
configfile = /etc/mysql/debian.cnf
",
    require => Package['backupninja'],
  }

}
