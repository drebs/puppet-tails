# Manage a SSH user with a fake login prompt for the automated test suite

class tails::tester::support::ssh (
  String $password,
  String $ssh_pub_key,
  String $ssh_pub_key_type,
  String $user               = 'autotest-ssh',
  Stdlib::Absolutepath $home = '/var/lib/autotest-ssh',
) {

  ensure_packages([falselogin])

  file { '/etc/falselogin.conf':
    content => "%user%@%host% ~$ \n",
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package[falselogin],
  }
  user { $user:
    ensure     => present,
    shell      => '/usr/bin/falselogin',
    system     => true,
    home       => $home,
    managehome => true,
    password   => $password,
    require    => Package[falselogin],
  }
  file { [$home, "${home}/.ssh"]:
    ensure  => directory,
    owner   => $user,
    group   => $user,
    mode    => '0700',
    require => User[$user],
  }
  ssh_authorized_key { $user:
    ensure  => present,
    user    => $user,
    type    => $ssh_pub_key_type,
    key     => $ssh_pub_key,
    require => File["${home}/.ssh"],
  }

}
