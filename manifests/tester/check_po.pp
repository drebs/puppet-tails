# Set up what's necessary to automatically check PO files
class tails::tester::check_po (
  String $tools_repo_local_user             = 'jenkins',
  Stdlib::Absolutepath $tools_repo_checkout = '/var/lib/jenkins/tools',
  String $tools_repo_url                    = 'https://git-tails.immerda.ch/jenkins-tools',
  String $tools_repo_rev                    = 'master',
  String $tools_repo_ensure                 = 'latest',
) {

  if $::operatingsystem != 'Debian' {
    fail('This module only supports Debian.')
  }

  $tester_packages = [
    'i18nspector'
  ]

  ensure_packages($tester_packages)

  vcsrepo { $tools_repo_checkout:
    ensure   => $tools_repo_ensure,
    provider => git,
    source   => $tools_repo_url,
    revision => $tools_repo_rev,
    user     => $tools_repo_local_user,
    require  => Package[jenkins-slave],
  }

}
