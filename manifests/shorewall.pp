# Common shorewall snippets used by Tails systems.
class tails::shorewall (
  String $net_interface          = 'eth0',
  String $vpn_interface          = 'tailsvpn',
  String $vpn_zone               = 'vpn',
  Optional[String] $vm_interface = undef,
  Optional[String] $vm_zone      = undef,
  String $puppetmaster_dest      = 'vpn:192.168.122.32',
){

  if (($vm_interface != undef) and ($vm_zone == undef)) or
    (($vm_interface == undef) and ($vm_zone != undef)) {
    fail('Both $vm_interface and $vm_zone must be defined if used.')
  }

  shorewall::zone { ['net', $vpn_zone]: type => 'ipv4' }

  if ($vm_interface != undef) and ($vm_zone != undef) {
    validate_string(
      $vm_interface,
      $vm_zone
    )
    shorewall::zone { $vm_zone: type => 'ipv4' }
    shorewall::interface { $vm_interface:
      zone        => $vm_zone,
      dhcp        => true,
      add_options => bridge,
    }
    class { 'shorewall::rules::libvirt::host':
      masq_iface    => $net_interface,
      debproxy_port => false,
      vmz_iface     => 'vmz:virbr0',
      accept_dhcp   => false,
    }
    shorewall::rule {
      'vmz_to-puppetmaster':
        source          => $vm_zone,
        proto           => tcp,
        destination     => $puppetmaster_dest,
        destinationport => 8140,
        order           => 300,
        action          => 'ACCEPT';
    }
  }

  shorewall::interface { $net_interface:
    zone    => net,
    dhcp    => true,
    options => 'tcpflags,routefilter,nosmurfs',
  }

  shorewall::interface { $vpn_interface:
    zone    => $vpn_zone,
    dhcp    => false,
    options => 'tcpflags,routefilter,nosmurfs',
  }

  shorewall::policy {
    'fw-to-fw':
      sourcezone      => '$FW',
      destinationzone => '$FW',
      policy          => 'ACCEPT',
      order           => 100;
    'fw-to-net':
      sourcezone      => '$FW',
      destinationzone => net,
      policy          => 'ACCEPT',
      order           => 110;
    'net-to-all':
      sourcezone      => net,
      destinationzone => all,
      policy          => 'DROP',
      shloglevel      => 'info',
      order           => 800;
    'all-to-all':
      sourcezone      => all,
      destinationzone => all,
      policy          => 'REJECT',
      shloglevel      => 'info',
      order           => 900;
  }

  shorewall::rule_section { 'NEW': order => 010 }

  shorewall::rule {
    'neededICMP':
      source      => all,
      destination => all,
      order       => 100,
      action      => 'AllowICMPs(ACCEPT)';
    'ping':
      source      => all,
      destination => all,
      order       => 100,
      action      => 'Ping(ACCEPT)';
  }

  shorewall::rule {
    'to-puppetmaster':
      source          => '$FW',
      proto           => tcp,
      destination     => $puppetmaster_dest,
      destinationport => 8140,
      order           => 300,
      action          => 'ACCEPT';
  }

  class { 'shorewall::rules::ssh': ports => ['22'] }

}
