# Manage a mirror of a git-annex repository
define tails::git_annex::mirror (
  Pattern[/\A[a-z_-]+\z/] $user,
  Stdlib::Absolutepath $home,
  String $ssh_keyname,

  Stdlib::Absolutepath $checkout_dir,
  String $remote_repo,

  Enum['present', 'absent'] $ensure           = 'present',
  String $mode                                = 'mirror',
  Boolean $direct_mode                        = true,
  String $pull_hour                           = '*',
  Variant[String, Array[String]] $pull_minute = ['14', '29', '44', '59'],

  Boolean $manage_mount                       = false,
  $mount_point                                = false,
  $mount_device                               = false,
  $mount_fstype                               = false,
  $mount_options                              = false,

  String $webserver                           = 'nginx',
  Boolean $manage_vhost                       = false,
  $vhost_template                             = false,
  $vhost_auth_source                          = false,
) {

  ### Resources

  vcsrepo { $checkout_dir:
    ensure   => $ensure,
    provider => git,
    source   => $remote_repo,
    user     => $user,
    require  => Sshkeys::Set_client_key_pair[$ssh_keyname],
  }

  file { $checkout_dir:
    ensure  => directory,
    owner   => $user,
    group   => 'www-data',
    mode    => '0750',
    require => Vcsrepo[$checkout_dir],
  }

  exec { "Switch ${name} to direct mode":
    command => 'git annex direct',
    user    => $user,
    cwd     => $checkout_dir,
    creates => "${checkout_dir}/.git/annex",
    require => [
      Package['git-annex'],
      Vcsrepo[$checkout_dir],
    ],
  }

  cron { "Update local mirror of ${name}":
    ensure  => $ensure,
    command => "cd '${checkout_dir}' && lckdo '/run/lock/tails-pull-git-annex-${name}' /usr/local/bin/pull-git-annex '${mode}'",
    user    => $user,
    hour    => $pull_hour,
    minute  => $pull_minute,
    require => [
      Exec["Switch ${name} to direct mode"],
      File['/usr/local/bin/pull-git-annex'],
    ],
  }

  user { $user:
    ensure => $ensure,
    system => true,
    home   => $home,
  }

  file { [ $home, "${home}/.ssh" ]:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  sshkeys::set_client_key_pair { $ssh_keyname:
    keyname => $ssh_keyname,
    user    => $user,
    home    => $home,
    require => [
      User[$user],
      File["${home}/.ssh"],
    ],
  }

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)

    file { $mount_point:
      ensure => directory,
      owner  => $user,
      group  => 'www-data',
      mode   => '2755',
    }

    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

  }

  if $manage_vhost {
    case $webserver {

      'nginx': {
        ensure_packages(['libnginx-mod-http-fancyindex'])

        nginx::vhostsd { $name:
          content => template($vhost_template),
          require => Package['libnginx-mod-http-fancyindex'],
        }
        if $vhost_auth_source {
          nginx::authd { $name:
            source  => $vhost_auth_source,
          }
        }
      }

      default: {
        fail("Unsupported webserver ${webserver}")
      }

    }
  }

}
