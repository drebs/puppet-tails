# Manage CPU frequency settings
#
# $cpufreq_governor, $energy_perf_policy: false == don't manage this setting

class tails::cpufreq (
  String $cpufreq_governor = 'performance',
  String $energy_perf_policy = 'performance',
) {

  ensure_packages(['linux-cpupower'])

  if $cpufreq_governor {
    file { '/etc/systemd/system/cpufreq-governor.service':
      owner   => root,
      group   => root,
      mode    => '0644',
      require => Package['linux-cpupower'],
      content => "[Unit]
Description=Tweak cpufreq governor

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/cpupower -c all frequency-set --governor ${cpufreq_governor}

[Install]
WantedBy=multi-user.target
",
    }
    service { 'cpufreq-governor':
      ensure    => running,
      provider  => systemd,
      require   => File['/etc/systemd/system/cpufreq-governor.service'],
      subscribe => File['/etc/systemd/system/cpufreq-governor.service'],
    }
    exec { 'systemctl enable cpufreq-governor.service':
      creates => '/etc/systemd/system/multi-user.target.wants/cpufreq-governor.service',
      require => Service['cpufreq-governor'],
    }
  } else {
    # The service will be disabled on next boot; no way to revert to the default
    # system settings immediately: they are hardware-dependent
    file { '/etc/systemd/system/multi-user.target.wants/cpufreq-governor.service':
      ensure => absent,
    }
  }

  if $energy_perf_policy {
    file { '/etc/modules-load.d/msr.conf':
      owner   => root,
      group   => root,
      mode    => '0644',
      content => "msr\n",
    }
    file { '/etc/systemd/system/energy-perf-policy.service':
      owner   => root,
      group   => root,
      mode    => '0644',
      require => [Package['linux-cpupower'], File['/etc/modules-load.d/msr.conf']],
      content => "[Unit]
Description=Tweak CPU energy/perf policy

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/sbin/x86_energy_perf_policy ${energy_perf_policy}

[Install]
WantedBy=multi-user.target
",
    }
    service { 'energy-perf-policy':
      ensure    => running,
      provider  => systemd,
      require   => File['/etc/systemd/system/energy-perf-policy.service'],
      subscribe => File['/etc/systemd/system/energy-perf-policy.service'],
    }
    exec { 'systemctl enable energy-perf-policy.service':
      creates => '/etc/systemd/system/multi-user.target.wants/energy-perf-policy.service',
      require => Service['energy-perf-policy'],
    }
  } else {
    # The service will be disabled on next boot; no way to revert to the default
    # system settings immediately: they are hardware-dependent
    file { '/etc/systemd/system/multi-user.target.wants/energy-perf-policy.service':
      ensure => absent,
    }
  }

}
