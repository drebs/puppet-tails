# Install the cronjob and scripts necessary to automatically update the
# build_Tails_ISO_* jobs in Jenkins.
class tails::jenkins::iso_jobs_generator (
  Integer $active_days,
  String $jenkins_jobs_repo,
  Enum['present', 'absent'] $ensure = 'present',
  String $tails_repo                = 'https://git-tails.immerda.ch/tails',
) {

  file { '/var/lib/jenkins/.gitconfig':
    ensure  => $ensure,
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0640',
    content => "[user]\n  name = jenkins\n  email = jenkins@${::fqdn}\n",
  }

  if !defined(Class['tails::pythonlib']) {
    include tails::pythonlib
  }

  ensure_packages(
    ['python3-yaml'],
    {'ensure' => $ensure}
  )

  file { '/usr/local/sbin/generate_tails_iso_jobs':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/generate_tails_iso_jobs',
    require => Vcsrepo['/var/lib/tails_pythonlib/git'],
  }

  file { '/usr/local/sbin/update_tails_iso_jobs':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/update_tails_iso_jobs',
    require => File['/usr/local/sbin/generate_tails_iso_jobs'],
  }

  cron {'update_tails_iso_jobs':
    ensure  => $ensure,
    minute  => '*/5',
    user    => 'jenkins',
    command => "/usr/local/sbin/update_tails_iso_jobs '${active_days}' '${tails_repo}' '${jenkins_jobs_repo}'",
    require => File['/usr/local/sbin/update_tails_iso_jobs'],
  }

}
