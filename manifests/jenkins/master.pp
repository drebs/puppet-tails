# Manages the Tails Jenkins master.
# Installs extra packages for nice features as well as Jenkins plugins.
# If $automatic_iso_jobs_generator is 'present', $jenkins_jobs_repo must be
# set to the URL of a jenkins-jobs git repo where the 'jenkins@jenkins-master'
# SshKey has write access. It depends on $deploy_on_git_push being set to true
# for the pushed configuration to be applied automatically.
# For this to happen, hooks managed in tails::gitolite::hooks::jenkins_jobs
# also need to be installed in the jenkins-jobs repo.

class tails::jenkins::master (
  String $jenkins_jobs_repo,
  String $tails_repo                                      = 'https://git-tails.immerda.ch/tails',
  Boolean $deploy_jobs_on_git_push                        = true,
  Enum['present', 'absent'] $automatic_iso_jobs_generator = 'present',
  Integer $active_branches_max_age_in_days                = 49,
  String $gitolite_pubkey_name                            = 'gitolite@puppet-git',

  Boolean $manage_mount                                   = false,
  $mount_point                                            = '/var/lib/jenkins',
  $mount_device                                           = false,
  $mount_fstype                                           = 'ext4',
  $mount_options                                          = 'relatime,user_xattr,acl',

  String $monitoring_parent_zone                          = 'Lizard',
) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $ssh_pubkey_name = "jenkins@jenkins-master.${::domain}"

  ### Resources

  apt::sources_list { 'tails-jenkins-master.list':
    content => "deb http://deb.tails.boum.org/ jenkins-master main\n",
  }

  apt::preferences_snippet { [
    'jenkins',
    'jenkins-common']:
    pin      => 'origin deb.tails.boum.org',
    priority => 991,
    require  => Apt::Sources_list['tails-jenkins-master.list'],
  }

  class { 'jenkins':
    repo         => false,
    install_java => false,
    version      => 'latest',
    require      => [
      Apt::Preferences_snippet['jenkins'],
      Package[$base_packages],
    ],
  }

  $base_packages = [
    'git',
    'jenkins-common',
    'jenkins-job-builder',
    'libmockito-java',
    'libtap-formatter-junit-perl',
    'python3-jenkins-job-builder',
    'python-pkg-resources',
  ]

  ensure_packages($base_packages)

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)
    validate_string($monitoring_parent_zone)

    file { $mount_point:
      ensure => directory,
    }

    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

    Mount[$mount_point] -> Class['jenkins']

    @@::tails::monitoring::service::disk { "jenkins-data-disk@${::fqdn}":
      nodename  => $::fqdn,
      zone      => $::fqdn,
      partition => $mount_point,
      wfree     => '30000',
      cfree     => '20000',
      tag       => $monitoring_parent_zone,
    }
  }

  include nfs::server

  Nfs::Export <<| tag == $::fqdn |>>

  # lint:ignore:140chars -- SHA512
  jenkins::plugin { 'build-timeout':
    version       => '1.16',
    digest_string => '0203a5980e1e2154b90ed7fe0d662d94f0eda5d461af33977cfee7910c76dceeff7aaf2abfd84ec2dd2d0af8dcbb5e8b60577ddc6558041740933b88e67c04fa',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'f09b82ccec2afc60c3b9235d804f312c3e5a0a847b4243b1e3546f718f344af1f7a0f26c4d53a02ae360aa6a50a20676910b143011c4eb880daa8ab0bc0fb073',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'copyartifact':
    version       => '1.33',
    digest_string => 'ff3ba55ba49034b9aa4a630891cd9ba3e11692934c2694ca42562fc5200158e4d393d742f122abf053cd61cdeb87f31f2c0291a00853cc167c55b05361fffaf6',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'credentials':
    version       => '1.28',
    digest_string => '86f4404245272c2e45e86dcd7c706eff45f30095273910a466968704e6859799c1f0302c3ec6b2c9c80b6e66f569103c7d0853e415488ea3f8263ed84454680e',
    digest_type   => 'sha512',
    require       => [Class['jenkins'], Jenkins::Plugin['icon-shim'],],
  }

  jenkins::plugin { 'cucumber-testresult-plugin':
    version       => '0.7',
    digest_string => '0d02938f0db4b15dbdce118a60f42f0d5f952c02442cf45d8e183b741c493d5d4972e308be525f250edc1e4654a04237c1a7fb44ac52f7056b29c5188d24890a',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'downstream-ext':
    version       => '1.8',
    digest_string => '5033490d9b34943488e387d64a3a09cf02a43dced29b0f43e8a68b9d837a1869702f9080831fc80e64e2addebf0553bd5c6a8793b46182ed1535422ca839d27f',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'email-ext':
    version       => '2.39.3',
    digest_string => 'e810accf918560daab2ba08929dad2f9d130758555d9ba1d2b94c28a0e594e2fc163a9c18f531d4051ead10c400523f126a85365ce48f2ac875effaa4a508acb',
    digest_type   => 'sha512',
    require       => [
      Class['jenkins'],
      Jenkins::Plugin[
        'token-macro', 'script-security'
      ],
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '1.92.1',
    digest_string => '86e4588f7aa05167e1470c2ba798e22e0781489b3a42dbb78831fdbe2f776c6e50db225023dec311b5c7748dc68ca92e580069aa146d2f6babc5cfed7cce0421',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'git':
    version       => '2.2.12',
    digest_string => '48141822e0eea1faa1a1a99b35372494e7352c2746ca3aa3a19a07f34b021848d2cd0bffc8959c1b809c5be231c1b49e9ffec0430dd68938197ac0f34588ee25',
    digest_type   => 'sha512',
    require       =>  [
      Class['jenkins'],
      Jenkins::Plugin[
        'credentials', 'git-client', 'scm-api', 'ssh-credentials'
      ],
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '1.15.0',
    digest_string => '6817fe635e64f5886aee16e47e87b50b967b989ce09d45ecb2426ab03f788e5eb1e90d28a557e2b1e41daa520d9a979cdf35cf07e1ce32d8c8fe33035af33235',
    digest_type   => 'sha512',
    require       =>  [
      Class['jenkins'],
      Jenkins::Plugin['ssh-credentials'],
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '1.3',
    digest_string => '9686cbe407c743cf0dd7681475e0b674683d8272504d08e260234a383cc3086fb2192bef4ac7dd292b6bdbaca5925d9f5ffed73f8493c0f05b9d68b26a6f47e9',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'icon-shim':
    version       => '2.0.3',
    digest_string => 'a83ebce40c28b4bb2474eea3aecfea9c079904b02dee210a70d5ffd7455c437e50ad59b1a9c677a749f9eae84ac702e5d9726a68b63d64ae5c209f5d105418b3',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'javadoc':
    version       => '1.3',
    digest_string => '6a89a9e5e0c345432edde647c46e1661072ce84e2768b54a658fa52476b8fd4319699368494a338b5926d797548f917c9519c8dcde4502f7895aee9a0bd269a8',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'mapdb-api':
    version       => '1.0.6.0',
    digest_string => '8acc6b56ec8dad809fd712a180aff394b11a63b7a95261a6dbaf8297a8ab23e274d045444e2068ac97e36ec532a90429cca62c584325f9ec99ec879c487a3cf3',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'maven-plugin':
    version       => '2.9',
    digest_string => '40da822f0bb42460bc6cf92bdb8fb12c4b869f111e7a28c87c6203dee184348440f01941f19919c71e84e032e842298cc3d879f38c4cd59e4846181676533361',
    digest_type   => 'sha512',
    require       => [
      Class['jenkins'],
      Jenkins::Plugin['javadoc'],
    ],
}

  jenkins::plugin { 'parameterized-trigger':
    version       => '2.25',
    digest_string => 'f36cfb2042c103b02ec760097a325892f440e56055da5d740ffe2f3224a6ef35ef5ce692f942818e60bc5168fbf4d88c0c40f6d5f4430e382af62b6f4460db96',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'postbuildscript':
    version       => '0.17',
    digest_string => 'e5aeab156496e178464958ac0ee4bf57d2a0022b7810335148c88be6f3c2a5113ae921c5d2f6e0f2ce663d4a5776be152cd7085455bf2f7683b306de95a34d20',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '1.3',
    digest_string => '5674c324351a28ebbd6e73ea7d7251144feda36413cfc6d6cf8b0a0251ffb3c4a75f401f6bb870ee307628e490e87a147d5221821b296cceb27437e8291b57e6',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'scm-api':
    version       => '0.2',
    digest_string => '2189dcd720685170e94a35753803156b55ad492e0d47bc525301b9d1a0838b3e338f6d8729621db10a20579088fe461b5c1af8d61f09094db322f4b680db9fc3',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'scm-sync-configuration':
    version       => '0.0.9',
    digest_string => '2f9dc1c3b5c050e442f4a64828aca481e4ea9bdf539621c45ab71e9e0b66c04b824ed9b30cfd341a16d19a3262c535ab646e0236b07d28f01e506b259868cd03',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'script-security':
    version       => '1.19',
    digest_string => 'a2cbb93b05291dda213b7744140c867809e9f026131ad47bcb4918c9105483b827e48f7c1a7c064284b5235e82f6f287ad8ecf3a4fc0200e2ffd0b6275caaf62',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '1.11',
    digest_string => '5e24c067ecf57d4016bbdd4e34eb8df9cd5e28dd5b4b2015e9c38bce15228b3ed6c3d82cfbd93d7a728ab449a7a4e2294dbc7758c8672ff374b4de1017205513',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'subversion':
    version       => '2.4.5',
    digest_string => '7f943c7099b3552ef816ccea9dd8195bae8caf1ea58dca80e394049e78ad9ff558524fe56d6134cd47f3548f25d1d1f7ae3e44962e0debd5f05fe6a4eebab98a',
    digest_type   => 'sha512',
    require       => [
      Class['jenkins'],
      Jenkins::Plugin['mapdb-api'],
    ],
  }

  jenkins::plugin { 'timestamper':
    version       => '1.6.2',
    digest_string => '3435a032e9ba53cf747f26b37fa52c4e672ecd95a5e393b343e407098309039031505b471c67b20c9f3f96919f408818c9b3b89248780d5b6508e502ad374b74',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'token-macro':
    version       => '1.11',
    digest_string => '9c94463ecc0c3039d8b444ffd3a1d1a686db5765d51b43c45f280752c51eb4ea8d55f31de568584c6d547fa4e8c9eee0df7d7dc687f77c3ffd3bc06c7b247f94',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.29',
    digest_string => '6c9bc83e369732e71ac71b6561c685fa1cbb41330f6d195e954a807c83c4561f64d94cad256377c138ca636f9cba922b2e541e444d9c0deb4a343a7c71480c33',
    digest_type   => 'sha512',
    require       => Class['jenkins'],
  }
  # lint:endignore

  file_line { 'jenkins_listen_on_all_interfaces':
    path    => '/etc/default/jenkins',
    match   => '#?JENKINS_ARGS="\$JENKINS_ARGS --httpListenAddress=\$HTTP_HOST --ajp13ListenAddress=\$AJP_HOST"',
    line    => '#JENKINS_ARGS="$JENKINS_ARGS --httpListenAddress=$HTTP_HOST --ajp13ListenAddress=$AJP_HOST"',
    require => Package['jenkins'],
  }

  ## Uncomment this (or similar) once all this is moved to a proper class,
  ## that inherits jenkins::service and can thus append to its dependencies.
  # Service['jenkins'] {
  #   require +> File_line['jenkins_HTTP_HOST'],
  # }

  file { '/etc/jenkins_jobs/jenkins_jobs.ini':
    owner   => root,
    group   => jenkins,
    mode    => '0640',
    source  => 'puppet:///modules/tails/jenkins/master/jenkins_jobs.ini',
    require => [
      Package['jenkins'],
      Package['jenkins-job-builder'],
    ],
  }

  file { '/etc/jenkins_jobs':
    ensure => directory,
    owner  => root,
    group  => jenkins,
    mode   => '0770',
  }

  vcsrepo { '/etc/jenkins_jobs/jobs':
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => jenkins,
    provider => git,
    source   => $jenkins_jobs_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
      Package['git'],
      File['/etc/jenkins_jobs'],
    ],
  }

  if $deploy_jobs_on_git_push {
    file { '/var/tmp/jenkins_jobs_test':
      ensure => directory,
      owner  => jenkins,
      group  => jenkins,
      mode   => '0700',
    }

    file { '/usr/local/sbin/deploy_jenkins_jobs':
      ensure  => present,
      source  => 'puppet:///modules/tails/jenkins/master/deploy_jenkins_jobs',
      owner   => root,
      group   => root,
      mode    => '0755',
      require => [
        File['/var/tmp/jenkins_jobs_test'],
        Ssh_authorized_key[$gitolite_pubkey_name],
      ],
    }

    sshkeys::set_authorized_keys { $gitolite_pubkey_name:
      user    => jenkins,
      home    => '/var/lib/jenkins',
      require => Package['jenkins'],
    }
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts':
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts',
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts_wrapper':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts_wrapper',
    require => File['/usr/local/bin/clean_old_jenkins_artifacts'],
  }

  cron { 'clean_old_jenkins_artifacts':
    command => '/usr/local/bin/clean_old_jenkins_artifacts_wrapper /var/lib/jenkins',
    user    => 'jenkins',
    hour    => '23',
    minute  => '50',
    require => [File['/usr/local/bin/clean_old_jenkins_artifacts_wrapper'],
                Package['jenkins']],
  }

  file { '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/deduplicate_reproducible_build_jobs_upstream_ISOs',
    require => Package['jenkins'],
  }

  cron { 'deduplicate_reproducible_build_jobs_upstream_ISOs':
    command => '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/6',
    require => File['/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs'],
  }

  file { '/usr/local/bin/manage_latest_iso_symlinks':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/manage_latest_iso_symlinks',
    require => Package['jenkins'],
  }

  cron { 'manage_latest_iso_symlinks':
    command => '/usr/local/bin/manage_latest_iso_symlinks /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/5',
    require => File['/usr/local/bin/manage_latest_iso_symlinks'],
  }

  class  { 'tails::jenkins::iso_jobs_generator':
    ensure            => $automatic_iso_jobs_generator,
    tails_repo        => $tails_repo,
    jenkins_jobs_repo => $jenkins_jobs_repo,
    active_days       => $active_branches_max_age_in_days,
    require           => [
      Class['jenkins'],
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
    ],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => Class['jenkins'],
  }

  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }
}
