# Manage resources that are common to all Tails Jenkins slaves
class tails::jenkins::slave (
  String $api_token,
  String $api_user        = 'Tails',
  String $master_url      = "http://jenkins.${::domain}:8080",
  Stdlib::Fqdn $node_name = $::domain ? { 'lizard' => $::hostname, default => $::fqdn },
) {

  ### Sanity checks

  if $::osfamily != 'Debian' {
    fail('The tails::jenkins::slave class only supports Debian.')
  }

  ### Resources

  $unit_file = '/etc/systemd/system/jenkins-slave.service'

  apt::source { 'tails-jenkins-slave':
    location => 'http://deb.tails.boum.org/',
    release  => 'jenkins-slave',
    repos    => 'main',
  }

  apt::pin { 'jenkins-slave':
    packages => 'jenkins-slave',
    origin   => 'deb.tails.boum.org',
    priority => 991,
  }

  package { 'jenkins-slave':
    ensure  => present,
    require => [
      Apt::Source['tails-jenkins-slave'],
      Apt::Pin['jenkins-slave'],
    ],
  }

  file { '/usr/local/share/jenkins-slave-download':
    source => 'puppet:///modules/tails/jenkins/slaves/jenkins-slave-download',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/usr/local/share/jenkins-enable-node':
    source  => 'puppet:///modules/tails/jenkins/slaves/jenkins-enable-node',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['python3-jenkins'],
  }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  file { '/etc/jenkins/jenkins_apikey':
    ensure  => present,
    content => $api_token,
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
  }

  file { '/etc/tmpfiles.d/tails-jenkins-slave.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "d  /run/jenkins  0775  root  jenkins  -\n",
    require => User[jenkins],
  }

  file { '/etc/systemd/system/jenkins-slave.service.d':
    ensure => directory,
  }
  file { '/etc/systemd/system/jenkins-slave.service.d/cleanup-workspace.conf':
    content => "[Service]\nExecStartPre=/usr/bin/sudo /bin/rm -rf /var/lib/jenkins/workspace/\n",
  }

  package { 'python3-jenkins':
    ensure  => present,
  }

  file { $unit_file:
    content => template('tails/jenkins/jenkins-slave.service.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      File['/usr/local/share/jenkins-enable-node'],
      File['/usr/local/share/jenkins-slave-download'],
      File['/etc/jenkins/jenkins_apikey'],
    ],
  }

  service { 'jenkins-slave':
    ensure   => running,
    enable   => true,
    provider => systemd,
    require  => File[$unit_file],
  }

  @user { 'jenkins':
      membership => minimum,
      groups     => ['sudo'],
      require    => Package['jenkins-slave'],
      home       => '/var/lib/jenkins',
      system     => true,
    }

  file { '/var/lib/jenkins':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0755',
    require => Package['jenkins-slave'],
  }

  file { '/var/lib/jenkins/.netrc':
    mode   => '0600',
    owner  => 'jenkins',
    group  => 'jenkins',
    source => 'puppet:///modules/tails_secrets_jenkins/jenkins/slaves/iso-history/netrc',
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => File['/var/lib/jenkins'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }

  sshkeys::set_client_key_pair { 'jenkins@jenkins-slave':
    keyname => 'jenkins@jenkins-slave',
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

}
