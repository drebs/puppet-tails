# Manage resources that are common to all Tails Jenkins slaves
class tails::jenkins::slave (
  String $master_url,
  Stdlib::Fqdn $node_name = $::hostname,
) {

  ### Sanity checks

  if $::osfamily != 'Debian' {
    fail('The tails::jenkins::slave class only supports Debian.')
  }

  ### Resources

  apt::sources_list { 'tails-jenkins-slave.list':
    content => "deb http://deb.tails.boum.org/ jenkins-slave main\n",
  }

  apt::preferences_snippet { 'jenkins-slave':
    pin      => 'origin deb.tails.boum.org',
    priority => 991,
  }

  package { 'jenkins-slave':
    ensure  => present,
    require => [
      Apt::Sources_list['tails-jenkins-slave.list'],
      Apt::Preferences_snippet['jenkins-slave'],
    ],
  }

  file_line { 'master_url':
    path    => '/etc/default/jenkins-slave',
    match   => '#?JENKINS_URL=(?:".*")?',
    line    => "JENKINS_URL='${master_url}'",
    require => Package['jenkins-slave'],
    notify  => Service['jenkins-slave'],
  }

  file_line { 'jenkins_hostname':
    path    => '/etc/default/jenkins-slave',
    match   => '#?JENKINS_HOSTNAME=(?:".*")?',
    line    => "JENKINS_HOSTNAME='${node_name}'",
    require => Package['jenkins-slave'],
    notify  => Service['jenkins-slave'],
  }

  service { 'jenkins-slave':
    ensure    => running,
    hasstatus => false,
    pattern   => '/usr/bin/daemon --name=jenkins-slave',
    require   => File_line['master_url', 'jenkins_hostname'],
  }

  file { '/var/lib/jenkins':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0755',
    require => Package['jenkins-slave'],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => File['/var/lib/jenkins'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }

  # SSH keys

  sshkeys::set_client_key_pair { 'jenkins@jenkins-slave':
    keyname => 'jenkins@jenkins-slave',
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

}
