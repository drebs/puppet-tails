# Configure an APT proxy for Tails' Jenkins use
# Requires: tails::apt_cacher_ng or another way to manage
# Package['apt-cacher-ng'] and Service['apt-cacher-ng']
class tails::jenkins::apt_proxy (
  Pattern[/\A[a-z_-]+\z/] $tails_git_user = 'tails-git',
  Stdlib::Absolutepath $tails_git_home    = '/var/lib/tails-git',
  String $tails_git_remote                = 'https://git-tails.immerda.ch/tails',
  String $tails_git_revision              = 'devel',
) {

  # Custom apt-cacher-ng configuration to support ISO builds.
  file { '/etc/apt-cacher-ng/builds.conf':
    source  => 'puppet:///modules/tails/apt-cacher-ng/isobuilds.conf',
    require => Package['apt-cacher-ng'],
    owner   => root,
    group   => root,
    mode    => '0644',
  }

  # Keep the acng configuration up-to-date for ISO builds

  $tails_git_dir = "${tails_git_home}/git"

  user { $tails_git_user:
    home       => $tails_git_home,
    managehome => true,
    system     => true,
  }

  vcsrepo { $tails_git_dir:
    provider => git,
    source   => $tails_git_remote,
    revision => $tails_git_revision,
    user     => $tails_git_user,
  }

  cron { 'update-acng-config':
    command => "cd '${tails_git_dir}' && ./auto/scripts/update-acng-config",
    user    => root,
    hour    => '18',
    minute  => '35',
    notify  => Service['apt-cacher-ng'],
    require => [
      Package['apt-cacher-ng'],
      Vcsrepo[$tails_git_dir],
    ],
  }

  # Periodically empty the parts of acng's cache that cannot easily be
  # cleaned up in a more clever way, and would otherwise be growing
  # forever. Ideally we would instead use DontCache, but it causes
  # lots of additional network traffic, network connection book-keeping,
  # and seems to work quite bad generally.
  #
  # At of 2016-07-15, these are:
  #  * deb.tails.boum.org: cached dists+pool for our custom APT repository;
  #    it would be cleaned up automatically by acng, but it happens
  #    to be caught by the glob, which is no big deal -- KISS!
  #  * tagged.snapshots.deb.tails.boum.org,
  #    time-based.snapshots.deb.tails.boum.org: dists for our APT snapshots;
  #    the "upstream" APT snapshots repository deletes old dists on its side,
  #    so acng's regular expiration process gets 404 and errs on the safe side
  #    i.e. does not delete any content; hence we have to delete these dists
  #    on this side ourselves; let's hope that the content of the corresponding
  #    pools (tailssnapshots*) will be properly garbage-collected by acng's
  #    regular expiration process, once the files in there are not referenced
  #    by any remaining dist
  #  * torbrowser-archive.tails.boum.org: as its name says; not an APT repo,
  #    so can't be automatically cleaned up by acng

  cron { 'prune-acng-cache':
    command => 'rm -rf /var/cache/apt-cacher-ng/*.tails.boum.org',
    user    => 'apt-cacher-ng',
    weekday => 'Thursday',
    hour    => '23',
    minute  => '57',
    require => Package['apt-cacher-ng'],
  }

}
