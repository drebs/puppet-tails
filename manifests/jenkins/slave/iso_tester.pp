# Manage a Jenkins slave ready to run the Tails ISO test suite.
class tails::jenkins::slave::iso_tester (
  String $master_url,
  Enum['present', 'absent'] $ensure      = 'present',
  Stdlib::Absolutepath $config_basedir   = '/etc/TailsToaster',
  Boolean $manage_temp_dir_mount         = false,
  String $root_ssh_pubkey_name           = "root@${::fqdn}",
  Stdlib::Absolutepath $temp_dir         = '/tmp/TailsToaster',
  $temp_dir_backing_device               = undef,
  String $temp_dir_fs_type               = 'ext4',
  String $temp_dir_mount_options         = 'relatime,acl',
  String $test_suite_shared_secrets_repo = 'tails@git.tails.boum.org:test-suite-shared-secrets',
  Boolean $manage_email_server           = true,
) {

  user { 'jenkins':
    membership => minimum,
    groups     => [ 'sudo' ],
    home       => '/var/lib/jenkins',
    system     => true,
  }

  class { '::tails::jenkins::slave':
    master_url => $master_url,
    require    => User['jenkins'],
  } -> class { 'tails::tester::check_po': }

  class { '::tails::tester':
    manage_temp_dir_mount   => $manage_temp_dir_mount,
    temp_dir_backing_device => $temp_dir_backing_device,
    temp_dir                => $temp_dir,
    temp_dir_fs_type        => $temp_dir_fs_type,
    temp_dir_mount_options  => $temp_dir_mount_options,
  }

  if $manage_email_server {
    include tails::tester::support::email
  }

  file { '/etc/systemd/system/jenkins-slave.service.d':
    ensure => directory,
  }
  file { '/etc/systemd/system/jenkins-slave.service.d/cleanup-workspace.conf':
    content => "[Service]\nExecStartPre=/bin/rm -rf /var/lib/jenkins/workspace/\n",
  }

  file {
    $config_basedir:
      ensure => directory,
      mode   => '0755',
      owner  => 'root',
      group  => 'root';
    "${config_basedir}/local.d":
      source  => "puppet:///modules/tails_secrets_jenkins/jenkins/slaves/${::fqdn}/TailsToaster_config",
      owner   => 'jenkins',
      group   => 'jenkins',
      purge   => true,
      recurse => true;
  }

  vcsrepo { "${config_basedir}/common.d":
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => root,
    provider => git,
    source   => $test_suite_shared_secrets_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$root_ssh_pubkey_name],
      Package['git'],
      File[$config_basedir],
    ],
  }

  file { '/var/lib/jenkins/.netrc':
    ensure => $ensure,
    mode   => '0600',
    owner  => 'jenkins',
    group  => 'jenkins',
    source => 'puppet:///modules/tails_secrets_jenkins/jenkins/slaves/isotesters/iso-history/netrc',
  }

  file { '/var/run/jenkins':
    ensure => directory,
    mode   => '0755',
    owner  => 'jenkins',
    group  => 'root',
  }

  file { '/usr/local/bin/wrap_test_suite':
    ensure => $ensure,
    source => 'puppet:///modules/tails/jenkins/slaves/isotesters/wrap_test_suite',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/post_test_cleanup':
    ensure => $ensure,
    source => 'puppet:///modules/tails/jenkins/slaves/isotesters/post_test_cleanup',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  # Prevent some Recommends pulled by GTK3 to be installed.
  # Some of them imply running daemons we don't need, which slows down the boot
  # ... and we're going to reboot these VMs quite often.
  package { ['colord', 'libsane', 'sane-utils']:
    ensure => absent,
  }

  sshkeys::set_authorized_keys{ 'jenkins_master_to_iso_tester':
    keyname => "jenkins@jenkins-master.${::domain}",
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
  }

  # Workaround incompatibility between Stretch's Cucumber
  # and the cucumber-testresult-plugin we have on Jenkins
  # (https://labs.riseup.net/code/issues/11739#note-11),
  # by installing Jessie's cucumber and ruby-gherkin, rebuilt
  # against Stretch (so that the *.so etc. are built for Ruby 2.3).
  $stretch_and_newer_only_ensure = $::lsbdistcodename ? {
    'jessie' => absent,
    default  => present,
  }
  apt::preferences_snippet { [
    'cucumber',
    'ruby-gherkin',
    'ruby-rspec',
    'ruby-rspec-core',
    'ruby-rspec-expectations',
    'ruby-rspec-mocks',
  ]:
    ensure   => $stretch_and_newer_only_ensure,
    pin      => 'origin deb.tails.boum.org',
    priority => 1000,
  }
}
