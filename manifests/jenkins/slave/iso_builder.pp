# Manage a Jenkins slave ready to run Tails ISO builds.
class tails::jenkins::slave::iso_builder (
  String $master_url,
  Optional[String] $http_proxy,
  Boolean $use_vagrant = true,
) {

  class { '::tails::jenkins::slave':
    master_url => $master_url,
  }

  if $use_vagrant {
    class { '::tails::iso_builder': }
    user { 'jenkins':
      membership => minimum,
      groups     => ['libvirt', 'kvm', 'libvirt-qemu', 'sudo'],
      require    => Package['jenkins-slave', 'libvirt-daemon-system'],
    }
    file { '/usr/local/bin/compare_artifacts':
      source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/compare_artifacts',
      mode    => '0755',
      owner   => 'root',
      group   => 'root',
      require => Package['diffoscope'],
    }
  } else {
    class { '::tails::builder':
      http_proxy => $http_proxy,
    }
    user { 'jenkins':
      membership => minimum,
      groups     => [ 'sudo' ],
      require    => Package['jenkins-slave'],
    }
  }

  file { '/usr/local/bin/collect_build_environment':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/collect_build_environment',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/cleanup_build_jobs_leftovers':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/cleanup_build_jobs_leftovers',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/sign_artifacts':
    source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/sign_artifacts',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => Class['::tails_secrets_jenkins'],
  }

  file { '/usr/local/bin/decide_if_reproduce':
    source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/decide_if_reproduce',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => [
      File['/etc/jenkins/redmine_apikey'],
      Package['python3-redmine'],
    ],
  }

  ### Email notifications

  ensure_packages([
    'python3-redmine',
    'python3-sh',
  ])

  file { '/usr/local/bin/output_ISO_builds_and_tests_notifications':
    source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/output_ISO_builds_and_tests_notifications',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => [
      File['/etc/jenkins/redmine_apikey'],
      Package[
        'python3-redmine',
        'python3-sh'
      ],
    ],
  }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  file { '/etc/jenkins/redmine_apikey':
    ensure  => 'present',
    source  => 'puppet:///modules/tails_secrets_jenkins/jenkins/slaves/isobuilders/redmine/redmine_apikey',
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
    require => Class['::tails_secrets_jenkins'],
  }

}
