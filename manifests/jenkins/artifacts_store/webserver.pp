# Serve Jenkins artifacts over HTTP.
class tails::jenkins::artifacts_store::webserver (
  Stdlib::Absolutepath $artifacts_store_path,
  Stdlib::Fqdn $hostname,
  Enum['present', 'absent'] $ensure = 'present',
  Boolean $ssl                      = true,
){

  if !defined(Class['nginx']) {
    class { '::nginx':
      access_log => 'noip',
      error_log  => 'none',
      require    => Tails::Dhparam['/etc/nginx/dhparams.pem'],
    }
    tails::dhparam { '/etc/nginx/dhparams.pem': }
  }

  ensure_packages(['libnginx-mod-http-fancyindex'])

  $vhost_common_require = [
    Package[nginx, 'libnginx-mod-http-fancyindex'],
    File[$artifacts_store_path],
  ]
  $vhost_require = $ssl ? {
    true    => concat(
      $vhost_common_require,
      Tails::Letsencrypt::Certonly[$hostname]
    ),
    default => $vhost_common_require,
  }
  nginx::vhostsd { $hostname:
    ensure  => $ensure,
    content => template('tails/jenkins/artifacts_store/nginx.erb'),
    notify  => Service[nginx],
    require => $vhost_require,
  }

  if $ssl == true { tails::letsencrypt::certonly { $hostname: } }

}
