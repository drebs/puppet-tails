# Deploy the Tails pythonlib on systems from git.
# The `$ensure' parameter is passed through to Vcsrepo resources,
# and (with some translation) to File and ensure_packages resources.
class tails::pythonlib (
  String $ensure                      = 'latest',
  Stdlib::Absolutepath $clone_basedir = '/var/lib/tails_pythonlib',
  String $repo_origin                 = 'https://git-tails.immerda.ch/pythonlib',
  String $repo_rev                    = 'master',
  String $repo_user                   = 'root',
) {

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  file { $clone_basedir:
    ensure => $directory_ensure,
    owner  => $repo_user,
    group  => $repo_user,
    mode   => '0755',
  }

  $package_ensure = $ensure ? {
    absent  => absent,
    default => present,
  }

  ensure_packages(
    ['python3-sh'],
    {'ensure' => $package_ensure}
  )

  vcsrepo { "${clone_basedir}/git":
    ensure   => $ensure,
    provider => git,
    source   => $repo_origin,
    revision => $repo_rev,
    user     => $repo_user,
    require  => File[$clone_basedir],
  }

  # XXX: support $ensure = absent
  exec { 'install_tails_pythonlib':
    user        => root,
    group       => root,
    cwd         => "${clone_basedir}/git",
    command     => 'python3 setup.py clean && python3 setup.py install',
    umask       => '0022',
    subscribe   => Vcsrepo["${clone_basedir}/git"],
    refreshonly => true,
    require     => Vcsrepo["${clone_basedir}/git"],
  }
}
