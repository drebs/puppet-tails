# Manage systemd
class tails::systemd (
  Boolean $use_backport = false,
) {

  $packages = [
    'systemd',
    'systemd-sysv',
    'systemd-container',
    'systemd-journal-remote',
    'systemd-coredump',
    'systemd-tests',
    'libpam-systemd',
    'libnss-myhostname',
    'libnss-mymachines',
    'libnss-resolve',
    'libnss-systemd',
    'libsystemd0',
    'libsystemd-dev',
    'udev',
    'libudev1',
    'libudev-dev',
  ]

  $pin_ensure = $use_backport ? {
    true    => present,
    default => absent,
  }

  apt::pin { 'systemd':
    ensure     => $pin_ensure,
    packages   => $packages,
    originator => 'Debian Backports',
    priority   => 991,
  }

}
