class tails::builder (
  $http_proxy = undef,
) {

  ### Sanity checks

  if $http_proxy {
    validate_string($http_proxy)
  }

  if $::lsbdistcodename !~ /^jessie|stretch|sid$/ {
    fail('This module only supports Debian Jessie, Stretch and sid.')
  }

  ### Resources

  include tails::website_builder

  $jessie_and_older_only_ensure = $::lsbdistcodename ? {
    jessie  => present,
    default => absent,
  }

  if !defined(Apt::Sources_list["tails-builder-${::lsbdistcodename}.list"]) {
    apt::sources_list { "tails-builder-${::lsbdistcodename}.list":
      content => "deb http://deb.tails.boum.org/ builder-${::lsbdistcodename} main\n",
    }
  }

  apt::preferences_snippet { ['debootstrap']:
    ensure   => $jessie_and_older_only_ensure,
    pin      => 'release o=Debian Backports,a=jessie-backports',
    priority => 990,
  }

  apt::preferences_snippet { ['live-build']:
    pin      => 'origin deb.tails.boum.org',
    priority => 991,
  }

  $packages = [
    'dpkg-dev',
    'eatmydata',
    'intltool',
    'libfile-slurp-perl',
    'liblist-moreutils-perl',
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'live-build',
    'psmisc',
    'sudo',
    'syslinux-utils',
    'time',
    'whois',
  ]

  ensure_packages($packages)

  file { '/etc/live':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { '/etc/live/build.conf':
    owner => root,
    group => root,
    mode  => '0644',
  }

  file { '/etc/sudoers.d/tails-builder-defaults':
    owner   => root,
    group   => root,
    mode    => '0440',
    content => "Defaults secure_path=\"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"\n",
    require => File_line['sudo-includedir-sudoers.d'],
  }

  file_line { 'sudo-includedir-sudoers.d':
    ensure  => present,
    path    => '/etc/sudoers',
    line    => '#includedir /etc/sudoers.d',
    require => Package['sudo'],
  }

  if $http_proxy {
    augeas { 'tails_builder_live-build.conf_http_proxy':
      lens    => 'Shellvars.lns',
      incl    => '/etc/live/build.conf',
      changes => "set LB_APT_HTTP_PROXY ${http_proxy}",
      require => File['/etc/live/build.conf'],
    }
  }
  else {
    augeas { 'tails_builder_live-build.conf_http_proxy':
      lens    => 'Shellvars.lns',
      incl    => '/etc/live/build.conf',
      changes => 'rm LB_APT_HTTP_PROXY',
      require => File['/etc/live/build.conf'],
    }
  }

}
