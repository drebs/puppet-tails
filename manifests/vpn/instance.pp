# This class manages a Tails VPN installation using tinc in `switch`
# mode.
#
# It needs by default only two parameters:
#
# * vpn_address: the VPN internal ip address, used to configure the VPN
#   interface. Must be in the CIDR format, e.g 192.168.0.2/24.
#
# * vpn_subnet: the VPN internal subnet, which is used to
#   configure the routing. Must be in the form of subnet/netmask, e.g
#   192.168.0.0/255.255.255.0.
#
# Note that this manifest does not check that you pass consistent
# values for $vpn_address CIDR and $vpn_subnet netmask. Obviously you
# should. Take care to do so!
#
# It's also possible to define the ipaddress tinc will listen on with
# the ipaddress parameter. Usefull if you have several interfaces.

define tails::vpn::instance (
  Stdlib::IP::Address::V4::CIDR $vpn_address,
  Pattern[/\A([0-9]{1,3}\.){3}[0-9]{1,3}\/([0-9]{1,3}\.){3}[0-9]{1,3}\z/] $vpn_subnet,
  Optional[String] $master                   = undef,
  Stdlib::Ipv4 $ip_address                   = $::ipaddress,
  Boolean $route_to_lizard_vm_net            = false,
  Optional[Stdlib::Ipv4] $lizard_vpn_address = undef,
  Enum['present', 'absent'] $ensure          = present,
  Optional[String] $proxy                    = undef,
){

  $vpn_spaddress = split($vpn_address, '/')
  $vpn_ip = $vpn_spaddress[0]

  $vpn_spsubnet = split($vpn_subnet, '/')
  $vpn_net = $vpn_spsubnet[0]
  $vpn_netmask = $vpn_spsubnet[1]

  validate_re($name, '.+@.+')
  $sp_name = split($name, '@')
  $hostname = $sp_name[0]
  $vpn_name = $sp_name[1]
  $interface = $vpn_name

  validate_string(
    $vpn_name,
    $hostname,
  )

  if $route_to_lizard_vm_net {
    if $lizard_vpn_address == undef {
      fail('$lizard_vpn_address must be defined if $route_to_lizard_vm_net is true')
    }
  }

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  include tails::vpn

  file {
    "/etc/tinc/${vpn_name}":
      ensure  => $directory_ensure,
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      require => Package['tinc'];
    "/etc/tinc/${vpn_name}/hosts":
      ensure  => $directory_ensure,
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0700';
    "/etc/tinc/${vpn_name}/tinc-up":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('tails/vpn/tinc-up.erb');
    "/etc/tinc/${vpn_name}/tinc-down":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('tails/vpn/tinc-down.erb');
    "/etc/tinc/${vpn_name}/tinc.conf":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      content => template('tails/vpn/tinc.conf.erb');
    "/etc/tinc/${vpn_name}/rsa_key.priv":
      ensure => $ensure,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
  }

  concat::fragment { "nets_boot_${vpn_name}":
    target  => '/etc/tinc/nets.boot',
    content => "${vpn_name}\n",
    notify  => Service['tinc'],
  }

  @@::tails::vpn::host { $hostname:
    ensure      => $ensure,
    ip_address  => $ip_address,
    vpn_address => $vpn_address,
    vpn_name    => $vpn_name,
    tag         => $vpn_name,
    require     => File["/etc/tinc/${vpn_name}/hosts"],
    notify      => Service['tinc'],
  }

  Tails::Vpn::Host <<| tag == $vpn_name |>>

}
