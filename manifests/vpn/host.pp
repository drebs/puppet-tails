# This class manages a Tails VPN host related to a tails::vpn::instance.

define tails::vpn::host (
  Stdlib::Ipv4 $ip_address,
  Stdlib::IP::Address::V4::CIDR $vpn_address,
  String $vpn_name,
  Enum['present', 'absent'] $ensure = 'present',
){

  $host_file = "/etc/tinc/${vpn_name}/hosts/${name}"

  concat { $host_file:
    owner => 'root',
    group => 'root',
    mode  => '0600',
  }

  concat::fragment { "${name}_address":
    target  => $host_file,
    content => "Address=${ip_address}\n",
    order   => 10,
  }

  concat::fragment { "${name}_port":
    target  => $host_file,
    content => "Port=655\n",
    order   => 20,
  }

  concat::fragment { "${name}_compression":
    target  => $host_file,
    content => "Compression=10\n",
    order   => 30,
  }

  concat::fragment { "${name}_subnet":
    target  => $host_file,
    content => "Subnet=${vpn_address}\n",
    order   => 40,
  }

  concat::fragment { "${name}_pubkey":
    target => $host_file,
    source => "puppet:///modules/tails/vpn/${vpn_name}/public_keys/${name}.crt",
    order  => 50,
  }
}
