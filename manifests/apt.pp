# Manage APT on Tails systems
class tails::apt (
  String $cron_mode,
  Variant[Boolean, String] $proxy,
  Optional[Stdlib::Port] $proxy_port,
) {

  include ::apt

  if $proxy {
    class { '::apt::proxy_client':
      proxy => $proxy,
      port  => $proxy_port
    }
  }

  case $cron_mode {
    'apticron':           {
      class { '::apt::apticron': ipaddressnum => 0 }
      package { 'cron-apt': ensure => purged }
      cron { 'apt_cron_every_N_hours': ensure => absent }
    }
    'cron::dist_upgrade': {
      include ::apt::cron::dist_upgrade
      package { 'apticron': ensure => purged }
    }
    default: {}
  }

  class { '::apt::listchanges': which => 'news' }

  # Ensure we are told by our monitoring system when reboot-notifier
  # creates its flag file, but disable reboot-notifier's own email
  # notification. system instead.
  include apt::reboot_required_notify
  file_line { 'disable_reboot-notifier_email':
    path  => '/etc/default/reboot-notifier',
    match => 'NOTIFICATION_EMAIL=.*',
    line  => 'NOTIFICATION_EMAIL=',
  }

  ::apt::apt_conf { '08_periodoc_autoclean_interval':
    content => 'APT::Periodic::AutocleanInterval "1";',
  }
  ::apt::apt_conf { '09_periodic_clean_interval':
    content => 'APT::Periodic::CleanInterval "1";',
  }
  ::apt::apt_conf { '10_dont_keep_downloaded_packages':
    content => 'APT::Keep-Downloaded-Packages "false";',
  }
}
