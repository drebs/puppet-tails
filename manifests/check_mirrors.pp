# Manage checking of Tails mirrors
class tails::check_mirrors (
  String $email_recipient,
  Stdlib::Absolutepath $homedir      = '/var/lib/tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $user       = 'tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $repo_user  = 'tails',
  Stdlib::Host $repo_host            = 'git.tails.boum.org',
  Pattern[/\A[a-z_-]+\z/] $repo_name = 'check-mirrors',
  String $repo_rev                   = 'master',
  String $repo_ensure                = 'latest',
) {

  validate_email_address($email_recipient)

  $repo_checkout    = "${homedir}/check-mirrors"
  $needed_packages  = ['curl', 'ruby-nokogiri', 'wget']
  $gnupg_homedir    = "${homedir}/.gnupg"
  $signing_key_file = "${homedir}/tails-signing.key"

  package { $needed_packages: ensure => present }

  user::managed { $user:
    ensure  => present,
    homedir => $homedir,
  }

  postfix::mailalias { $user:
    recipient => $email_recipient,
  }

  exec { "SSH key pair for user ${user}":
    command => "ssh-keygen -t rsa -b 4096 -N '' -f \"${homedir}/.ssh/id_rsa\"",
    user    => $user,
    creates => "${homedir}/.ssh/id_rsa",
  }

  file { $signing_key_file:
    owner  => $user,
    group  => $user,
    mode   => '0600',
    source => 'puppet:///modules/tails/check_mirrors/tails-signing.key',
  }

  file { $gnupg_homedir:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  exec { 'Import Tails signing key':
    user        => $user,
    group       => $user,
    command     => "gpg --batch --quiet --import '${signing_key_file}'",
    environment => "HOME=${homedir}",
    require     => File[$gnupg_homedir, $signing_key_file],
    subscribe   => File[$signing_key_file],
    refreshonly => true,
  }

  vcsrepo { $repo_checkout:
    ensure   => $repo_ensure,
    provider => git,
    source   => "${repo_user}@${repo_host}:${repo_name}.git",
    revision => $repo_rev,
    user     => $user,
    require  => [
      User[$user], File[$homedir], Sshkey[$repo_host],
      Exec["SSH key pair for user ${user}"]
    ],
  }

  cron { 'tails_check_mirrors':
    command => "sleep \$(( \$( </dev/urandom od -N2 -t u2 -A none ) >> 5 )) && \"${repo_checkout}/check-mirrors.rb\"",
    user    => $user,
    hour    => 0,
    minute  => 16,
    require => [Vcsrepo[$repo_checkout], Package[$needed_packages],
                Postfix::Mailalias[$user], Exec['Import Tails signing key']],
  }

}
