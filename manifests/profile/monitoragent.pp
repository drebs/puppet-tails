# profile for our monitoring satellites
class tails::profile::monitoragent (
) {

  if $::facts['os']['release']['major'] == '9' {
    $packages_from_stretch_backports = [
      'icinga2',
      'icinga2-bin',
      'icinga2-common',
      'libicinga2',
      'monitoring-plugins',
      'monitoring-plugins-basic',
      'monitoring-plugins-common',
      'monitoring-plugins-standard',
    ]
    $packages_from_stretch_backports.each |String $package| {
      apt::pin { $package:
        packages   => $package,
        originator => 'Debian Backports',
        codename   => 'stretch-backports',
        priority   => 991,
      }
    }
  }
  $plugin_packages = [
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'monitoring-plugins-contrib',
    'python3-nagiosplugin',
  ]
  ensure_packages($plugin_packages, {ensure => latest} )

  include monitoring::agent

}
