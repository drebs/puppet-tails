# profile for our monitoring master
class tails::profile::monitormaster (
  String $spamaddress = 'tails-sysadmins@boum.org',
) {

  $plugin_packages = [
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'monitoring-plugins-contrib',
    'python3-nagiosplugin',
  ]
  ensure_packages($plugin_packages, {ensure => latest} )

  class { 'monitoring::master':
    spamaddress => $spamaddress,
  }

  tor::daemon::hidden_service { "icinga-api-${::fqdn}":
    ports   => [ '5665' ],
  }

}
