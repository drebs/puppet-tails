# An rss2email feed
define tails::profile::rss2email::feed (
  String               $url,
  String               $from    = $tails::profile::rss2email::default_from,
  String               $to      = $tails::profile::rss2email::default_to,
  Hash[String, String] $filters = {},
) {

  $user = $tails::profile::rss2email::user
  $database = $tails::profile::rss2email::database
  $config_file = $tails::profile::rss2email::config_file

  # add feed unless already present in database
  exec { "r2e add ${name}":
    command => "/usr/bin/r2e add '${name}' '${url}' '${to}'",
    unless  => "jq < ${database} '.feeds[] | select(.name == \"${name}\")'",
    require => Exec['r2e new'],
    user    => $user,
  }

  $settings = {
    'url'         => $url,
    'from'        => $from,
    'to'          => $to,
    'name-format' => '{feed-title}',
  }

  # ensure up-to-date feed config
  create_ini_settings(
    { "feed.${name}" => $settings },
    { path => $config_file, require => Exec["r2e add ${name}"] }
  )

  $filters.each | String $filtername, String $ensure | {
    tails::profile::rss2email::filter { "${filtername} for ${name}":
      ensure    => $ensure,
      feed_name => $name,
      filter    => $filtername,
      require   => Exec["r2e add ${name}"],
    }
  }

}
