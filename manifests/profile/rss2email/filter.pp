# Install custom rss2email post-process filters
define tails::profile::rss2email::filter (
  String $feed_name,
  String $filter,
  String $ensure = present,
) {
  ini_setting { "rss2email feed filter: ${name}":
    ensure  => $ensure,
    path    => $tails::profile::rss2email::config_file,
    section => "feed.${feed_name}",
    setting => 'post-process',
    value   => "feedfilters ${filter}",
  }

}
