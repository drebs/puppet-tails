# Set up custom GRUB configuration.

class tails::grub {

  exec { 'update-grub': refreshonly => true }

  if $::hostname == 'lizard' {
    # lint:ignore:140chars -- long kernel command-line
    $grub_changes = [
      'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor ipv6.disable=1 console=ttyS1,115200n8 ip=198.252.153.59::198.252.153.1:255.255.255.0:lizard:eth1:off rootdelay=20"\'',
      'set GRUB_TERMINAL \'serial\'',
      'set GRUB_SERIAL_COMMAND \'"serial --unit=1 --speed=115200 --word=8 --parity=no --stop=1"\'',
    ]
    # lint:endignore
    package { 'dropbear': ensure => present }

  } elsif $::hostname == 'buse' {
    $grub_changes = [
      'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor ipv6.disable=1 console=tty0 console=ttyS0,115200n8"\'',
      'set GRUB_TERMINAL \'"serial console"\'',
      'set GRUB_SERIAL_COMMAND \'"serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"\'',
    ]

  } elsif $::hostname == 'ecours' {
    $grub_changes = [
      'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor ipv6.disable=1 console=ttyS0,115200n8"\'',
      'set GRUB_TERMINAL \'serial\'',
      'set GRUB_SERIAL_COMMAND \'"serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"\'',
    ]

  } elsif $::hostname == 'sib' {
    $grub_changes = [
      'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor ipv6.disable=1"\'',
      'set GRUB_TERMINAL \'console\'',
      'set GRUB_SERIAL_COMMAND \'""\'',
    ]

  } else {
    case $::virtual {
      physical: {
        $grub_changes = [
          'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor console=ttyS0,115200n8"\'',
          'set GRUB_TERMINAL \'serial\'',
          'set GRUB_SERIAL_COMMAND \'"serial --speed=115200 --unit=1 --word=8 --parity=no --stop=1"\'',
        ]
      }
      default:  {
        $grub_changes = [
          'set GRUB_CMDLINE_LINUX \'"apparmor=1 security=apparmor elevator=noop console=ttyS0,115200n8"\'',
          'set GRUB_TERMINAL \'serial\'',
          'set GRUB_SERIAL_COMMAND \'"serial --speed=115200 --unit=1 --word=8 --parity=no --stop=1"\'',
        ]
      }
    }
  }

  augeas { 'GRUB_CMDLINE_LINUX':
    context => '/files/etc/default/grub',
    changes => $grub_changes,
    notify  => Exec['update-grub'],
    require => Package['apparmor'],
  }

}
