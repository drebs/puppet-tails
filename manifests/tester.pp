# Set up what's necessary to run automated tests
class tails::tester (
  Boolean $manage_temp_dir_mount            = false,
  String $temp_dir                          = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
) {

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::tester module only supports Debian Stretch.')
  }

  ### Resources

  $stretch_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  $libvpx_package = $::lsbdistcodename ? {
    stretch => 'libvpx4',
    default => undef,
  }

  class { '::libvirt::host':
    qemu_security_driver => 'none',
  }

  # Stretch: workaround #12142
  apt::sources_list { 'tails-isotester-stretch.list':
    ensure  => $stretch_and_older_only_ensure,
    content => "deb http://deb.tails.boum.org/ isotester-stretch main\n",
  }
  apt::preferences_snippet { 'qemu':
    ensure   => $stretch_and_older_only_ensure,
    package  => 'qemu*',
    pin      => 'origin deb.tails.boum.org',
    priority => 991,
  }

  $tester_packages = [
    'cucumber',
    'devscripts',
    'dnsmasq-base',
    'gawk',
    'git',
    'i18nspector',
    'libav-tools',
    'libcap2-bin',
    'libsikulixapi-java',
    'libvirt0',
    'libvirt-clients',
    'libvirt-daemon-system',
    'libvirt-dev',
    $libvpx_package,
    'obfs4proxy',
    'ovmf',
    'pry',
    'python-jabberbot',
    'python-potr',
    'qemu-kvm',
    'qemu-system-x86',
    'redir',
    'ruby-guestfs',
    'ruby-json',
    'ruby-libvirt',
    'ruby-net-irc',
    'ruby-packetfu',
    'ruby-rb-inotify',
    'ruby-rjb',
    'ruby-rspec',
    'ruby-test-unit',
    'seabios',
    'tcpdump',
    'unclutter',
    'virt-viewer',
    'x11vnc',
    'x264',
    'xtightvncviewer',
    'xvfb',
  ]

  ensure_packages($tester_packages)

  file { '/etc/tmpfiles.d/TailsToaster.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "d  ${temp_dir}  0755  root  root  -\n",
  }

  if $manage_temp_dir_mount {
    validate_string(
      $temp_dir_backing_device,
      $temp_dir_fs_type,
      $temp_dir_mount_options
    )

    mount { $temp_dir:
      ensure  => mounted,
      device  => $temp_dir_backing_device,
      fstype  => $temp_dir_fs_type,
      options => $temp_dir_mount_options,
      require => File['/etc/tmpfiles.d/TailsToaster.conf']
    }

  }
}
