# Manage credentials for users who admin Prosody
define tails::prosody::admin () {

  user::groups::manage_user { "${name}_in_prosody":
    user    => $name,
    group   => 'prosody',
    require => Package['prosody'],
  }

  user::groups::manage_user { "${name}_in_prosody_admin":
    user    => $name,
    group   => 'prosody_admin',
    require => Group['prosody_admin'],
  }

}
