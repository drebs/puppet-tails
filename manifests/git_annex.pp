# Manage the git-annex package
class tails::git_annex (
  Enum['present', 'absent'] $ensure         = present,
  Boolean $use_backport                     = false,
  Variant[Boolean, String] $with_recommends = 'default',
) {

  $install_options = $with_recommends ? {
    false   => [ '--no-install-recommends' ],
    default => [],
  }

  $pin = $use_backport ? {
    true    => "release o=Debian Backports,n=${::lsbdistcodename}-backports",
    default => "release o=Debian,n=${::lsbdistcodename}",
  }

  package { 'git-annex':
    ensure          => $ensure,
    install_options => $install_options,
    require         => Apt::Preferences_snippet['git', 'git-annex', 'git-man'],
  }

  apt::preferences_snippet { ['git', 'git-annex', 'git-man']:
    ensure   => $ensure,
    pin      => $pin,
    priority => 991,
  }

  file { '/usr/local/bin/pull-git-annex':
    ensure => $ensure,
    source => 'puppet:///modules/tails/git-annex/pull-git-annex',
    owner  => root,
    group  => root,
    mode   => '0755',
  }

}
