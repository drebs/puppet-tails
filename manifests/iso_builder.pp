# Make a system ready to build Tails ISO and USB images.
class tails::iso_builder (
  Enum['present', 'absent'] $ensure = 'present',
) {

  apt::pin { 'diffoscope':
    ensure     => $ensure,
    packages   => 'diffoscope',
    originator => 'Debian Backports',
    codename   => "${::lsbdistcodename}-backports",
    priority   => 991,
  }

  package { 'diffoscope':
    ensure  => $ensure,
    require => Apt::Pin['diffoscope'],
  }

  apt::pin { 'vmdb2':
    ensure     => $ensure,
    packages   => 'vmdb2',
    originator => 'Debian',
    codename   => 'bullseye',
    priority   => 991,
  }

  package { 'vmdb2':
    ensure  => $ensure,
    require => Apt::Pin['vmdb2'],
  }

  $builder_packages = [
    'dnsmasq-base',
    'ebtables',
    'faketime',
    'git',
    'libvirt-daemon-system',
    'pigz',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
  ]

  ensure_packages(
    $builder_packages,
    {'ensure' => $ensure}
  )

  # FIXME: remove once deployed everywhere
  package { 'vmdebootstrap': ensure => purged }

}
