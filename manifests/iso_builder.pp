class tails::iso_builder (
  Enum['present', 'absent'] $ensure = 'present',
) {

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::iso_builder module only supports Debian Stretch.')
  }

  ### Resources

  $package_ensure = $ensure ? {
    absent  => absent,
    default => present,
  }

  apt::preferences_snippet { 'diffoscope':
    ensure   => $ensure,
    package  => 'diffoscope',
    pin      => 'release o=Debian,n=stretch-backports',
    priority => 991,
  }

  package { 'diffoscope':
    ensure  => $package_ensure,
    require => Apt::Preferences_snippet['diffoscope'],
  }

  $builder_packages = [
    'git',
    'rake',
    'libvirt-daemon-system',
    'dnsmasq-base',
    'ebtables',
    'qemu-system-x86',
    'qemu-utils',
    'vagrant',
    'vagrant-libvirt',
    'sudo',
    'vmdebootstrap'
  ]

  ensure_packages(
    $builder_packages,
    {'ensure' => $package_ensure}
  )

}
