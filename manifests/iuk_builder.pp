# Make a system ready to build Tails IUKs.
# Design doc: https://tails.boum.org/contribute/design/incremental_upgrades/
class tails::iuk_builder {

  $buster_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    buster  => present,
    default => absent,
  }

  apt::source { 'tails-iukbuilder-stretch':
    ensure   => $buster_and_older_only_ensure,
    location => 'http://deb.tails.boum.org/',
    release  => 'iukbuilder-stretch',
    repos    => 'main',
  }

  apt::pin { 'squashfs-tools':
    ensure   => $buster_and_older_only_ensure,
    packages => ['squashfs-tools', 'libzstd1'],
    origin   => 'deb.tails.boum.org',
    priority => 991,
  }

  $iuk_builder_packages = [
    'attr',
    'bsdtar',
    'libarchive-tar-wrapper-perl',
    'libcarp-assert-more-perl',
    'libcarp-assert-perl',
    'libclass-xsaccessor-perl',
    'libdevice-cdio-perl',
    'libdpkg-perl',
    'libfile-which-perl',
    'libfilesys-df-perl',
    'libfunction-parameters-perl',
    'libgnupg-interface-perl',
    'libipc-system-simple-perl',
    'libmoo-perl',
    'libmoox-handlesvia-perl',
    'libmoox-late-perl',
    'libmoox-options-perl',
    'libnamespace-clean-perl',
    'libpath-tiny-perl',
    'libstring-errf-perl',
    'libtry-tiny-perl',
    'libtypes-path-tiny-perl',
    'libyaml-libyaml-perl',
    'libyaml-perl',
    'nocache',
    'rsync',
    'squashfs-tools',
  ]

  ensure_packages($iuk_builder_packages)

  file { '/usr/local/bin/wrap_tails_create_iuks':
    ensure => present,
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/wrap_tails_create_iuks',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

}
