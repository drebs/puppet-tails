# Manage a private rsync server
class tails::rsync (
  Array[String] $auth_users, # array of user names
  Stdlib::Absolutepath $basedir = '/srv/rsync',
  String $secrets_source        = 'puppet:///modules/tails_secrets_rsync/users/tails.secrets',
  ) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $data_dir    = "${basedir}/tails"
  $secrets_dir = '/etc/rsync.d'
  $secrets     = "${secrets_dir}/tails.secrets"

  ### Resources

  package { 'acl': ensure => present }
  package { 'rsync': ensure => present }

  user::managed { 'rsync_tails': ensure => present }

  file { [ $basedir, $secrets_dir ]:
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { $data_dir:
    ensure  => directory,
    owner   => root,
    group   => rsync_tails,
    mode    => '2755',
    require => [ File[$basedir], User::Managed['rsync_tails'] ],
  }

  file { '/etc/rsyncd.conf':
    content => template('tails/rsync/rsyncd.conf.erb'),
    owner   => root,
    group   => root,
    mode    => '0600',
    notify  => Service[rsync],
  }

  file { $secrets:
    source  => $secrets_source,
    owner   => root,
    group   => 'rsync_tails',
    mode    => '0640',
    require => User::Managed['rsync_tails'],
  }

  service { 'rsync':
    ensure  => running,
    require => Package[rsync],
  }

  $acls = "${common::moduledir::module_dir_path}/tails/rsync-dir.acl"
  file { $acls:
    content => "group::r-X\ndefault:group::r-X\nother::r-X\ndefault:other::r-X\n",
    mode    => '0600',
    owner   => root,
    group   => root;
  }
  exec {
    'set-acl-on-rsync-data-dir':
      command     => "setfacl -R -M ${acls} ${data_dir}",
      require     => [ File[$acls, $data_dir], Package['acl'] ],
      subscribe   => File[$acls],
      refreshonly => true;
  }

}
