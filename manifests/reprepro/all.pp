# Manage all Tails APT repositories
class tails::reprepro::all (
  Hash $release_managers_sshkeys,
  Hash $snapshots_architectures = $tails::reprepro::params::snapshots_architectures,
  String $snapshots_signwith    = $tails::reprepro::params::snapshots_signwith,
) inherits tails::reprepro::params {

  class { '::nginx':
    access_log => 'noip',
    error_log  => 'none'
  }

  include ::reprepro

  include ::tails_secrets_apt

  class { '::tails::reprepro::custom':
    require => Class['::nginx'],
  }

  class { 'tails::reprepro::snapshots::tagged': }

  class { 'tails::reprepro::snapshots::time_based':
    architectures            => $snapshots_architectures,
    release_managers_sshkeys => $release_managers_sshkeys,
    signwith                 => $snapshots_signwith,
  }

}
