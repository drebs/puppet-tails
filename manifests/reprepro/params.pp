# Default parameters that are shared between tails::reprepro::* classes.
class tails::reprepro::params () {

  $snapshots_architectures = {
    'stretch'      => ['amd64', 'source'],
    'buster'       => ['amd64', 'source'],
    'sid'          => ['amd64', 'source'],
    'experimental' => ['amd64', 'source'],
    'tails'        => ['amd64', 'source'],
    'torproject'   => ['amd64'],
  }

  $snapshots_signwith = 'yes'

}
