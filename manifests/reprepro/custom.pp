# Manage Tails custom APT repository
class tails::reprepro::custom (
  Stdlib::Absolutepath $basedir = '/srv/reprepro',
  String $email_recipient       = 'root',
  Array[String] $uploaders      = [
    'C92949B8A63BB098+',
    '1D84CCF010CC5BC7',
    '91F73701D9C99DC9',
    '43AB710FAE9FE593+',
    'B14BB0C38D861CF1+',
  ],
  String $origin                = 'Tails',
  Stdlib::Fqdn $web_hostname    = 'deb.tails.boum.org',
  String $onion_hostname        = '',
  Stdlib::Port $web_port        = 80,
  String $git_remote            = 'https://git-tails.immerda.ch/tails'
) {

  ### Sanity checks

  if !defined(Class['::nginx']) {
    fail('Depends on the nginx class')
  }

  if !defined(Class['::reprepro']) {
    fail('Depends on the reprepro class')
  }

  if !defined(Class['::tails_secrets_apt']) {
    fail('Depends on the tails_secrets_apt class')
  }

  ### Class variables

  $git_repo = "${basedir}/tails.git"
  $shell_lib = '/usr/local/share/tails-reprepro/functions.sh'

  ### Resources

  reprepro::repository { 'tails':
    uploaders                    => $uploaders,
    basedir                      => $basedir,
    origin                       => $origin,
    basedir_mode                 => '0751',
    incoming_mode                => '1775',
    manage_distributions_conf    => false,
    manage_incoming_conf         => false,
    handle_incoming_with_inotify => true,
    index_template               => 'tails/reprepro/index.html.erb',
  }

  ensure_packages(['git', 'moreutils'])

  file {

    '/usr/local/share/tails-reprepro':
      ensure => directory,
      mode   => '0755',
      owner  => root,
      group  => reprepro;

    $shell_lib:
      mode    => '0644',
      owner   => root,
      group   => reprepro,
      source  => 'puppet:///modules/tails/reprepro/custom/functions.sh',
      require => File['/usr/local/share/tails-reprepro'];

    '/usr/local/bin/tails-diff-suites':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-diff-suites',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-merge-suite':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-merge-suite',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-suites-list':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-suites-list',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-suites-to-distributions':
      source => 'puppet:///modules/tails/reprepro/custom/tails-suites-to-distributions',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/bin/tails-suites-to-incoming':
      source => 'puppet:///modules/tails/reprepro/custom/tails-suites-to-incoming',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/bin/tails-update-reprepro-config':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-update-reprepro-config',
      require => [
        Exec['tails-reprepro-git-clone'],
        File[$shell_lib],
        File['/usr/local/bin/tails-suites-list'],
        File['/usr/local/bin/tails-suites-to-distributions'],
        File['/usr/local/bin/tails-suites-to-incoming'],
        Package['moreutils'],
      ],
      mode    => '0755',
      owner   => root,
      group   => root;

    "${basedir}/conf/deny_all_uploaders":
      mode    => '0660',
      owner   => root,
      group   => reprepro,
      content => '',
      require => File["${basedir}/conf"];

  }

  cron { 'tails-update-reprepro-config':
    user        => 'reprepro',
    minute      => '*',
    command     => "flock -n /var/lock/tails-update-reprepro-config /usr/local/bin/tails-update-reprepro-config '${git_repo}' '${origin}' '${basedir}'", # lint:ignore:140chars -- command
    require     => File['/usr/local/bin/tails-update-reprepro-config'],
    environment => [ "MAILTO=${email_recipient}" ],
  }

  # Can't use vcsrepo, that doesn't support --mirror before
  # https://github.com/puppetlabs/puppetlabs-vcsrepo/commit/b8f25cea95317a4b2a622e2799f1aa7ba159bdca
  # that is not part of an upstream release as of 20160523
  exec { 'tails-reprepro-git-clone':
    user    => reprepro,
    group   => reprepro,
    cwd     => $basedir,
    command => "git clone --bare --mirror '${git_remote}' '${git_repo}' && chmod -R g+rX '${git_repo}'",
    creates => "${git_repo}/config",
    require => Package['git'],
    timeout => -1,
  }

  exec { 'tails-reprepro-import-keys':
    user        => reprepro,
    group       => reprepro,
    command     => "gpg --homedir '${basedir}/.gnupg' --batch --quiet --import '${tails_secrets_apt::keys}'",
    subscribe   => File[$tails_secrets_apt::keys],
    refreshonly => true,
    notify      => Exec["/usr/local/bin/reprepro-export-key '${basedir}'"],
    require     => Mount[$tails_secrets_apt::gnupg_homedir],
  }

  mailalias { 'reprepro': ensure => present, recipient => ['root']; }

  class { '::tails::reprepro::custom::nginx':
    hostname      => $web_hostname,
    basedir       => $basedir,
    vhost_content => template('tails/reprepro/custom/nginx/site.erb'),
  }

  # Refresh OpenPGP keys

  package { ['dbus-x11', 'parcimonie']:
    ensure          => present,
    install_options => [ '--no-install-recommends' ],
  }

  file { '/etc/systemd/system/parcimonie-reprepro.service':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['dbus-x11', 'parcimonie'],
    content => "[Unit]
Description=Refresh reprepro's GnuPG keyring

[Service]
Type=simple
ExecStart=/usr/bin/dbus-launch /usr/bin/parcimonie --verbose
User=reprepro

[Install]
WantedBy=multi-user.target
",
  }

  service { 'parcimonie-reprepro.service':
    ensure    => running,
    enable    => true,
    provider  => systemd,
    require   => File['/etc/systemd/system/parcimonie-reprepro.service'],
    subscribe => File['/etc/systemd/system/parcimonie-reprepro.service'],
  }

  # Make sure the exported public key is up-to-date
  cron { 'tails-reprepro-export-key':
    user    => reprepro,
    minute  => 17,
    command => "/usr/local/bin/reprepro-export-key '${basedir}'",
    require => Reprepro::Repository['tails'],
  }

  class { '::tails::reprepro::custom::notify_incoming':
    ensure           => present,
    mail_to          => $email_recipient,
    mail_from        => 'reprepro@lizard.tails.boum.org',
    reprepro_basedir => $basedir,
  }
}
