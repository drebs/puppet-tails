# Wrapper around the letsencrypt class
class tails::letsencrypt (
  String $email = 'tails-sysadmins@boum.org',
  Hash $config  = {
    'agree-tos'           => 'True',
    'authenticator'       => 'webroot',
    'keep-until-expiring' => 'True',
    'post-hook'           => '/bin/systemctl reload nginx.service',
    'rsa-key-size'        => 4096,
    'webroot-path'        => '/var/www/html',
  },
) {

  # Sanity checks

  validate_email_address($email)

  # Packages

  ensure_packages([gnutls-bin]) # ships certtool

  case $::lsbdistcodename {
    /^(stretch|sid)$/: {
      $pin = "release o=Debian,n=${::lsbdistcodename}"
    }
    default: {
      fail("Unsupported Debian release ${::lsbdistcodename}")
    }
  }
  $certbot_pinned_packages = [
    certbot,
    python-certbot,
    python-acme,
    python-cryptography,
    python-openssl,
    python-pkg-resources,
    python-psutil,
    python-pyasn1,
    python-setuptools,
  ]
  apt::preferences_snippet { $certbot_pinned_packages:
    pin      => $pin,
    priority => 991;
  }
  package { 'certbot':
    ensure  => installed,
    require => Apt::Preferences_snippet[$certbot_pinned_packages],
  }

  # Other resources

  class { '::letsencrypt':
    email           => $email,
    manage_install  => false,
    install_method  => 'package',
    package_command => 'certbot',
    config          => $config,
    require         => Package[certbot],
  }

}
