class tails::gitolite::hooks::common {

  file {
    # Tails common hook directory
    #
    # We don't use the gitolite one, because when gl-setup is run, all
    # hooks in the gitolite common hook directory will be installed in
    # all repos
    '/var/lib/gitolite/.gitolite/hooks/tails_common':
      ensure => directory,
      mode   => '0755',
      owner  => gitolite,
      group  => gitolite;

    # immerda mirror push
    '/var/lib/gitolite/.gitolite/hooks/tails_common/immerda_mirror-post-update.hook':
      source => 'puppet:///modules/tails/gitolite/hooks/immerda_mirror-post-update.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    # labs mirror push
    '/var/lib/gitolite/.gitolite/hooks/tails_common/labs_mirror-post-update.hook':
      source => 'puppet:///modules/tails/gitolite/hooks/labs_mirror-post-update.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    # hook to run everything in post-update.d/
    '/var/lib/gitolite/.gitolite/hooks/tails_common/post_update.d.hook':
      source => 'puppet:///modules/tails/gitolite/hooks/post-update.d.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    # script to run periodically to make sure the hooks are in the right place
    '/usr/local/sbin/gitolite_hooks':
      source => 'puppet:///modules/tails/gitolite/install_hooks.sh',
      mode   => '0744',
      owner  => gitolite,
      group  => gitolite;

    # script to run periodically to make sure the remotes are configured
    '/usr/local/sbin/gitolite_remotes':
      source => 'puppet:///modules/tails/gitolite/install_remotes.sh',
      mode   => '0744',
      owner  => gitolite,
      group  => gitolite;
  }

  cron { 'install_gitolite_hooks':
    command => '/usr/local/sbin/gitolite_hooks',
    user    => gitolite,
    hour    => '*',
    require => File['/usr/local/sbin/gitolite_hooks'],
  }

  cron { 'install_gitolite_remotes':
    command => '/usr/local/sbin/gitolite_remotes',
    user    => gitolite,
    hour    => '*',
    require => File['/usr/local/sbin/gitolite_remotes'],
  }

}
