class tails::gitolite::hooks::jenkins_jobs {

  # XXX: remove once deployed
  file {
    [
      '/var/lib/gitolite/.gitolite/hooks/jenkins_jobs',
      '/var/lib/gitolite/.gitolite/hooks/jenkins_jobs/post-update.hook',
      '/var/lib/gitolite/.gitolite/hooks/jenkins_jobs/pre-receive.hook',
    ]:
      ensure => absent,
      force  => true,
  }

  # run by post-update -> post_update.d.hook -> post-update.d/*
  file { '/var/lib/gitolite/repositories/jenkins-jobs.git/hooks/post-update.d/deploy.hook':
    content => template(
      'tails/gitolite/hooks/jenkins_jobs-post-update.hook.erb'
    ),
    mode    => '0700',
    owner   => gitolite,
    group   => gitolite,
  }

  file { '/var/lib/gitolite/repositories/jenkins-jobs.git/hooks/pre-receive':
      content => template(
        'tails/gitolite/hooks/jenkins_jobs-pre-receive.hook.erb'
      ),
      mode    => '0700',
      owner   => gitolite,
      group   => gitolite;
  }

}
