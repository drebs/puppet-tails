# Manage hooks on tails.git
class tails::gitolite::hooks::tails {

  file { '/var/lib/gitolite/repositories/tails.git/hooks/':
      ensure => directory,
      mode   => '0755',
      owner  => gitolite,
      group  => gitolite;
  }

  file { '/var/lib/gitolite/repositories/tails.git/hooks/post-receive':
    content => template('tails/gitolite/hooks/tails-post-receive.erb'),
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package[curl],
  }

  ensure_packages(['curl'])

}
