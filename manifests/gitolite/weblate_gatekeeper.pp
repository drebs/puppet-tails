# The Weblate Gatekeeper repository is used as an intermediary between Weblate
# and GitLab to make sure Weblate only pushes what it's allowed to and mitigate
# privilege escalation in the Weblate VM from compromising Tails code in the
# main repository.
class tails::gitolite::weblate_gatekeeper() inherits tails::website::params {

  $hooks_dir = '/var/lib/gitolite3/repositories/weblate-gatekeeper.git/hooks'

  # The repository itself is created by Gitolite and managed in the
  # puppet-git.lizard:gitolite-admin repo, so we only manage hooks here.
  file { $hooks_dir:
    ensure => directory,
    owner  => 'gitolite3',
    group  => 'gitolite3',
    mode   => '0700',
  }

  ensure_packages([ 'python3-git' ])

  file { "${hooks_dir}/update.secondary":  # Gitolite chains 'update' to 'update.secondary'
    source  => 'puppet:///modules/tails/gitolite/hooks/tails-weblate-update.hook',
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0700',
    require => [
      Package['python3-git'],
      File[$hooks_dir],
    ],
  }

  file { "${hooks_dir}/langs.json":
    content => template('tails/weblate/langs.json.erb'),
    mode    => '0600',
    owner   => 'gitolite3',
    group   => 'gitolite3',
  }

  file { "${hooks_dir}/post-update":
    content => '#!/bin/sh
set -eu
/usr/bin/git push --quiet gitlab master',
    owner   => 'gitolite3',
    group   => 'gitolite3',
    mode    => '0700',
    require => [
      Package['python3-git'],
      File[$hooks_dir],
    ],
  }

}
