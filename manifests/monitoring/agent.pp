# Manages a Tails Icinga2 agent. It collect its check result and send them to a
# zone endpoint, which is a Tails satellite.

class tails::monitoring::agent (
  Enum['present', 'absent'] $ensure = 'present',
  String $parent_zone               = 'Lizard',
  Stdlib::Ipv4 $ip                  = $::ipaddress,
  Stdlib::Fqdn $nodename            = $::fqdn,
  String $mem_warning               = '20',
  String $mem_critical              = '10',
  String $system_disk_warning       = '20%',
  String $system_disk_critical      = '10%',
){

  class { '::tails::monitoring::config':
    ensure               => $ensure,
    ip                   => $ip,
    zone                 => $nodename,
    parent_zone          => $parent_zone,
    nodename             => $nodename,
    mem_warning          => $mem_warning,
    mem_critical         => $mem_critical,
    system_disk_warning  => $system_disk_warning,
    system_disk_critical => $system_disk_critical,
  }

  @@::tails::monitoring::config::host { $nodename :
    ensure => $ensure,
    zone   => $nodename,
    ip     => $ip,
    tag    => $parent_zone,
  }

  @@::tails::monitoring::config::zone { $nodename :
    ensure   => $ensure,
    endpoint => $nodename,
    parent   => $parent_zone,
    order    => '200',
    tag      => $nodename,
  }

  @@::tails::monitoring::config::endpoint { $nodename :
    ensure => $ensure,
    zone   => $nodename,
    ip     => $ip,
    order  => '200',
  }

  if $ensure == present {

    Tails::Monitoring::Config::Zone <<| |>>

    Tails::Monitoring::Config::Endpoint <<| |>>

    Tails::Monitoring::Config::Host <<| title == $nodename |>>

    Tails::Monitoring::Service <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Disk <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Memory <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Postfix_mailqueue <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Puppetmaster <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Reboot_required <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Upgradeable <<| nodename == $nodename |>>

    Tails::Monitoring::Service::Systemd <<| nodename == $nodename |>>

  }

}
