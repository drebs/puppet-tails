# Manage a Tails icinga2 host, connected to a zone, with its certificate.
define tails::monitoring::config::host (
  Stdlib::Ipv4 $ip,
  String $zone,
  Enum['present', 'absent'] $ensure = present,
){

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  # Manage the node zone and configuration.
  file { "/etc/icinga2/zones.d/${zone}/host.conf":
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      content => template('tails/monitoring/host.conf.erb'),
      require => Tails::Monitoring::Config::Zone[$zone];
  }

}
