# Manage a endpoint in a Tails icinga2 zone.

define tails::monitoring::config::endpoint (
  Stdlib::Ipv4 $ip,
  String $zone,
  Variant[Integer, String] $order,
  Enum['present', 'absent'] $ensure = present,
){

  concat::fragment { "endpoint_${zone}_${name}":
    target  => '/etc/icinga2/zones.conf',
    order   => $order,
    content => template('tails/monitoring/endpoint.conf.erb'),
    require => Package['icinga2-common'],
    notify  => Service['icinga2'],
  }

}
