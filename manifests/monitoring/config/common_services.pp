# Configure services that will be checked on all Tails monitored systems.

class tails::monitoring::config::common_services (
  String $zone,
  String $nodename,
  Enum['present', 'absent'] $ensure = present,
  String $mem_warning               = '20',
  String $mem_critical              = '10',
  String $system_disk_warning       = '20%',
  String $system_disk_critical      = '10%',
  Optional[String] $parent_zone     = undef,
){

  assert_private()

  @@::tails::monitoring::service { "apt@${nodename}":
    ensure             => $ensure,
    nodename           => $nodename,
    zone               => $zone,
    check_command      => 'apt',
    check_interval     => '1h',
    retry_interval     => '10m',
    max_check_attempts => 24,
    tag                => $parent_zone,
  }

  @@::tails::monitoring::service::disk { "system_disks@${nodename}":
    ensure     => $ensure,
    partitions => '["/", "/boot"]',
    nodename   => $nodename,
    zone       => $zone,
    wfree      => $system_disk_warning,
    cfree      => $system_disk_critical,
    tag        => $parent_zone,
  }

  @@::tails::monitoring::service::memory { "memory@${nodename}":
    ensure       => $ensure,
    nodename     => $nodename,
    zone         => $zone,
    mem_warning  => $mem_warning,
    mem_critical => $mem_critical,
    tag          => $parent_zone,
  }

  @@::tails::monitoring::service::systemd { "systemd@${nodename}":
    ensure   => $ensure,
    nodename => $nodename,
    zone     => $zone,
    tag      => $parent_zone,
  }

  @@::tails::monitoring::service::postfix_mailqueue { "mailqueue@${nodename}":
    ensure   => $ensure,
    nodename => $nodename,
    zone     => $zone,
    tag      => $parent_zone,
  }

  @@::tails::monitoring::service::reboot_required { "reboot_required@${nodename}":
    ensure   => $ensure,
    nodename => $nodename,
    zone     => $zone,
    tag      => $parent_zone,
  }

  @@::tails::monitoring::service::upgradeable { "upgradeable@${nodename}":
    ensure   => $ensure,
    nodename => $nodename,
    zone     => $zone,
    tag      => $parent_zone,
  }

}
