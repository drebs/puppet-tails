# Manages the Icinga2 api for nodes to interact with others.
class tails::monitoring::config::api (
  Stdlib::Ipv4 $ip,
  Enum['present', 'absent'] $ensure = 'present',
){

  assert_private()

  file { '/etc/icinga2/features-enabled/api.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/api.conf.erb'),
    require => Package['icinga2-common'],
    notify  => Service['icinga2'],
  }

}
