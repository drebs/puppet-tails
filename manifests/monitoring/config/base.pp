# This class manages the base of a Tails icinga2 instance.
# Shall be installed on every hosts managed by puppet.

class tails::monitoring::config::base (
  String $nodename,
  Enum['present', 'absent'] $ensure = 'present',
  Array[String] $nagios_user_groups = [],
){

  assert_private()

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  case $::lsbdistcodename {
    /^(stretch|sid)$/: {
      $pin = "release o=Debian,n=${::lsbdistcodename}"
    }
    default: {
      fail("Unsupported Debian release ${::lsbdistcodename}")
    }
  }

  apt::preferences_snippet { ['icinga2', 'icinga2-bin', 'icinga2-common', 'libicinga2']:
    ensure   => $ensure,
    pin      => $pin,
    priority => 991,
  }

  package { ['icinga2', 'icinga2-bin', 'icinga2-common']:
    ensure  => $ensure,
    require => [
      Apt::Preferences_snippet[
        'icinga2',
        'icinga2-bin',
        'icinga2-common',
        'libicinga2'
      ]
    ]
  }

  package { 'monitoring-plugins':
    ensure => $ensure,
  }

  # monitoring-plugins installs samba, which we don't want.
  package { ['samba', 'samba-common', 'samba-libs']:
    ensure  => 'purged',
  }

  $service_ensure = $ensure ? {
    absent  => stopped,
    default => running,
  }

  $service_enable = $ensure ? {
    absent  => false,
    default => true,
  }

  service { 'icinga2':
    ensure     => $service_ensure,
    enable     => $service_enable,
    hasrestart => true,
    require    => Package['icinga2-common'],
  }

  file { '/etc/icinga2':
    # needs to be present even when $ensure == absent,
    # for Concat['/etc/icinga2/zones.conf'] to work
    # (concat has no ensure parameter)
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  # needed for Concat['/etc/icinga2/zones.conf']
  user { 'nagios':
    ensure  => present,
    system  => true,
    gid     => 'nagios',
    groups  => $nagios_user_groups,
    require => Package['icinga2-common'],
  }

  group { 'nagios':
    ensure => present,
    system => true,
  }

  file { '/etc/icinga2/conf.d/services.conf':
    ensure  => $ensure,
    content => '',
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    require => Package['icinga2-common'],
  }

  file { '/etc/icinga2/conf.d/apt.conf':
    ensure  => $ensure,
    content => '',
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    require => Package['icinga2-common'],
  }

  file { '/etc/icinga2/conf.d/hosts.conf':
    ensure  => $ensure,
    content => '',
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    require => Package['icinga2-common'],
  }

  concat { '/etc/icinga2/zones.conf':
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    require => Package['icinga2-common'],
    notify  => Service['icinga2'],
  }

  file { '/etc/icinga2/constants.conf':
    ensure  => $ensure,
    content => template('tails/monitoring/constants.conf.erb'),
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    notify  => Service['icinga2'],
    require => Package['icinga2-common'],
  }

  $pki_force = $ensure ? {
    absent  => true,
    default => false,
  }
  file { '/etc/icinga2/pki':
    ensure => $directory_ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0700',
    require => Package['icinga2-common'],
    force   => $pki_force,
  }

  file { '/etc/icinga2/pki/ca.crt':
    ensure => $ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0600',
    source => 'puppet:///modules/site_tails_monitoring/public_keys/ca.crt',
    require => File['/etc/icinga2/pki'],
  }

}
