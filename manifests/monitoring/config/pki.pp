# Manages a node PKI configuration: add its own private and public keys.
define tails::monitoring::config::pki (
  Enum['present', 'absent'] $ensure = present,
){

  assert_private()

  $key_source = $ensure ? {
    absent  => undef,
    default => "puppet:///modules/tails_secrets_monitoring/private_keys/${name}.key",
  }
  file { "/etc/icinga2/pki/${name}.key":
      ensure => $ensure,
      owner  => 'nagios',
      group  => 'nagios',
      mode   => '0600',
      source => $key_source,
      notify => Service['icinga2'],
  }

  $cert_source = $ensure ? {
    absent  => undef,
    default => "puppet:///modules/site_tails_monitoring/public_keys/${name}.crt",
  }
  file { "/etc/icinga2/pki/${name}.crt":
      ensure => $ensure,
      source => $cert_source,
      owner  => 'nagios',
      group  => 'nagios',
      mode   => '0600',
      notify => Service['icinga2'],
  }

}
