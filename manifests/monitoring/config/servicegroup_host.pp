# Creates  the service group for all checks concerning a given host.
define tails::monitoring::config::servicegroup_host (
  Enum['present', 'absent'] $ensure = 'present',
){

  assert_private()

  file { "/etc/icinga2/conf.d/servicegroup_${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/servicegroup_host.conf.erb'),
  }

}
