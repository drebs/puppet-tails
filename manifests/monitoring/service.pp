# Wrap ::icinga2 service definitions so that they're configured with what
# fits best for a Tails monitoring setup.
#
# When a service is declared, its $name must be in the form of
# $service_name@$nodename to ensure resource uniqness. $service_name
# will be used to set the default display_name of the service if
# unspecified. $nodename parameter must *also* be set, so that the
# service gets collected on the node.

define tails::monitoring::service (
  String $zone,
  String $nodename,
  Optional[String] $display_name        = undef,
  Optional[String] $check_command       = undef,
  Optional[Integer] $max_check_attempts = undef,
  Optional[String] $check_period        = undef,
  Optional[String] $check_interval      = undef,
  Optional[String] $retry_interval      = undef,
  Boolean $enable_notifications         = true,
  Optional[String] $enable_flapping     = undef,
  Enum['present', 'absent'] $ensure     = 'present',
){

  validate_re($name, '^[\w-]+@[\w.-]+$')

  $sp_name = split($name, '@')
  $service_name = $sp_name[0]

  if $display_name == undef {
    $displayed_name = $service_name
  } else {
    $displayed_name = $display_name
  }

  ::icinga2::object::service { $name:
    host_name               => $nodename,
    display_name            => $displayed_name,
    check_command           => $check_command,
    max_check_attempts      => $max_check_attempts,
    check_period            => $check_period,
    check_interval          => $check_interval,
    retry_interval          => $retry_interval,
    enable_notifications    => $enable_notifications,
    enable_flapping         => $enable_flapping,
    target_dir              => "/etc/icinga2/zones.d/${zone}",
    target_file_name        => "${service_name}.conf",
    target_file_ensure      => $ensure,
    refresh_icinga2_service => false,
    require                 => Tails::Monitoring::Config::Zone[$zone],
    notify                  => Service['icinga2'],
  }

}
