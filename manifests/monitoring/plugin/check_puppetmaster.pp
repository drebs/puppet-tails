class tails::monitoring::plugin::check_puppetmaster (
  Enum['present', 'absent'] $ensure                   = 'present',
  Stdlib::Absolutepath $check_puppetdb_nodes_checkout = '/var/lib/nagios/check_puppetdb_nodes',
  String $check_puppetdb_nodes_remote                 = 'https://git-tails.immerda.ch/check_puppetdb_nodes',
){

  $required_pkgs = [
    'libtimedate-perl',
    'libjson-perl',
    'libmonitoring-plugin-perl',
  ]

  ensure_packages($required_pkgs)

  vcsrepo { $check_puppetdb_nodes_checkout:
    ensure   => 'latest',
    provider => 'git',
    source   => $check_puppetdb_nodes_remote,
    user     => 'nagios',
  }

  file { '/usr/lib/nagios/plugins/check_puppetmaster':
    ensure  => 'link',
    target  => "${check_puppetdb_nodes_checkout}/check_puppetdb_nodes",
    owner   => root,
    group   => root,
    mode    => '0755',
    notify  => Service['icinga2'],
    require => [
      Package[$required_pkgs],
      Vcsrepo[$check_puppetdb_nodes_checkout],
    ],
  }

}
