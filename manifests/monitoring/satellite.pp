# Manages a Tails icinga2 satellite, endpoint of a zone who's parent is
# the Tails icinga2 master zone.
class tails::monitoring::satellite (
  Enum['present', 'absent'] $ensure = 'present',
  String $zone                      = 'Lizard',
  String $parent_zone               = 'Tails',
  Stdlib::Ipv4 $ip                  = $::ipaddress,
  String $nodename                  = $::fqdn,
){

  class { '::tails::monitoring::config':
    ensure   => $ensure,
    ip       => $ip,
    zone     => $zone,
    nodename => $nodename,
  }

  @@::tails::monitoring::config::host { $nodename :
    ensure => $ensure,
    zone   => $zone,
    ip     => $ip,
  }

  @@::tails::monitoring::config::zone { $zone :
    ensure   => $ensure,
    parent   => $parent_zone,
    endpoint => $nodename,
    order    => '100',
    tag      => $zone,
  }

  @@::tails::monitoring::config::endpoint { $nodename :
    ensure => $ensure,
    zone   => $zone,
    ip     => $ip,
    order  => '100',
  }

  Tails::Monitoring::Config::Zone <<| |>>

  Tails::Monitoring::Config::Endpoint <<| |>>

  # XXX: we don't use active_record for storedconfigs anymore, so
  # presumably we can now use or/and in exported resources collectors.
  Tails::Monitoring::Config::Host <<| zone == $zone |>>

  Tails::Monitoring::Service <<| zone == $zone |>>

  Tails::Monitoring::Service::Disk <<| zone == $zone |>>

  Tails::Monitoring::Service::Memory <<| zone == $zone |>>

  Tails::Monitoring::Service::Number_in_file <<| zone == $zone |>>

  Tails::Monitoring::Service::Postfix_mailqueue <<| zone == $zone |>>

  Tails::Monitoring::Service::Puppetmaster <<| zone == $zone |>>

  Tails::Monitoring::Service::Reboot_required <<| zone == $zone |>>

  Tails::Monitoring::Service::Systemd <<| zone == $zone |>>

  Tails::Monitoring::Service::Upgradeable <<| zone == $zone |>>

  Tails::Monitoring::Config::Host <<| tag == $zone |>>

  Tails::Monitoring::Service <<| tag == $zone |>>

  Tails::Monitoring::Service::Disk <<| tag == $zone |>>

  Tails::Monitoring::Service::Memory <<| tag == $zone |>>

  Tails::Monitoring::Service::Number_in_file <<| tag == $zone |>>

  Tails::Monitoring::Service::Postfix_mailqueue <<| tag == $zone |>>

  Tails::Monitoring::Service::Puppetmaster <<| tag == $zone |>>

  Tails::Monitoring::Service::Reboot_required <<| tag == $zone |>>

  Tails::Monitoring::Service::Systemd <<| tag == $zone |>>

  Tails::Monitoring::Service::Upgradeable <<| tag == $zone |>>

}
