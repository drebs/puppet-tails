# Manages the monitoring of the Tails SSH or SFTP account for iso
# testing.
# Use $name to define the username login to test.
# Use $keyname to pass the name of the SSH key generated with
# sshkeys::create_key that has been installed in the remote user
# authorized_keys.
# $real_host must be the host where the service is running.

define tails::monitoring::service::ssh_or_sftp_account (
  Stdlib::Fqdn $server_hostname,
  String $real_host,
  String $keyname,
  Enum['present', 'absent'] $ensure = 'present',
  String $nodename                  = 'ecours.tails.boum.org',
  Stdlib::Port $port                = 22,
  String $check_type                = 'ssh',
  Boolean $enable_notifications     = true,
  String $check_interval            = '10m',
  String $retry_interval            = '2m',
  Optional[String] $display_name    = undef,
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  sshkeys::set_client_key_pair { "monitoring@${name}":
    keyname  => $keyname,
    user     => 'nagios',
    home     => '/var/lib/nagios',
    filename => "id_rsa_${name}",
    require  => File['/var/lib/nagios/.ssh'],
  }

  file { "/etc/icinga2/conf.d/${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/ssh_or_sftp_account.erb'),
    require => Class['::tails::monitoring::checkcommand::ssh_or_sftp_account'],
    notify  => Service['icinga2'],
  }

}
