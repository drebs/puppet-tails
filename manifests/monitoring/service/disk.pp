# Manages the monitoring of one or several partitions. This should be used when
# one setup a custom partition for a specific service, and is used for the
# default monitoring of systems disks.
#
# Requires to specify $zone and $nodename for exported resources
# collection and either one $partition as a string, or $partitions as
# a string in the form of '["/srv/partition1", "/srv/partitions2"]'.
# Such define must be named in the form of "$service_name@$nodename" for
# resource uniqness and integration in Icinga2 configuration directory
# layout.
# By default will issue warnings if less than 20% of disk space is free,
# will issue critical alerts if disk space is less than 10% free. Same
# for inodes.
# e.g:
#
#    @@::tails::monitoring::service::disk { "apt-cacher-ng-disk@$::fqdn":
#      partition => '/var/cache/apt-cacher-ng',
#    }

define tails::monitoring::service::disk (
  String $nodename,
  String $zone,
  Enum['present', 'absent'] $ensure        = 'present',
  Optional[String] $partitions             = undef,
  Optional[String] $partition              = undef,
  Pattern[/\A\d{1,3}%|\d+\z/] $wfree       = '20%',
  Pattern[/\A\d{1,3}%|\d+\z/] $cfree       = '10%',
  Pattern[/\A\d{1,3}%|\d+\z/] $inode_wfree = '20%',
  Pattern[/\A\d{1,3}%|\d+\z/] $inode_cfree = '10%',
  String $check_interval                   = '1h',
  String $retry_interval                   = '5m',
  Optional[String] $display_name           = undef,
  Boolean $enable_notifications            = true,
){

  if ($partition == undef) and ($partitions == undef) {
    fail('One of the $partition or $partitions parameter must be set.')
  }

  validate_re($name, '^[\w-]+@[\w.-]+$')

  $sp_name      = split($name, '@')
  $service_name = $sp_name[0]

  validate_bool($enable_notifications)

  if $display_name == undef {
    $displayed_name = $service_name
  } else {
    $displayed_name = $display_name
  }

  file { "/etc/icinga2/zones.d/${zone}/${service_name}_disk.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/disk.erb'),
    notify  => Service['icinga2'],
  }

}
