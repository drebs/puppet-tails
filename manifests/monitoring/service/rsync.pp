# Manages the monitoring of a rsync server. Requires to specify a rsync
# "module" and which host is running this service with $real_host.

define tails::monitoring::service::rsync (
  String $module,
  String $real_host,
  Enum['present', 'absent'] $ensure = 'present',
  String $nodename                  = 'ecours.tails.boum.org',
  Stdlib::Port $port                = 873,
  Boolean $enable_notifications     = true,
  String $check_interval            = '10m',
  String $retry_interval            = '2m',
  Optional[String] $display_name    = undef,
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::rsync

  file { "/etc/icinga2/conf.d/${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/rsync.erb'),
    require => Class['::tails::monitoring::checkcommand::rsync'],
    notify  => Service['icinga2'],
  }

}
