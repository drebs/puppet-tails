# Manages the monitoring of a website.
#
# The name of this resource can be the vhost expected to be monitored,
# or you can pass the vhost as a parameter if you wish to monitor several
# uri on it and ensure resource name uniqueness.
# e.g:
#
#    @@::tails::monitoring::service::http { "tails.boum.org":
#      ssl => true,
#    }
# $real_host must be set to the hostname where the service is actualy
# running, to be added to its service group.

define tails::monitoring::service::http (
  String $real_host,
  Enum['present', 'absent'] $ensure   = present,
  Optional[Stdlib::Fqdn] $vhost       = undef,
  Optional[Stdlib::Ipv4] $ip          = undef,
  String $nodename                    = 'ecours.tails.boum.org',
  Optional[Stdlib::Port] $custom_port = undef,
  Boolean $ssl                        = false,
  String $uri                         = '/',
  Optional[String] $expected_header   = undef,
  String $check_interval              = '5m',
  String $retry_interval              = '100s',
  Optional[String] $display_name      = undef,
  Boolean $enable_notifications       = true,
){

  if $vhost == undef {
    validate_re($name, '^[\w.-]+$')
    $vhost_name = $name
  } else {
    $vhost_name = $vhost
  }

  if $display_name == undef {
    if $ssl {
      $displayed_name = "https://${vhost_name}"
    } else {
      $displayed_name = "http://${vhost_name}"
    }
  } else {
    $displayed_name = $display_name
  }

  file { "/etc/icinga2/conf.d/${name}.conf":
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/http.erb'),
    notify  => Service['icinga2'],
  }

}
