# Manages the monitoring of Postfix mail queue

define tails::monitoring::service::postfix_mailqueue (
  String $nodename,
  String $zone,
  Enum['present', 'absent'] $ensure = present,
  String $check_interval            = '1h',
  String $retry_interval            = '5m',
  Optional[String] $display_name    = undef,
  Boolean $enable_notifications     = true,
  Stdlib::Absolutepath $config_dir  = '/etc/postfix',
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::postfix_mailqueue

  file {
    "/etc/icinga2/zones.d/${zone}/postfix_mailqueue_${name}.conf":
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      content => template('tails/monitoring/service/postfix_mailqueue.erb'),
      notify  => Service['icinga2'],
  }

}
