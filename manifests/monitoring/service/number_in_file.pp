# Manages the monitoring of a number stored in a file

define tails::monitoring::service::number_in_file (
  String $nodename,
  String $zone,
  Stdlib::Absolutepath $file,
  Integer[1] $warning,
  Integer[1] $critical,
  Enum['present', 'absent'] $ensure = present,
  String $check_interval            = '12h',
  String $retry_interval            = '5m',
  Optional[String] $display_name    = undef,
  Boolean $enable_notifications     = true,
){

  validate_re($name, '^[\w-]+$')

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::number_in_file

  file {
    "/etc/icinga2/zones.d/${zone}/number_in_file_${name}.conf":
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      content => template('tails/monitoring/service/number_in_file.erb'),
      notify  => Service['icinga2'],
  }

}
