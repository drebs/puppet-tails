# Manages the monitoring of upgradeable Debian packages

define tails::monitoring::service::upgradeable (
  String $nodename,
  String $zone,
  Enum['present', 'absent'] $ensure = present,
  String $check_interval            = '1h',
  String $retry_interval            = '5m',
  Optional[String] $display_name    = undef,
  Boolean $enable_notifications     = true,
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::upgradeable

  file {
    "/etc/icinga2/zones.d/${zone}/upgradeable_${name}.conf":
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      content => template('tails/monitoring/service/upgradeable.erb'),
      notify  => Service['icinga2'],
  }

}
