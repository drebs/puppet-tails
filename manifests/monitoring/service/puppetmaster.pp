# Manages the monitoring of puppetmaster:
# did agents manage to apply their catalog recently?

define tails::monitoring::service::puppetmaster (
  String $nodename,
  String $zone,
  Enum['present', 'absent'] $ensure = present,
  String $check_interval            = '1h',
  String $retry_interval            = '10m',
  Optional[String] $display_name    = undef,
  Boolean $enable_notifications     = true,
  Pattern[/\A\d+\z/] $api_version   = '4',
  Array[String] $ignore_nodes       = [],
  Integer[1] $warning_delay         = 1440, # 24 hours in minutes
  Integer[1] $critical_delay        = 2880, # 48 hours in minutes
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::puppetmaster

  file {
    "/etc/icinga2/zones.d/${zone}/puppetmaster_${name}.conf":
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      content => template('tails/monitoring/service/puppetmaster.erb'),
      notify  => Service['icinga2'],
  }

}
