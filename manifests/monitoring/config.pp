# Manages various common configurations once a node is configured as part of
# the Tails monitoring setup.
class tails::monitoring::config (
  Stdlib::Ipv4 $ip,
  String $zone,
  String $nodename,
  Enum['present', 'absent'] $ensure  = 'present',
  String $mem_warning                = '20',
  String $mem_critical               = '10',
  String $system_disk_warning        = '20%',
  String $system_disk_critical       = '10%',
  Optional[String] $parent_zone      = undef,
  Optional[String] $email_recipient  = undef,
  String $notification_interval      = '1d',
  Boolean $disable_node_notification = true,
){

  class { '::tails::monitoring::config::base':
    ensure   => $ensure,
    nodename => $nodename,
  }

  class { '::tails::monitoring::config::api':
    ensure => $ensure,
    ip     => $ip,
  }

  ::tails::monitoring::config::pki { $nodename:
    ensure => $ensure,
  }

  class { '::tails::monitoring::config::common_services':
    ensure               => $ensure,
    zone                 => $zone,
    parent_zone          => $parent_zone,
    nodename             => $nodename,
    mem_warning          => $mem_warning,
    mem_critical         => $mem_critical,
    system_disk_warning  => $system_disk_warning,
    system_disk_critical => $system_disk_critical,
  }

  @@::tails::monitoring::config::servicegroup_host { $nodename:
    ensure => $ensure,
  }

  if $disable_node_notification == true {
    file { '/etc/icinga2/features-enabled/notification.conf':
      ensure => absent,
    }
  } else {
    file { '/etc/icinga2/conf.d/users.conf':
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0644',
      content => template('tails/monitoring/users.conf.erb'),
      notify  => Service['icinga2'],
    }
    file { '/etc/icinga2/conf.d/notifications.conf':
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0644',
      content => template('tails/monitoring/notifications.conf.erb'),
      notify  => Service['icinga2'],
    }
  }

  if $email_recipient != undef {
    postfix::mailalias { 'nagios':
      recipient => $email_recipient,
    }
  }

  sudo::conf { 'nagios-postfix':
    ensure  => $ensure,
    content => "nagios ALL = (postfix) NOPASSWD: /usr/lib/nagios/plugins/check_postfix_mailqueue\n",
    require => Package['icinga2'],
  }

  sudo::conf { 'nagios-systemd':
    ensure  => $ensure,
    content => "nagios ALL = (root) NOPASSWD: /usr/lib/nagios/plugins/pynagsystemd\n",
    require => Package['icinga2'],
  }

}
