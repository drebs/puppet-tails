# Manages the webserver hosting the Tails icingaweb2 instance.
#

class tails::monitoring::icingaweb2::nginx (
  Stdlib::Fqdn $webserver_hostname,
  Enum['present', 'absent'] $ensure = present,
){

  assert_private()

  ### Sanity checks

  if $::lsbdistcodename !~ /^stretch$/ {
    fail('This module only supports Debian Stretch.')
  }

  ### Resources

  class { '::nginx':
    access_log => 'noip',
    error_log  => 'none',
    require    => Tails::Dhparam['/etc/nginx/dhparams.pem'],
  }
  tails::dhparam { '/etc/nginx/dhparams.pem': }

  case $::lsbdistcodename {
    'stretch': {
      $php_fpm_ini     = '/etc/php/7.0/fpm/php.ini'
      $php_fpm_package = 'php7.0-fpm'
      $php_fpm_service = 'php7.0-fpm'
      $php_fpm_socket  = '/run/php/php7.0-fpm.sock'
      package { 'php5-fpm': ensure => absent }
    }
  }

  nginx::vhostsd { $webserver_hostname:
    ensure  => $ensure,
    content => template('tails/monitoring/icingaweb2/nginx/site.erb'),
    require => [
      Package[$php_fpm_package],
      Tails::Letsencrypt::Certonly[$webserver_hostname],
    ],
    notify  => Service['nginx'],
  }

  include ::tails::letsencrypt
  tails::letsencrypt::certonly { $webserver_hostname: }

  package { $php_fpm_package:
    ensure  => $ensure,
  }

  service { $php_fpm_service:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    require    => Package[$php_fpm_package],
  }

  file_line { 'timezone_date':
    ensure  => $ensure,
    path    => $php_fpm_ini,
    line    => 'date.timezone = UTC',
    match   => 'date.timezone\s*=.*',
    require => Package[$php_fpm_package],
    notify  => Service[$php_fpm_service],
  }

  file_line { 'session.gc_maxlifetime':
    ensure  => $ensure,
    path    => $php_fpm_ini,
    # 3 days = 72*3600 seconds
    line    => 'session.gc_maxlifetime = 259200',
    match   => 'session.gc_maxlifetime\s*=\s*\d+',
    require => Package[$php_fpm_package],
    notify  => Service[$php_fpm_service],
  }

  # Apache may be automatically pulled while upgrading to Stretch
  package { [
    'apache2',
    'apache2-bin',
    'apache2-data',
    'apache2-utils',
    'libapache2-mod-php7.0'
  ]:
    ensure => purged,
  }
}
