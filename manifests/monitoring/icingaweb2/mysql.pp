# Setup the MySQL database used by icingaweb2
# Icingaweb2 package doesn't use dbconfig yet...
class tails::monitoring::icingaweb2::mysql (
  String $web_db_name,
  String $web_db_user,
  String $web_db_pass,
  String $web_user_pass,
  Enum['present', 'absent'] $ensure = present,
){

  assert_private()

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  @@mysql_database { $web_db_name:
    ensure => $ensure,
    tag    => "mysql_${::fqdn}",
  }

  @@mysql_user { "${web_db_user}@localhost":
    password_hash => mysql_password($web_db_pass),
    require       => Mysql_database[$web_db_name],
    tag           => "mysql_${::fqdn}",
  }

  @@mysql_grant { "${web_db_user}@localhost/${web_db_name}":
    privileges => all,
    require    => Mysql_user["${web_db_user}@localhost"],
    tag        => "mysql_${::fqdn}",
  }

  file { '/usr/local/sbin/install_icingaweb2_database':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icingaweb2/scripts/install_icingaweb2_database',
  }

  file { '/var/lib/icingaweb2':
    ensure => $directory_ensure,
    owner  => 'icingaweb2',
    group  => 'icingaweb2',
    mode   => '0755',
  }

  exec { 'install_web_database':
    user    => 'icingaweb2',
    group   => 'icingaweb2',
    command => "/usr/local/sbin/install_icingaweb2_database '${web_db_name}' '${web_db_user}' '${web_db_pass}' '${web_user_pass}'",
    creates => '/var/lib/icingaweb2/.db_installed',
    require => [
      Mysql_grant["${web_db_user}@localhost/${web_db_name}"],
      File['/var/lib/icingaweb2'],
    ],
  }

}
