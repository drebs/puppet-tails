# Manage the Tails monitoring icinga2 master instance. This one is the
# CA owner, is collecting every other hosts results and is responsible
# for the notifications to sysadmins.
#
# It installs a Icingaweb2 instance, and configure its database with the
# $web_* parameters. The web interface requires to enable the ido-mysql
# Icinga2 feature, the $ido_* parameters are used to configure this IDO
# database.

class tails::monitoring::master (
  String $ido_db_pass,
  String $web_db_pass,
  Pattern[/\A\w+\z/] $web_user_pass,
  String $db_admin_pass,
  String $email_recipient           = 'root',
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Ipv4 $ip                  = $::ipaddress,
  String $zone                      = 'Tails',
  String $nodename                  = $::fqdn,
  String $ido_db                    = 'mysql',
  String $ido_db_name               = 'icinga2',
  String $ido_db_user               = 'icinga2',
  String $web_db                    = 'mysql',
  String $web_db_name               = 'icingaweb2',
  String $web_db_user               = 'icingaweb2',
  Stdlib::Fqdn $webserver_hostname  = 'icingaweb2.tails.boum.org',
  String $web_admins                = 'icingaadmin',
){

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  class { '::tails::monitoring::config':
    ensure                    => $ensure,
    ip                        => $ip,
    zone                      => $zone,
    nodename                  => $nodename,
    disable_node_notification => false,
    email_recipient           => $email_recipient,
  }

  @@::tails::monitoring::config::host { $nodename :
    ensure => $ensure,
    zone   => $zone,
    ip     => $ip,
  }

  file {
    '/etc/icinga2/pki/ca.key':
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      source  => 'puppet:///modules/tails_secrets_monitoring/ca/ca.key',
      require => Package['icinga2-common'];
    '/var/lib/icinga2/ca/ca.crt':
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      source  => 'puppet:///modules/site_tails_monitoring/public_keys/ca.crt',
      require => Package['icinga2-common'];
    '/var/lib/icinga2/ca/ca.key':
      ensure  => $ensure,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0600',
      source  => 'puppet:///modules/tails_secrets_monitoring/ca/ca.key',
      require => Package['icinga2-common'],
  }

  file { '/etc/icinga2/features-enabled/perfdata.conf':
    ensure => 'absent',
    notify => Service['icinga2'],
  }

  file { '/etc/icinga2/conf.d/functions.conf':
    ensure => $ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0600',
    source => 'puppet:///modules/tails/monitoring/icinga2/functions.conf',
  }

  file { '/etc/icinga2/conf.d/servicegroups.conf':
    ensure => $ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0600',
    source => 'puppet:///modules/tails/monitoring/icinga2/servicegroups.conf',
  }

  include ::mysql::server

  class { '::tails::monitoring::config::ido':
    ensure        => $ensure,
    ido_db_name   => $ido_db_name,
    ido_db_user   => $ido_db_user,
    ido_db_pass   => $ido_db_pass,
    db_admin_pass => $db_admin_pass,
  }

  class { '::tails::monitoring::icingaweb2':
    ensure             => $ensure,
    ido_db             => $ido_db,
    ido_db_name        => $ido_db_name,
    ido_db_user        => $ido_db_user,
    ido_db_pass        => $ido_db_pass,
    web_db             => $web_db,
    web_db_name        => $web_db_name,
    web_db_user        => $web_db_user,
    web_db_pass        => $web_db_pass,
    web_user_pass      => $web_user_pass,
    webserver_hostname => $webserver_hostname,
    web_admins         => $web_admins,
    require            => [
      Class['::mysql::server'],
      Class['::tails::monitoring::config'],
      Class['::tails::monitoring::config::ido'],
    ],
  }

  @@::tails::monitoring::config::zone { $zone :
    ensure   => $ensure,
    endpoint => $nodename,
    order    => '001',
    tag      => $zone,
  }

  @@::tails::monitoring::config::endpoint { $nodename :
    ensure => $ensure,
    zone   => $zone,
    ip     => $ip,
    order  => '001',
  }

  include ::tails::monitoring::checkcommand::torified_smtp

  include ::tails::monitoring::checkcommand::ssh_or_sftp_account

  file { '/var/lib/nagios':
    ensure  => $directory_ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0755',
    require => Package['icinga2-common'],
  }

  # python3-paramiko does not support ed25519 yet, so force the use of
  # RSA host key.
  file { '/var/lib/nagios/.ssh':
    ensure => $directory_ensure,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0700',
  }

  file { '/var/lib/nagios/.ssh/config':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => "Hostname lizard.tails.boum.org\n  HostKeyAlgorithms=ssh-rsa",
  }

  # Collect zones, the master needs to know all of them.
  Tails::Monitoring::Config::Zone <<| |>>

  Tails::Monitoring::Config::Endpoint <<| |>>

  # Also collect every host definitions.
  Tails::Monitoring::Config::Host <<| |>>

  Tails::Monitoring::Service <<| |>>

  Tails::Monitoring::Config::Servicegroup_host <<| |>>

  Tails::Monitoring::Service::Disk <<| |>>

  Tails::Monitoring::Service::Http <<| |>>

  Tails::Monitoring::Service::Memory <<| |>>

  Tails::Monitoring::Service::Systemd <<| |>>

  Tails::Monitoring::Service::Postfix_mailqueue <<| |>>

  Tails::Monitoring::Service::Puppetmaster <<| |>>

  Tails::Monitoring::Service::Reboot_required <<| |>>

  Tails::Monitoring::Service::Upgradeable <<| |>>

  Tails::Monitoring::Service::Whisperback <<| |>>

  Tails::Monitoring::Service::Number_in_file <<| |>>

  Tails::Monitoring::Service::Rsync <<| |>>

  Tails::Monitoring::Service::Torbrowser_archive <<| |>>

  Tails::Monitoring::Service::Ssh_or_sftp_account <<| |>>
}
