# Manage Weblate dependencies that are in not in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::python_dependencies () {

  $$pip_packages = [
    'python3-pip',
  ]

  ensure_packages($$pip_packages)

  class { 'tails::profile::rss2email':
    user         => 'weblate-rss2email',
    default_from => 'translate@lizard.tails.boum.org'
    default_to   => 'tails-weblate@boum.org',
  }

  # Dependencies for Weblate $WEBLATE_VERSION

  $PIP_DEPENDS
}
