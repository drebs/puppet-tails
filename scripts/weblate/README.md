Update Weblate Python dependencies manifests
============================================

While setting up Weblate, we needed to choose how to install Python
dependencies that were not available in Debian. Because Pypi doesn't provide a
way to easily verify the packages they distribute against upstream code, we
decided to install those dependencies directly from upstream repositories using
Pip. The downside of this decision is that we had to find a new way of
resolving dependencies.

The script in this directory extracts direct requirements of Weblate from its
repository, queries Pypi for package information, traverses Weblate's full
dependency tree, and generates Puppet code for declaring all dependencies.

Installation
------------

  1. Make sure you have the Debian repository of the target Debian version
     configured in your system. The script will query the APT cache to check
     which dependencies are available in Debian (if any).
  2. Install requirements. You may use run `make` to install the requirements
     needed for running the script. It'll use sudo to install some dependencies
     from Debian and virtualenv to install the rest of dependencies from Pip.
  3. Update the `override.yaml` file with the options you need.
  4. Run the script.

Override file
-------------

The script will read the `override.yaml` file from the same directory, which
should be a YAML file containing a dict with the following entries:

    'skip': A list of packages to be skipped.

    'require_pip': A list of packages that must be installed from pip if they
                   appear somewhere in the dependency tree.

    'extra_pip': A list of extra packages to be installed from pip regardless
                 of being dependencies of other packages.

    'require_debian': A list of packages that must be installed from Debian.

    'pip': A dictionary indexed by pip package names in which each item is
           itself a dict that may contain a 'url', a 'repo_type', and a list
           of 'additional_conditions' for the package version to be installed.

Usage
-----

Get help:

```
./update_puppet_dependency_files.py --help
```

Usage example:

  python3 ./update_puppet_dependecy_files.py \
    --weblate-version 3.5.1 \
    --debian-suite stretch \
    --weblate-repo ~/weblate/
