#!/bin/sh
#
# Test Weblate Scripts
# ====================
#
# 

set -ex

clone_upstream_repo() {
	git clone --depth 1 --branch weblate-3.11.3 https://github.com/WeblateOrg/weblate.git /usr/local/share/weblate
}

configure_weblate() {
	cp /usr/local/share/weblate/weblate/settings_example.py /usr/local/share/weblate/weblate/settings.py
	sed -i 's%\"NAME\": \"weblate\"%\"NAME\": \"/tmp/weblate.sqlite3\"%' /usr/local/share/weblate/weblate/settings.py
	sed -i 's%\"ENGINE\": \"django.db.backends.postgresql\"%\"ENGINE\": \"django.db.backends.sqlite3\"%' /usr/local/share/weblate/weblate/settings.py
	sed -i 's%\"LOCATION\": \"redis://127.0.0.1:6379/1\"%\"LOCATION\": \"redis://redis:6379/1\"%' /usr/local/share/weblate/weblate/settings.py
	sed -i 's%\"redis://localhost:6379\"%\"redis://redis:6379\"%' /usr/local/share/weblate/weblate/settings.py
}

install_dependencies() {
	apt-get -qy install python3-pip python3-nose gettext pkg-config libglib2.0-dev libgirepository1.0-dev libcairo2-dev libxml2-dev libxslt1-dev gir1.2-pango-1.0 libssl-dev libacl1-dev postgresql-common postgresql-server-dev-all libpq-dev python3-gi python3-gi-cairo gir1.2-gtk-3.0
	pip3 install setuptools-scm  # needs to be installed separatelly otherwise borgbackup tries to install a version incompatible with Python 3.5 (See: https://github.com/pypa/setuptools_scm/issues/541 )
	pip3 install -r /usr/local/share/weblate/requirements.txt kombu\<4.7 psycopg2 pyyaml
	pip3 install -e /usr/local/share/weblate/
}

deploy_test_project() {
	python3 /usr/local/share/weblate/manage.py migrate
	python3 /usr/local/share/weblate/manage.py shell -c "from weblate.trans.models.project import Project; Project(name='Tails', slug='tails', web='https://tails.boum.org').save()"
	python3 /usr/local/share/weblate/manage.py shell -c "from weblate.trans.models.project import Project; Project(name='Tails2', slug='tails2', web='https://tails.boum.org').save()"
	python3 /usr/local/share/weblate/manage.py import_project --language-regex "^(de|fr)$" tails https://gitlab.tails.boum.org/tails/tails.git master "wiki/src/**.*.po"
}

setup_weblate() {
	clone_upstream_repo
	configure_weblate
	install_dependencies
	deploy_test_project
}

run_tests() {
	cd files/weblate/scripts
	python3 -m nose --verbose
}

main() {
	setup_weblate
	run_tests
}

main
