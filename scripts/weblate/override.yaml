---
skip:
  - pillow   # Pillow is packaged as python3-pil in Debian
  - brotli   # extra depdendecies by kombu, we don't need
  - brotlipy # extra depdendecies by kombu, we don't need
  - configparser # it is part of PYthon 3.5
  - backports.lzma # only needed for python <3.3
  - importlib-resources # depdendency only needed for testing importlib-metadata
  - contextlib2 # only needed for python <3
require_pip:
  - django
  - django-appconf
  - django-crispy-forms
  - django-compressor
  - jellyfish
  - setuptools
  - translate-toolkit
extras_pip:
  - cheroot  # optional dependency of "translate-toolkit" for the tmserver backend
  - chardet     # Dependency for translation-finder 1.8 not tracked in Pypi
  - ruamel.yaml # Dependency for translation-finder 1.8 not tracked in Pypi
  - six         # Dependency for translation-finder 1.8 not tracked in Pypi
require_debian:
  - python3-levenshtein # optional Weblate dependency to speed up DB migrations
  - python3-memcache
  - python3-mysqldb     # Tails uses MariaDB as Weblate database
  - python3-pil         # Pillow is packaged as python3-pil in Debian
  - python3-pyuca       # optional Weblate depdendency for Unicode support
  - python3-yaml        # For our own scripts
  - pkg-config             # Required for building pygobject from source
  - libglib2.0-dev         # Required for building pygobject from source
  - libgirepository1.0-dev # Required for building pygobject from source
  - libcairo2-dev          # Required for building pygobject from source
  - libxml2-dev            # Required for building lxml
  - libxslt1-dev           # Required for building lxml
  - gir1.2-pango-1.0       # Required for upgrading Weblate from 3.6.1 to 3.7.1
  - libssl-dev          # required for building borgbackup
  - libacl1-dev         # required for building borgbackup

pip:
  amqp:
    additional_condition:
      - '<=2.6.1'  # Last version compatible with vine<5.0.0
    version: 2.6.1
  borgbackup:
    url: https://github.com/borgbackup/borg
  celery:
    url: https://github.com/celery/celery
    version: 4.4.7
    tag: v4.4.7
  celery-batches:
    tag: v0.2
  certifi:
    url: https://github.com/certifi/python-certifi
    tag: 2020.06.20
  cheroot:
    url: https://github.com/cherrypy/cheroot
    version: 8.4.5
    tag: v8.4.5
  click:
    url: https://github.com/pallets/click
  cython:
    url: https://github.com/cython/cython
  django:
    additional_condition:
      - <3.0
    url: https://github.com/django/django
  django-appconf:
    url: https://github.com/django-compressor/django-appconf
  django-compressor:
    url: https://github.com/django-compressor/django-compressor
  djangorestframework:
    url: https://github.com/encode/django-rest-framework
  gitpython:
    version: 3.1.11
  importlib-metadata:
    url: https://gitlab.com/python-devs/importlib_metadata
  jaraco.functools:
    additional_condition:
      - ==2.0 # last version to support Python 3.5
  jellyfish:
    url: https://github.com/jamesturk/jellyfish
  kombu:
    url: https://github.com/celery/kombu
    additional_condition:
      - <=4.6.11  # Last version whose deps support Python 3.5
    version: 4.6.11
  lxml:
    url: https://github.com/lxml/lxml
    version: 4.6.1
    tag: lxml-4.6.1
  openpyxl:
    url: https://foss.heptapod.net/openpyxl/openpyxl # TODO: remove once repo has been updated in Pypi
    repo_type: hg
    additional_condition:
      - <3.0  # Bigger versions require Python >=3.6
  pycairo:
    url: https://github.com/pygobject/pycairo
    additional_condition:
      - '>=1.11.1'  # Needed to build pygobject from source
      - '<=1.19.1'  # Last version to support Python 3.5
  pygobject:
    url: https://gitlab.gnome.org/GNOME/pygobject.git
  python-dateutil:
    url: https://github.com/dateutil/dateutil
  requests:
    url: https://github.com/psf/requests
    version: 2.23.0
  rjsmin:
    url: https://github.com/ndparker/rjsmin
  ruamel.yaml:
    url: http://hg.code.sf.net/p/ruamel-yaml/code
    additional_condition:
     - '>=0.15.0' # translation-finder is not aware but it fails with older versions of ruamel.yaml
    repo_type: hg
  setuptools:
    additional_condition:
      - '>49.2'  # We need a new-ish version so resolving package versions work as expected
  six:
    additional_condition: [ '>=1.12.0' ]  # for django-compressor
  translate-toolkit:
    url: https://github.com/translate/translate
  translation-finder:
    url: https://github.com/WeblateOrg/translation-finder.git
    additional_condition:
      - '<=2.2'  # Last version to not need weblate-translation-data, which itself needs Python 3.6
    version: 1.8
  urllib3:
    url: https://github.com/urllib3/urllib3
  vine:
    additional_condition:
      - '<=1.3.0'  # Last (major) version to support Python 3.5
  weblate-language-data:
    url: https://github.com/WeblateOrg/language-data
  whoosh:
    url: https://github.com/whoosh-community/whoosh
    tag: v2.7.4
  zipp:
    additional_condition: [ '==1.2.0' ] # last version to support Python 3.5
