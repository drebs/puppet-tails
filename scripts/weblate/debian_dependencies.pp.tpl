# Manage Weblate dependencies that are in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::debian_dependencies () {

  $$packages = [
    'ccze', 'ipython3',  # for more convenient debugging
    'mercurial',  # so we can use pip to install from hg repos
    # Dependencies for Weblate $WEBLATE_VERSION
    '$WEBLATE_DEPENDS',
    'sqlite3',
  ]

  ensure_packages($$packages)

}
